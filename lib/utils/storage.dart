import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';

class Storage {
  Storage._privateConstructor();

  static final Storage _instance = Storage._privateConstructor();

  static Storage get instance {
    return _instance;
  }

  static final box = Hive.box(Constants.configName);

  String getBaseUrl() {
    return box.get(Constants.baseUrl, defaultValue: Endpoints.baseUrl) + "api/";
  }

  String getAssetUrl() {
    return box.get(Constants.baseUrl, defaultValue: Endpoints.baseUrl) + "assets/upload/image/";
  }

  void setValue(String key, dynamic value) {
    box.put(key, value);
  }

  dynamic getValue(String key) {
    return box.get(key);
  }
}
