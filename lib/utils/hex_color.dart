import 'package:flutter/material.dart';

import 'color_utils.dart';

class HexColor extends Color {
  HexColor(final String hexColor) : super(colorToInt(hexColor));
}
