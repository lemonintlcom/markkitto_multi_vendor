class Constants {
  Constants._();

  static const String configName = "markkito";
  static const String userdb = "userDB";
  static const String authToken = "authToken";

  static const String countryCode = "country_code";
  static const String countryName = "country_name";
  static const String countryMobileCode = "country_mobile_code";
  static const String isCountrySelected = "isCountrySelected";
  static const String baseUrl = "base_url";
  static const String countryId = "country_id";
  static const String deviceId = "country_id";
  static const String currency = "currency";
  static const String totalMobileNumberDigits = "TotalMobileNumberDigits";

  static const String latitude = "latitude";
  static const String longitude = "longitude";
  static const String locationText = "location_text";
  static const String hasUserLocation = "has_location";
  static const String buildingDetails = "BuildingDetails";
  static const String streetDetails = "StreetDetails";
  static const String locality = "Locality";
  static const String state = "State";
  static const String district = "district";
  static const String country = "district";
  static const String city = "City";
  static const String pinCode = "PinCode";
  static const String landmark = "Landmark";
  static const String isUserLoggedIn = "isUserLoggedIn";

  //single Vendor
  static const String isAppConfigLoaded = "isAppConfigLoaded";
  static const String colorCode = "colorCode";
  static const String appName = "appName";
  static const String storeName = "storeName";
  static const String tagLine = "tagLine";
  static const String isVendorActive = "isVendorActive";
  static const String logo = "logo";
  static const String storeType = "storeType";
  static const String currencySymbol = "CurrencySymbol";
  static const String contact1 = "contact1";
  static const String contact2 = "contact2";
  static const String email = "email";
  static const String storeDb = "storeDb";
  static const String storeId = "storeId";
  static const String displayName = "displayName";
  static const String whatsappContact = "whatsappContact";
  static const String termsCondition = "termsCondition";
  static const String privacyPolicy = "privacyPolicy";
  static const String globalCountryCode = "GlobalCountryCode";
}
