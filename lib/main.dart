import 'package:country_codes/country_codes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:markkito_customer/firebase_options.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await CountryCodes.init();
  await Hive.initFlutter();
  await Hive.openBox(Constants.configName);
  // Firebase.initializeApp();
  // FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  runApp(GetMaterialApp(
    builder: EasyLoading.init(),
    title: "Application",
    debugShowCheckedModeBanner: false,
    initialRoute: AppPages.INITIAL,
    getPages: AppPages.routes,
    theme: ThemeData(
        pageTransitionsTheme: const PageTransitionsTheme(builders: {
      TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
      TargetPlatform.android: ZoomPageTransitionsBuilder(),
    })),
  ));
}
