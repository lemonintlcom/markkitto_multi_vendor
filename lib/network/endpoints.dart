class Endpoints {
  Endpoints._();

  // base url
  static const String baseUrl = "http://uae.markkito.com/";
  static const String baseUrl_dev = "http://192.168.1.16:8000/api/v1/";

  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 30000;

  // booking endpoints
  static const String getPosts = "posts";

  //login
  static const String customer_login = "customer_loginv2/";
  static const String r_country_list = "r_country_list/";
  static const String login_verification = "login_verification_checkingv3/";
  static const String customer_registration = "c_customer_registration";
  static const String customer_shop_list = "r_ShopList_v2";
  static const String customer_market_list = "r_market_list";
  static const String r_market_shop_list = "r_market_shop_list";
  static const String r_ShopDetails_v2 = "r_ShopDetails_v2";
  static const String c_shop_favourite = "c_shop_favourite";
  static const String i_user_address = "i_user_address";
  static const String r_wishlist = "r_wishlist";
  static const String r_order_listv2 = "r_order_listv2";
  static const String r_ProductDetailsv2 = "r_ProductDetailsv2";
  static const String r_home_page_details = "r_home_page_details";
  static const String r_ProductCart = "r_ProductCart";
  static const String c_ProductToCart = "c_ProductToCart";
  static const String r_get_coupon_details = "r_get_coupon_details";
  static const String c_ChangeCartItem = "c_ChangeCartItem";
  // static const String c_order_v3 = "c_order_v3";
  static const String c_confirm_order = "c_confirm_order";
  static const String c_place_order = "c_place_order";
  static const String r_order_detailsv2 = "r_order_detailsv2";
  static const String r_get_notification_data = "r_get_notification_data";
  static const String c_generate_payment_details = "c_generate_payment_details";
  static const String update_order_payment_ref = "update_order_payment_ref";
  static const String r_user_address = "r_user_address";
  static const String d_user_address = "d_user_address";
  static const String r_reviews_and_ratings = "r_reviews_and_ratings";
  static const String r_productList = "r_productList";
  static const String r_bannerDetails = "r_bannerDetails"; //offers

  //single Vendor
  static const String r_get_app_conf = "r_get_app_conf";
  static const String r_search_individual = "r_search_individual";
  static const String r_single_vendor_home = "r_single_vendor_home";
  static const String c_createdProductFavorate = "c_createdProductFavorate";
  static const String r_single_wishList = "r_single_wishList";
  static const String c_add_rating = "c_add_rating";
  static const String c_ClearCart = "c_ClearCart";
  static const String r_single_banners = "r_single_banners";
  static const String r_single_vendor_Cart = "r_single_vendor_Cart";
  static const String r_shop_delivery_options = "r_shop_delivery_options";
  static const String r_categoryproductList = "r_categoryproductList";
  static const String r_update_profile = "r_update_profile";
  static const String c_profile_upload = "c_profile_upload";
  static const String r_banner_details = "r_banner_details";
  static const String u_user_address = "u_user_address";
  static const String r_brandproductList = "r_brandproductList";
  static const String c_makeStripeCurl = "c_makeStripeCurl";

  String AssetUrl() {
    return '';
  }
}
