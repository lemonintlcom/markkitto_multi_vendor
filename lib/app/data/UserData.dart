/// statusCode : 200
/// data : {"otpStatus":"valid","msg":"Correct OTP","token":{"auth":"2fefcbb32845243ccbced7f56fd615c45dfe6acd007e954220d331f0debb70d85e58f18b0f09ffd4b9c67bb04092a3b08a4a469a088055a97d91374c6ac78ad1sjg/2dbUQe88dtEta4AMY9KE2fYZ+ZB6M+pVvL7NQEH649QLdqLkEUmfRHStQvEmzRJIZjVlces88a6bUGJt6YMqgeBIglX0gU7Nac63AJye/H8QBPK0LSJIghV6E+FXOGcgInOR9ygyZw83SV3GoE98URXw6ckLGBa0WTNGe6VIE5FX1kdD80FoGBiGiQi+ws3zc9BC6T6iU1lcXM3jSLFFfkeIJetNGVskrsskjY0PDDL8UCxP25fOICbSz1TNbDDKZERbHw==","UserTypeId":"33","StatusId":"1","CountryId":"2","UserMasterId":"1677","UserId":"1677","Name":null,"ProfilePic":null,"Contact1":null,"Contact2":null,"AreaCode":null,"Gender":null,"DOB":null,"Email":null,"MobileNo":"9496885852","OtpVerification":"2","UserTypeName":"CUSTOMER"}}

class UserData {
  UserData({
    this.statusCode,
    this.data,
  });

  UserData.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// otpStatus : "valid"
/// msg : "Correct OTP"
/// token : {"auth":"2fefcbb32845243ccbced7f56fd615c45dfe6acd007e954220d331f0debb70d85e58f18b0f09ffd4b9c67bb04092a3b08a4a469a088055a97d91374c6ac78ad1sjg/2dbUQe88dtEta4AMY9KE2fYZ+ZB6M+pVvL7NQEH649QLdqLkEUmfRHStQvEmzRJIZjVlces88a6bUGJt6YMqgeBIglX0gU7Nac63AJye/H8QBPK0LSJIghV6E+FXOGcgInOR9ygyZw83SV3GoE98URXw6ckLGBa0WTNGe6VIE5FX1kdD80FoGBiGiQi+ws3zc9BC6T6iU1lcXM3jSLFFfkeIJetNGVskrsskjY0PDDL8UCxP25fOICbSz1TNbDDKZERbHw==","UserTypeId":"33","StatusId":"1","CountryId":"2","UserMasterId":"1677","UserId":"1677","Name":null,"ProfilePic":null,"Contact1":null,"Contact2":null,"AreaCode":null,"Gender":null,"DOB":null,"Email":null,"MobileNo":"9496885852","OtpVerification":"2","UserTypeName":"CUSTOMER"}

class Data {
  Data({
    this.otpStatus,
    this.msg,
    this.token,
  });

  Data.fromJson(dynamic json) {
    otpStatus = json['otpStatus'];
    msg = json['msg'];
    token = json['token'] != null ? User.fromJson(json['token']) : null;
  }
  String? otpStatus;
  String? msg;
  User? token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['otpStatus'] = otpStatus;
    map['msg'] = msg;
    if (token != null) {
      map['token'] = token?.toJson();
    }
    return map;
  }
}

/// auth : "2fefcbb32845243ccbced7f56fd615c45dfe6acd007e954220d331f0debb70d85e58f18b0f09ffd4b9c67bb04092a3b08a4a469a088055a97d91374c6ac78ad1sjg/2dbUQe88dtEta4AMY9KE2fYZ+ZB6M+pVvL7NQEH649QLdqLkEUmfRHStQvEmzRJIZjVlces88a6bUGJt6YMqgeBIglX0gU7Nac63AJye/H8QBPK0LSJIghV6E+FXOGcgInOR9ygyZw83SV3GoE98URXw6ckLGBa0WTNGe6VIE5FX1kdD80FoGBiGiQi+ws3zc9BC6T6iU1lcXM3jSLFFfkeIJetNGVskrsskjY0PDDL8UCxP25fOICbSz1TNbDDKZERbHw=="
/// UserTypeId : "33"
/// StatusId : "1"
/// CountryId : "2"
/// UserMasterId : "1677"
/// UserId : "1677"
/// Name : null
/// ProfilePic : null
/// Contact1 : null
/// Contact2 : null
/// AreaCode : null
/// Gender : null
/// DOB : null
/// Email : null
/// MobileNo : "9496885852"
/// OtpVerification : "2"
/// UserTypeName : "CUSTOMER"

class User {
  User({
    this.auth,
    this.userTypeId,
    this.statusId,
    this.countryId,
    this.userMasterId,
    this.userId,
    this.name,
    this.profilePic,
    this.contact1,
    this.contact2,
    this.areaCode,
    this.gender,
    this.dob,
    this.email,
    this.mobileNo,
    this.otpVerification,
    this.userTypeName,
  });

  User.fromJson(dynamic json) {
    auth = json['auth'];
    userTypeId = json['UserTypeId'];
    statusId = json['StatusId'];
    countryId = json['CountryId'];
    userMasterId = json['UserMasterId'];
    userId = json['UserId'];
    name = json['Name'];
    profilePic = json['ProfilePic'];
    contact1 = json['Contact1'];
    contact2 = json['Contact2'];
    areaCode = json['AreaCode'];
    gender = json['Gender'];
    dob = json['DOB'];
    email = json['Email'];
    mobileNo = json['MobileNo'];
    otpVerification = json['OtpVerification'];
    userTypeName = json['UserTypeName'];
  }
  String? auth;
  int? userTypeId;
  int? statusId;
  int? countryId;
  int? userMasterId;
  int? userId;
  String? name;
  String? profilePic;
  int? contact1;

  String? contact2;

  String? areaCode;

  String? gender;

  String? dob;

  String? email;

  int? mobileNo;

  int? otpVerification;

  String? userTypeName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['auth'] = auth;
    map['UserTypeId'] = userTypeId;
    map['StatusId'] = statusId;
    map['CountryId'] = countryId;
    map['UserMasterId'] = userMasterId;
    map['UserId'] = userId;
    map['Name'] = name;
    map['ProfilePic'] = profilePic;
    map['Contact1'] = contact1;
    map['Contact2'] = contact2;
    map['AreaCode'] = areaCode;
    map['Gender'] = gender;
    map['DOB'] = dob;
    map['Email'] = email;
    map['MobileNo'] = mobileNo;
    map['OtpVerification'] = otpVerification;
    map['UserTypeName'] = userTypeName;
    return map;
  }
}
