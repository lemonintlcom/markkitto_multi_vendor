/// statusCode : 200
/// data : {"items":[{"name":"Everest Chhole Masala ","category":"Cooking Ingredients","count":2,"price":200,"final_price":340,"product_id":7829,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/5ff3cdcb1762b.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Britannia Gobbles Butter Blast Cake","category":"Bakery","count":1,"price":25,"final_price":23.75,"product_id":7828,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60d54d88dfdc0.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Brown Tape 1 Inches","category":"Stationery","count":1,"price":200,"final_price":200,"product_id":7827,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60dc497895b7d.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Carabao Sugar Free Energy Drink","category":"Beverages","count":2,"price":75,"final_price":127.5,"product_id":7825,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60d5aa123ccb7.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Amul Butter ","category":"Dairy & Eggs","count":2,"price":25,"final_price":50,"product_id":7824,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60d4141859ead.jpg","stock_count":0,"is_out_of_stock":false}],"delivery_method":[{"id":1,"name":"Bike"}],"delivery_address":[{"id":"FLaddress24","latitude":31.6318889,"longitude":76.4020704,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress110","latitude":31.6318889,"longitude":76.4020704,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress111","latitude":31.6318889,"longitude":76.4020704,"place_text":"tarlok chand niwas, Kashmir Rd, Tuni, Himachal Pradesh 177006, India\n (Kashmir Road) "},{"id":"FLaddress112","latitude":31.624501,"longitude":76.3951986,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress130","latitude":31.686207148503666,"longitude":76.52129989117383,"place_text":"Shimla-Kangra Rd, Kohta, Hamirpur, Himachal Pradesh 177001, India\n (Kohta) "},{"id":"FLaddress131","latitude":31.629713883294944,"longitude":76.4020874351263,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress167","latitude":11.28119464323403,"longitude":75.81181168556215,"place_text":"Paroppadi Chevarambalam Rd, Chevarambalam, Kozhikode, Kerala 673017, India\n (Chevarambalam) "},{"id":"FLaddress188","latitude":10.011677224423721,"longitude":76.40055891126394,"place_text":"Pullyattu, kumarapuram p.o, Pallikkara, Kerala 683565, India\n (Pallikkara) "},{"id":"FLaddress206","latitude":11.31185705667635,"longitude":75.81313669681549,"place_text":"Unnamed Road, Parambil Palli Makkam, Kozhikode, Kerala 673611, India\n (Parambil Palli Makkam) "},{"id":"FLaddress242","latitude":11.276722952510317,"longitude":75.77858783304693,"place_text":"Mythiri Building, 2/1037-C,D, PM Kutty Rd, East Nadakkave, Nadakkave, Kozhikode, Kerala 673006, India\n (2/1037-C,D) "},{"id":"FLaddress260","latitude":11.276738735070563,"longitude":75.7785640284419,"place_text":"Mythiri Building, 2/1037-C,D, PM Kutty Rd, East Nadakkave, Nadakkave, Kozhikode, Kerala 673006, India\n (2/1037-C,D) "},{"id":"FLaddress312","latitude":32.7266016,"longitude":74.8570259,"place_text":"Krishna Nagar, Jammu, , 180016, "},{"id":"FLaddress313","latitude":28.6665706,"longitude":77.2290677,"place_text":"Mori Gate, New Delhi, DL, 110006, India"},{"id":"FLaddress314","latitude":31.6318889,"longitude":76.4020704,"place_text":"Galore, Hamirpur, HP, 177006, India"},{"id":"FLaddress524","latitude":11.311881713906455,"longitude":75.8130605891347,"place_text":"8R67+HCV, Parambil Palli Makkam, Kozhikode, Kerala 673611, India\n (Parambil Palli Makkam) "},{"id":"FLaddress536","latitude":11.311881713906455,"longitude":75.8130605891347,"place_text":"8R67+HCV,+Parambil+Palli+Makkam,+Kozhikode,+Kerala+673611,+India\n+(Parambil+Palli+Makkam)+"}],"delivery_type":[{"id":1,"name":"FAST DELIVERY"},{"id":2,"name":"TAKE AWAY"},{"id":3,"name":"SCHEDULED DELIVERY"}],"bill_details":{"item_total":741.25,"delivery_fee":50,"tax":0,"grand_total":791.25},"tips":[{"amount":"Rupee10"},{"amount":"Rupee20"},{"amount":"Rupee30"},{"amount":"Rupee40"}]}

class CartResponse {
  CartResponse({
    this.statusCode,
    this.cartData,
  });

  CartResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    cartData = json['data'] != null ? CartData.fromJson(json['data']) : null;
  }

  int? statusCode;
  CartData? cartData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (cartData != null) {
      map['data'] = cartData?.toJson();
    }
    return map;
  }
}

/// items : [{"name":"Everest Chhole Masala ","category":"Cooking Ingredients","count":2,"price":200,"final_price":340,"product_id":7829,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/5ff3cdcb1762b.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Britannia Gobbles Butter Blast Cake","category":"Bakery","count":1,"price":25,"final_price":23.75,"product_id":7828,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60d54d88dfdc0.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Brown Tape 1 Inches","category":"Stationery","count":1,"price":200,"final_price":200,"product_id":7827,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60dc497895b7d.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Carabao Sugar Free Energy Drink","category":"Beverages","count":2,"price":75,"final_price":127.5,"product_id":7825,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60d5aa123ccb7.jpg","stock_count":0,"is_out_of_stock":false},{"name":"Amul Butter ","category":"Dairy & Eggs","count":2,"price":25,"final_price":50,"product_id":7824,"shop_id":25,"product_image":"http://india.markkito.com/assets/upload/image/60d4141859ead.jpg","stock_count":0,"is_out_of_stock":false}]
/// delivery_method : [{"id":1,"name":"Bike"}]
/// delivery_address : [{"id":"FLaddress24","latitude":31.6318889,"longitude":76.4020704,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress110","latitude":31.6318889,"longitude":76.4020704,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress111","latitude":31.6318889,"longitude":76.4020704,"place_text":"tarlok chand niwas, Kashmir Rd, Tuni, Himachal Pradesh 177006, India\n (Kashmir Road) "},{"id":"FLaddress112","latitude":31.624501,"longitude":76.3951986,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress130","latitude":31.686207148503666,"longitude":76.52129989117383,"place_text":"Shimla-Kangra Rd, Kohta, Hamirpur, Himachal Pradesh 177001, India\n (Kohta) "},{"id":"FLaddress131","latitude":31.629713883294944,"longitude":76.4020874351263,"place_text":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "},{"id":"FLaddress167","latitude":11.28119464323403,"longitude":75.81181168556215,"place_text":"Paroppadi Chevarambalam Rd, Chevarambalam, Kozhikode, Kerala 673017, India\n (Chevarambalam) "},{"id":"FLaddress188","latitude":10.011677224423721,"longitude":76.40055891126394,"place_text":"Pullyattu, kumarapuram p.o, Pallikkara, Kerala 683565, India\n (Pallikkara) "},{"id":"FLaddress206","latitude":11.31185705667635,"longitude":75.81313669681549,"place_text":"Unnamed Road, Parambil Palli Makkam, Kozhikode, Kerala 673611, India\n (Parambil Palli Makkam) "},{"id":"FLaddress242","latitude":11.276722952510317,"longitude":75.77858783304693,"place_text":"Mythiri Building, 2/1037-C,D, PM Kutty Rd, East Nadakkave, Nadakkave, Kozhikode, Kerala 673006, India\n (2/1037-C,D) "},{"id":"FLaddress260","latitude":11.276738735070563,"longitude":75.7785640284419,"place_text":"Mythiri Building, 2/1037-C,D, PM Kutty Rd, East Nadakkave, Nadakkave, Kozhikode, Kerala 673006, India\n (2/1037-C,D) "},{"id":"FLaddress312","latitude":32.7266016,"longitude":74.8570259,"place_text":"Krishna Nagar, Jammu, , 180016, "},{"id":"FLaddress313","latitude":28.6665706,"longitude":77.2290677,"place_text":"Mori Gate, New Delhi, DL, 110006, India"},{"id":"FLaddress314","latitude":31.6318889,"longitude":76.4020704,"place_text":"Galore, Hamirpur, HP, 177006, India"},{"id":"FLaddress524","latitude":11.311881713906455,"longitude":75.8130605891347,"place_text":"8R67+HCV, Parambil Palli Makkam, Kozhikode, Kerala 673611, India\n (Parambil Palli Makkam) "},{"id":"FLaddress536","latitude":11.311881713906455,"longitude":75.8130605891347,"place_text":"8R67+HCV,+Parambil+Palli+Makkam,+Kozhikode,+Kerala+673611,+India\n+(Parambil+Palli+Makkam)+"}]
/// delivery_type : [{"id":1,"name":"FAST DELIVERY"},{"id":2,"name":"TAKE AWAY"},{"id":3,"name":"SCHEDULED DELIVERY"}]
/// bill_details : {"item_total":741.25,"delivery_fee":50,"tax":0,"grand_total":791.25}
/// tips : [{"amount":"Rupee10"},{"amount":"Rupee20"},{"amount":"Rupee30"},{"amount":"Rupee40"}]

class CartData {
  CartData({
    this.cartProductItems,
    this.deliveryMethod,
    this.deliveryAddress,
    this.deliveryType,
    this.billDetails,
    this.tips,
  });

  CartData.fromJson(dynamic json) {
    if (json['items'] != null) {
      cartProductItems = [];
      json['items'].forEach((v) {
        cartProductItems?.add(CartProductItems.fromJson(v));
      });
    }
    if (json['delivery_method'] != null) {
      deliveryMethod = [];
      json['delivery_method'].forEach((v) {
        deliveryMethod?.add(DeliveryMethod.fromJson(v));
      });
    }
    if (json['delivery_address'] != null) {
      deliveryAddress = [];
      json['delivery_address'].forEach((v) {
        deliveryAddress?.add(DeliveryAddress.fromJson(v));
      });
    }
    if (json['delivery_type'] != null) {
      deliveryType = [];
      json['delivery_type'].forEach((v) {
        deliveryType?.add(DeliveryType.fromJson(v));
      });
    }
    billDetails = json['bill_details'] != null
        ? BillDetails.fromJson(json['bill_details'])
        : null;
    if (json['tips'] != null) {
      tips = [];
      json['tips'].forEach((v) {
        tips?.add(Tips.fromJson(v));
      });
    }
  }

  List<CartProductItems>? cartProductItems;
  List<DeliveryMethod>? deliveryMethod;
  List<DeliveryAddress>? deliveryAddress;
  List<DeliveryType>? deliveryType;
  BillDetails? billDetails;
  List<Tips>? tips;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (cartProductItems != null) {
      map['items'] = cartProductItems?.map((v) => v.toJson()).toList();
    }
    if (deliveryMethod != null) {
      map['delivery_method'] = deliveryMethod?.map((v) => v.toJson()).toList();
    }
    if (deliveryAddress != null) {
      map['delivery_address'] =
          deliveryAddress?.map((v) => v.toJson()).toList();
    }
    if (deliveryType != null) {
      map['delivery_type'] = deliveryType?.map((v) => v.toJson()).toList();
    }
    if (billDetails != null) {
      map['bill_details'] = billDetails?.toJson();
    }
    if (tips != null) {
      map['tips'] = tips?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// amount : "Rupee10"

class Tips {
  Tips({
    this.amount,
  });

  Tips.fromJson(dynamic json) {
    amount = json['amount'];
  }

  String? amount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['amount'] = amount;
    return map;
  }
}

/// item_total : 741.25
/// delivery_fee : 50
/// tax : 0
/// grand_total : 791.25

class BillDetails {
  BillDetails({
    this.itemTotal,
    this.deliveryFee,
    this.tax,
    this.grandTotal,
    this.discount,
  });

  BillDetails.fromJson(dynamic json) {
    itemTotal = json['item_total'];
    deliveryFee = json['delivery_fee'];
    tax = json['tax'];
    grandTotal = json['grand_total'];
  }

  dynamic itemTotal;
  dynamic deliveryFee;
  dynamic tax;
  dynamic grandTotal;
  dynamic discount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['item_total'] = itemTotal;
    map['delivery_fee'] = deliveryFee;
    map['tax'] = tax;
    map['grand_total'] = grandTotal;
    return map;
  }
}

/// id : 1
/// name : "FAST DELIVERY"

class DeliveryType {
  DeliveryType({
    this.id,
    this.name,
  });

  DeliveryType.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
  }

  int? id;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    return map;
  }
}

/// id : "FLaddress24"
/// latitude : 31.6318889
/// longitude : 76.4020704
/// place_text : "Kashmir Rd, Kashmir, Himachal Pradesh 177006, India\n (Kashmir) "

class DeliveryAddress {
  DeliveryAddress({
    this.id,
    this.latitude,
    this.longitude,
    this.placeText,
  });

  DeliveryAddress.fromJson(dynamic json) {
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    placeText = json['place_text'];
  }

  String? id;
  double? latitude;
  double? longitude;
  String? placeText;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    map['place_text'] = placeText;
    return map;
  }
}

/// id : 1
/// name : "Bike"

class DeliveryMethod {
  DeliveryMethod({
    this.id,
    this.name,
  });

  DeliveryMethod.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
  }

  int? id;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    return map;
  }
}

/// name : "Everest Chhole Masala "
/// category : "Cooking Ingredients"
/// count : 2
/// price : 200
/// final_price : 340
/// product_id : 7829
/// shop_id : 25
/// product_image : "http://india.markkito.com/assets/upload/image/5ff3cdcb1762b.jpg"
/// stock_count : 0
/// is_out_of_stock : false

class CartProductItems {
  CartProductItems({
    this.name,
    this.category,
    this.count,
    this.price,
    this.finalPrice,
    this.productId,
    this.shopId,
    this.productImage,
    this.stockCount,
    this.isOutOfStock,
  });

  CartProductItems.fromJson(dynamic json) {
    name = json['name'];
    category = json['category'];
    count = json['count'];
    price = json['price'];
    finalPrice = json['final_price'];
    productId = json['product_id'];
    shopId = json['shop_id'];
    productImage = json['product_image'];
    stockCount = json['stock_count'];
    isOutOfStock = json['is_out_of_stock'];
  }

  String? name;
  String? category;
  int? count;
  int? price;
  dynamic finalPrice;
  int? productId;
  int? shopId;
  String? productImage;
  int? stockCount;
  bool? isOutOfStock;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = name;
    map['category'] = category;
    map['count'] = count;
    map['price'] = price;
    map['final_price'] = finalPrice;
    map['product_id'] = productId;
    map['shop_id'] = shopId;
    map['product_image'] = productImage;
    map['stock_count'] = stockCount;
    map['is_out_of_stock'] = isOutOfStock;
    return map;
  }
}
