/// statusCode : 200
/// data : {"paymentDetails":{"paymentGateOrderId":"order_JRR1mpptVUVV0w","CustomerName":"Nowfal","amount":32932,"CustomerMob":9496885852,"razorpay_key":"rzp_test_Fj6IJVnJmyEXp5"},"status":"success"}

class RazorPayGeneratePaymentResponse {
  RazorPayGeneratePaymentResponse({
    this.statusCode,
    this.data,
  });

  RazorPayGeneratePaymentResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// paymentDetails : {"paymentGateOrderId":"order_JRR1mpptVUVV0w","CustomerName":"Nowfal","amount":32932,"CustomerMob":9496885852,"razorpay_key":"rzp_test_Fj6IJVnJmyEXp5"}
/// status : "success"

class Data {
  Data({
    this.paymentDetails,
    this.status,
  });

  Data.fromJson(dynamic json) {
    paymentDetails = json['paymentDetails'] != null ? PaymentDetails.fromJson(json['paymentDetails']) : null;
    status = json['status'];
  }
  PaymentDetails? paymentDetails;
  String? status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (paymentDetails != null) {
      map['paymentDetails'] = paymentDetails?.toJson();
    }
    map['status'] = status;
    return map;
  }
}

/// paymentGateOrderId : "order_JRR1mpptVUVV0w"
/// CustomerName : "Nowfal"
/// amount : 32932
/// CustomerMob : 9496885852
/// razorpay_key : "rzp_test_Fj6IJVnJmyEXp5"

class PaymentDetails {
  PaymentDetails({
    this.paymentGateOrderId,
    this.customerName,
    this.amount,
    this.customerMob,
    this.razorpayKey,
  });

  PaymentDetails.fromJson(dynamic json) {
    paymentGateOrderId = json['paymentGateOrderId'];
    customerName = json['CustomerName'];
    amount = json['amount'];
    customerMob = json['CustomerMob'];
    razorpayKey = json['razorpay_key'];
  }
  String? paymentGateOrderId;
  String? customerName;
  dynamic amount;
  dynamic customerMob;
  String? razorpayKey;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['paymentGateOrderId'] = paymentGateOrderId;
    map['CustomerName'] = customerName;
    map['amount'] = amount;
    map['CustomerMob'] = customerMob;
    map['razorpay_key'] = razorpayKey;
    return map;
  }
}
