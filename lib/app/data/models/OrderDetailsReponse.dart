/// statusCode : 200
/// data : {"Order_id":"OrD1047","Order_time":"2022-03-07 at 4:46 pm","total_price":408,"Delivered_time":"","delivery_status":"Ordered","Product_id":3953,"Product_thumb":"http://india.markkito.com/assets/upload/image/60d410c0c5331.png","Product_name":"1 X Amul Mithai Mate (1 Kg)","Items":[{"Product_id":3953,"Item_name":"1 X Amul Mithai Mate (1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d410c0c5331.png","Product_price":208,"Item_count":1},{"Product_id":3843,"Item_name":"1 X Sweet Potato Purple Yam(1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60e3e35f00d90.jpg","Product_price":200,"Item_count":2}],"Shop_details":{"Shop_id":12,"Shop_name":"Yoonus vendor","Shop_thumb":"http://india.markkito.com/assets/upload/image/"},"Address_data":{"StreetDetails":"JCJ2+QR4, Kashmir, Himachal Pradesh 177006, India","Locality":" Kashmir","State":" Himachal Pradesh","City":"JCJ2+QR4","PinCode":"","Landmark":"","Latitude":31.6318897297052,"Longitude":76.40207033604382},"OrderTracking":[{"OrderStatusName":"Ordered","orderstatusid":"OS1","date_time":"2022-03-07 16:46:31","ExpectedDeliveryDate":"","ExpectedDeliveryTime":"","Name":""}]}

class OrderDetailsReponse {
  OrderDetailsReponse({
    this.statusCode,
    this.orderDetails,
  });

  OrderDetailsReponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    orderDetails = json['data'] != null ? OrderDetails.fromJson(json['data']) : null;
  }
  int? statusCode;
  OrderDetails? orderDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (orderDetails != null) {
      map['data'] = orderDetails?.toJson();
    }
    return map;
  }
}

/// Order_id : "OrD1047"
/// Order_time : "2022-03-07 at 4:46 pm"
/// total_price : 408
/// Delivered_time : ""
/// delivery_status : "Ordered"
/// Product_id : 3953
/// Product_thumb : "http://india.markkito.com/assets/upload/image/60d410c0c5331.png"
/// Product_name : "1 X Amul Mithai Mate (1 Kg)"
/// Items : [{"Product_id":3953,"Item_name":"1 X Amul Mithai Mate (1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60d410c0c5331.png","Product_price":208,"Item_count":1},{"Product_id":3843,"Item_name":"1 X Sweet Potato Purple Yam(1 Kg)","Product_thumb":"http://india.markkito.com/assets/upload/image/60e3e35f00d90.jpg","Product_price":200,"Item_count":2}]
/// Shop_details : {"Shop_id":12,"Shop_name":"Yoonus vendor","Shop_thumb":"http://india.markkito.com/assets/upload/image/"}
/// Address_data : {"StreetDetails":"JCJ2+QR4, Kashmir, Himachal Pradesh 177006, India","Locality":" Kashmir","State":" Himachal Pradesh","City":"JCJ2+QR4","PinCode":"","Landmark":"","Latitude":31.6318897297052,"Longitude":76.40207033604382}
/// OrderTracking : [{"OrderStatusName":"Ordered","orderstatusid":"OS1","date_time":"2022-03-07 16:46:31","ExpectedDeliveryDate":"","ExpectedDeliveryTime":"","Name":""}]

class OrderDetails {
  OrderDetails({
    this.orderId,
    this.orderTime,
    this.totalPrice,
    this.deliveredTime,
    this.deliveryStatus,
    this.productId,
    this.productThumb,
    this.productName,
    this.items,
    this.shopDetails,
    this.addressData,
    this.orderTracking,
  });

  OrderDetails.fromJson(dynamic json) {
    orderId = json['Order_id'];
    orderTime = json['Order_time'];
    totalPrice = json['total_price'];
    deliveredTime = json['Delivered_time'];
    deliveryStatus = json['delivery_status'];
    productId = json['Product_id'];
    productThumb = json['Product_thumb'];
    productName = json['Product_name'];
    if (json['Items'] != null) {
      items = [];
      json['Items'].forEach((v) {
        items?.add(Items.fromJson(v));
      });
    }
    shopDetails = json['Shop_details'] != null ? ShopDetails.fromJson(json['Shop_details']) : null;
    addressData = json['Address_data'] != null ? AddressData.fromJson(json['Address_data']) : null;
    if (json['OrderTracking'] != null) {
      orderTracking = [];
      json['OrderTracking'].forEach((v) {
        orderTracking?.add(OrderTracking.fromJson(v));
      });
    }
  }
  String? orderId;
  String? orderTime;
  dynamic totalPrice;
  String? deliveredTime;
  String? deliveryStatus;
  int? productId;
  String? productThumb;
  String? productName;
  List<Items>? items;
  ShopDetails? shopDetails;
  AddressData? addressData;
  List<OrderTracking>? orderTracking;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Order_id'] = orderId;
    map['Order_time'] = orderTime;
    map['total_price'] = totalPrice;
    map['Delivered_time'] = deliveredTime;
    map['delivery_status'] = deliveryStatus;
    map['Product_id'] = productId;
    map['Product_thumb'] = productThumb;
    map['Product_name'] = productName;
    if (items != null) {
      map['Items'] = items?.map((v) => v.toJson()).toList();
    }
    if (shopDetails != null) {
      map['Shop_details'] = shopDetails?.toJson();
    }
    if (addressData != null) {
      map['Address_data'] = addressData?.toJson();
    }
    if (orderTracking != null) {
      map['OrderTracking'] = orderTracking?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// OrderStatusName : "Ordered"
/// orderstatusid : "OS1"
/// date_time : "2022-03-07 16:46:31"
/// ExpectedDeliveryDate : ""
/// ExpectedDeliveryTime : ""
/// Name : ""

class OrderTracking {
  OrderTracking({
    this.orderStatusName,
    this.orderstatusid,
    this.dateTime,
    this.expectedDeliveryDate,
    this.expectedDeliveryTime,
    this.name,
  });

  OrderTracking.fromJson(dynamic json) {
    orderStatusName = json['OrderStatusName'];
    orderstatusid = json['orderstatusid'];
    dateTime = json['date_time'];
    expectedDeliveryDate = json['ExpectedDeliveryDate'];
    expectedDeliveryTime = json['ExpectedDeliveryTime'];
    name = json['Name'];
  }
  String? orderStatusName;
  String? orderstatusid;
  String? dateTime;
  String? expectedDeliveryDate;
  String? expectedDeliveryTime;
  String? name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['OrderStatusName'] = orderStatusName;
    map['orderstatusid'] = orderstatusid;
    map['date_time'] = dateTime;
    map['ExpectedDeliveryDate'] = expectedDeliveryDate;
    map['ExpectedDeliveryTime'] = expectedDeliveryTime;
    map['Name'] = name;
    return map;
  }
}

/// StreetDetails : "JCJ2+QR4, Kashmir, Himachal Pradesh 177006, India"
/// Locality : " Kashmir"
/// State : " Himachal Pradesh"
/// City : "JCJ2+QR4"
/// PinCode : ""
/// Landmark : ""
/// Latitude : 31.6318897297052
/// Longitude : 76.40207033604382

class AddressData {
  AddressData({
    this.streetDetails,
    this.locality,
    this.state,
    this.city,
    this.pinCode,
    this.landmark,
    this.latitude,
    this.longitude,
  });

  AddressData.fromJson(dynamic json) {
    streetDetails = json['StreetDetails'];
    locality = json['Locality'];
    state = json['State'];
    city = json['City'];
    pinCode = json['PinCode'];
    landmark = json['Landmark'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
  }
  String? streetDetails;
  String? locality;
  String? state;
  String? city;
  dynamic pinCode;
  String? landmark;
  double? latitude;
  double? longitude;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['StreetDetails'] = streetDetails;
    map['Locality'] = locality;
    map['State'] = state;
    map['City'] = city;
    map['PinCode'] = pinCode;
    map['Landmark'] = landmark;
    map['Latitude'] = latitude;
    map['Longitude'] = longitude;
    return map;
  }
}

/// Shop_id : 12
/// Shop_name : "Yoonus vendor"
/// Shop_thumb : "http://india.markkito.com/assets/upload/image/"

class ShopDetails {
  ShopDetails({
    this.shopId,
    this.shopName,
    this.shopThumb,
  });

  ShopDetails.fromJson(dynamic json) {
    shopId = json['Shop_id'];
    shopName = json['Shop_name'];
    shopThumb = json['Shop_thumb'];
  }
  int? shopId;
  String? shopName;
  String? shopThumb;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Shop_id'] = shopId;
    map['Shop_name'] = shopName;
    map['Shop_thumb'] = shopThumb;
    return map;
  }
}

/// Product_id : 3953
/// Item_name : "1 X Amul Mithai Mate (1 Kg)"
/// Product_thumb : "http://india.markkito.com/assets/upload/image/60d410c0c5331.png"
/// Product_price : 208
/// Item_count : 1

class Items {
  Items({
    this.productId,
    this.itemName,
    this.productThumb,
    this.productPrice,
    this.itemCount,
  });

  Items.fromJson(dynamic json) {
    productId = json['Product_id'];
    itemName = json['Item_name'];
    productThumb = json['Product_thumb'];
    productPrice = json['Product_price'];
    itemCount = json['Item_count'];
  }
  int? productId;
  String? itemName;
  String? productThumb;
  dynamic productPrice;
  int? itemCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Product_id'] = productId;
    map['Item_name'] = itemName;
    map['Product_thumb'] = productThumb;
    map['Product_price'] = productPrice;
    map['Item_count'] = itemCount;
    return map;
  }
}
