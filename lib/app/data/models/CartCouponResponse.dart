/// statusCode : 200
/// data : {"coupon_details":{"CouponId":"4","status":"success","message":" Applied Successfuly","body":"odder 5 detailsatyt","redeemRate":"50","finalAmount":50}}

class CartCouponResponse {
  CartCouponResponse({
      this.statusCode, 
      this.data,});

  CartCouponResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

}

/// coupon_details : {"CouponId":"4","status":"success","message":" Applied Successfuly","body":"odder 5 detailsatyt","redeemRate":"50","finalAmount":50}

class Data {
  Data({
      this.couponDetails,});

  Data.fromJson(dynamic json) {
    couponDetails = json['coupon_details'] != null ? CouponDetails.fromJson(json['coupon_details']) : null;
  }
  CouponDetails? couponDetails;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (couponDetails != null) {
      map['coupon_details'] = couponDetails?.toJson();
    }
    return map;
  }

}

/// CouponId : "4"
/// status : "success"
/// message : " Applied Successfuly"
/// body : "odder 5 detailsatyt"
/// redeemRate : "50"
/// finalAmount : 50

class CouponDetails {
  CouponDetails({
      this.couponId, 
      this.status, 
      this.message, 
      this.body, 
      this.redeemRate, 
      this.finalAmount,});

  CouponDetails.fromJson(dynamic json) {
    couponId = json['CouponId'];
    status = json['status'];
    message = json['message'];
    body = json['body'];
    redeemRate = json['redeemRate'];
    finalAmount = json['finalAmount'];
  }
  int? couponId;
  String? status;
  String? message;
  String? body;
  dynamic redeemRate;
  dynamic finalAmount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['CouponId'] = couponId;
    map['status'] = status;
    map['message'] = message;
    map['body'] = body;
    map['redeemRate'] = redeemRate;
    map['finalAmount'] = finalAmount;
    return map;
  }

}