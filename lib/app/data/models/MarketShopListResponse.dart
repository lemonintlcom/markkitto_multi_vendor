import 'package:markkito_customer/app/data/models/ShopListResponse.dart';

import 'MarketListResponse.dart';

/// statusCode : 200
/// data : [{"marketId":1,"marketName":"RP Mall","offer":0,"favourite":0,"marketImage":"6239a49aed342.jpg","market_location":{"place":"Calicut","latitude":11.2685155,"longitude":75.8108029},"shoplist":[{"shop_id":25,"shop_name":"Aswathi Vendor","rating":0,"delivery_time":20,"offer_text":"","offer_code":"","km":6963.496017271562,"profilePic":"60d2c7c5d1344.jpg","shop_location":{"place":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","latitude":31.6318889,"longitude":76.4020704}}],"catogeries":[{"id":9,"name":"Book and stationery ","count":1}],"shop_thumbs":["60d2c7c5d1344.jpg"]}]

class MarketShopListResponse {
  MarketShopListResponse({
    this.statusCode,
    this.marketShopData,
  });

  MarketShopListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    marketShopData = json['data'] != null ? MarketData.fromJson(json['data']) : null;
  }
  int? statusCode;
  MarketData? marketShopData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (marketShopData != null) {
      map['data'] = marketShopData?.toJson();
    }
    return map;
  }
}

/// marketId : 1
/// marketName : "RP Mall"
/// offer : 0
/// favourite : 0
/// marketImage : "6239a49aed342.jpg"
/// market_location : {"place":"Calicut","latitude":11.2685155,"longitude":75.8108029}
/// shoplist : [{"shop_id":25,"shop_name":"Aswathi Vendor","rating":0,"delivery_time":20,"offer_text":"","offer_code":"","km":6963.496017271562,"profilePic":"60d2c7c5d1344.jpg","shop_location":{"place":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","latitude":31.6318889,"longitude":76.4020704}}]
/// catogeries : [{"id":9,"name":"Book and stationery ","count":1}]
/// shop_thumbs : ["60d2c7c5d1344.jpg"]

class MarketData {
  MarketData({
    this.marketId,
    this.marketName,
    this.offer,
    this.rating,
    this.distance,
    this.favourite,
    this.marketImage,
    this.marketLocation,
    this.shoplist,
    this.catogeries,
    this.shopThumbs,
  });

  MarketData.fromJson(dynamic json) {
    marketId = json['marketId'];
    marketName = json['marketName'];
    offer = json['offer'];
    rating = json['rating'];
    distance = json['distance'];
    favourite = json['favourite'];
    marketImage = json['marketImage'];
    marketLocation = json['market_location'] != null ? MarketLocation.fromJson(json['market_location']) : null;
    if (json['shoplist'] != null) {
      shoplist = [];
      json['shoplist'].forEach((v) {
        shoplist?.add(Shop.fromJson(v));
      });
    }
    if (json['catogeries'] != null) {
      catogeries = [];
      json['catogeries'].forEach((v) {
        catogeries?.add(Catogeries.fromJson(v));
      });
    }
    shopThumbs = json['shop_thumbs'] != null ? json['shop_thumbs'].cast<String>() : [];
  }
  int? marketId;
  String? marketName;
  String? offer;
  int? favourite;
  double? distance;
  String? marketImage;
  int? rating;
  MarketLocation? marketLocation;
  List<Shop>? shoplist;
  List<Catogeries>? catogeries;
  List<String>? shopThumbs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['marketId'] = marketId;
    map['marketName'] = marketName;
    map['offer'] = offer;
    map['distance'] = distance;
    map['rating'] = rating;
    map['favourite'] = favourite;
    map['marketImage'] = marketImage;
    if (marketLocation != null) {
      map['market_location'] = marketLocation?.toJson();
    }
    if (shoplist != null) {
      map['shoplist'] = shoplist?.map((v) => v.toJson()).toList();
    }
    if (catogeries != null) {
      map['catogeries'] = catogeries?.map((v) => v.toJson()).toList();
    }
    map['shop_thumbs'] = shopThumbs;
    return map;
  }
}

/// id : 9
/// name : "Book and stationery "
/// count : 1

class Catogeries {
  Catogeries({
    this.id,
    this.name,
    this.count,
  });

  Catogeries.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    count = json['count'];
  }
  int? id;
  String? name;
  int? count;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['count'] = count;
    return map;
  }
}

/// shop_id : 25
/// shop_name : "Aswathi Vendor"
/// rating : 0
/// delivery_time : 20
/// offer_text : ""
/// offer_code : ""
/// km : 6963.496017271562
/// profilePic : "60d2c7c5d1344.jpg"
/// shop_location : {"place":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","latitude":31.6318889,"longitude":76.4020704}

/// place : "Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) "
/// latitude : 31.6318889
/// longitude : 76.4020704

/// place : "Calicut"
/// latitude : 11.2685155
/// longitude : 75.8108029
