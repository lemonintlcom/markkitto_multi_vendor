/// statusCode : 200
/// data : {"marketlist":[{"marketId":1,"marketName":"RP Mall","market_avatar":"6239a49aed342.jpg","distance":6970.39,"market_location":{"place":"Calicut","latitude":11.2685155,"longitude":75.8108029},"rating":0,"offer":""}]}

class MarketListResponse {
  MarketListResponse({
    this.statusCode,
    this.data,
  });

  MarketListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// marketlist : [{"marketId":1,"marketName":"RP Mall","market_avatar":"6239a49aed342.jpg","distance":6970.39,"market_location":{"place":"Calicut","latitude":11.2685155,"longitude":75.8108029},"rating":0,"offer":""}]

class Data {
  Data({
    this.marketlist,
  });

  Data.fromJson(dynamic json) {
    if (json['marketlist'] != null) {
      marketlist = [];
      json['marketlist'].forEach((v) {
        marketlist?.add(Market.fromJson(v));
      });
    }
  }
  List<Market>? marketlist;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (marketlist != null) {
      map['marketlist'] = marketlist?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// marketId : 1
/// marketName : "RP Mall"
/// market_avatar : "6239a49aed342.jpg"
/// distance : 6970.39
/// market_location : {"place":"Calicut","latitude":11.2685155,"longitude":75.8108029}
/// rating : 0
/// offer : ""

class Market {
  Market({
    this.marketId,
    this.marketName,
    this.marketAvatar,
    this.distance,
    this.marketLocation,
    this.rating,
    this.offer,
  });

  Market.fromJson(dynamic json) {
    marketId = json['marketId'];
    marketName = json['marketName'];
    marketAvatar = json['market_avatar'];
    distance = json['distance'];
    marketLocation = json['market_location'] != null ? MarketLocation.fromJson(json['market_location']) : null;
    rating = json['rating'];
    offer = json['offer'];
  }
  int? marketId;
  String? marketName;
  String? marketAvatar;
  double? distance;
  MarketLocation? marketLocation;
  int? rating;
  String? offer;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['marketId'] = marketId;
    map['marketName'] = marketName;
    map['market_avatar'] = marketAvatar;
    map['distance'] = distance;
    if (marketLocation != null) {
      map['market_location'] = marketLocation?.toJson();
    }
    map['rating'] = rating;
    map['offer'] = offer;
    return map;
  }
}

/// place : "Calicut"
/// latitude : 11.2685155
/// longitude : 75.8108029

class MarketLocation {
  MarketLocation({
    this.place,
    this.latitude,
    this.longitude,
  });

  MarketLocation.fromJson(dynamic json) {
    place = json['place'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }
  String? place;
  double? latitude;
  double? longitude;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['place'] = place;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    return map;
  }
}
