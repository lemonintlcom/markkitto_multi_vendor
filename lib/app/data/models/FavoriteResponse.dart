/// statusCode : 200
/// data : {"status":"success"}

class FavoriteResponse {
  FavoriteResponse({
    this.statusCode,
    this.data,
  });

  FavoriteResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  int? statusCode;
  Data? data;
  FavoriteResponse copyWith({
    int? statusCode,
    Data? data,
  }) =>
      FavoriteResponse(
        statusCode: statusCode ?? this.statusCode,
        data: data ?? this.data,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }
}

/// status : "success"

class Data {
  Data({
    this.status,
  });

  Data.fromJson(dynamic json) {
    status = json['status'];
  }
  String? status;
  Data copyWith({
    String? status,
  }) =>
      Data(
        status: status ?? this.status,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = status;
    return map;
  }
}
