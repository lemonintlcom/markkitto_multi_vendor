import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';

/// statusCode : 200
/// data : [{"id":25,"name":"Aswathi Vendor","location":{"place":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","latitude":31.6318889,"longitude":76.4020704},"offer":"10%","distance":90.34984249634965,"rating":0,"delivery_time":"30-60","image":"http://localhost/markkito/assets/upload/image/60d2c7c5d1344.jpg","isFavourite":1,"product_price":"","product_mrp":"","product_type":"","type":"shop","priceList":[]}]

class WishListResponse {
  WishListResponse({
    this.statusCode,
    this.wishListdata,
  });

  WishListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      wishListdata = [];
      json['data'].forEach((v) {
        wishListdata?.add(WishList.fromJson(v));
      });
    }
  }
  int? statusCode;
  List<WishList>? wishListdata;
  WishListResponse copyWith({
    int? statusCode,
    List<WishList>? wishListdata,
  }) =>
      WishListResponse(
        statusCode: statusCode ?? this.statusCode,
        wishListdata: wishListdata ?? this.wishListdata,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (wishListdata != null) {
      map['data'] = wishListdata?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 25
/// name : "Aswathi Vendor"
/// location : {"place":"Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) ","latitude":31.6318889,"longitude":76.4020704}
/// offer : "10%"
/// distance : 90.34984249634965
/// rating : 0
/// delivery_time : "30-60"
/// image : "http://localhost/markkito/assets/upload/image/60d2c7c5d1344.jpg"
/// isFavourite : 1
/// product_price : ""
/// product_mrp : ""
/// product_type : ""
/// type : "shop"
/// priceList : []

class WishList {
  WishList({
    this.id,
    this.name,
    this.location,
    this.offer,
    this.distance,
    this.rating,
    this.deliveryTime,
    this.image,
    this.isFavourite,
    this.productPrice,
    this.productMrp,
    this.productType,
    this.type,
    this.priceList,
  });

  WishList.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    location = json['location'] != null ? Location.fromJson(json['location']) : null;
    offer = json['offer'];
    distance = json['distance'];
    rating = json['rating'];
    deliveryTime = json['delivery_time'];
    image = json['image'];
    isFavourite = json['isFavourite'];
    productPrice = json['product_price'];
    productMrp = json['product_mrp'];
    productType = json['product_type'];
    type = json['type'];
    if (json['priceList'] != null) {
      priceList = [];
      json['priceList'].forEach((v) {
        priceList?.add(PriceList.fromJson(v));
      });
    }
  }
  int? id;
  String? name;
  Location? location;
  String? offer;
  double? distance;
  int? rating;
  String? deliveryTime;
  String? image;
  int? isFavourite;
  String? productPrice;
  String? productMrp;
  String? productType;
  String? type;
  List<PriceList>? priceList;
  WishList copyWith({
    int? id,
    String? name,
    Location? location,
    String? offer,
    double? distance,
    int? rating,
    String? deliveryTime,
    String? image,
    int? isFavourite,
    String? productPrice,
    String? productMrp,
    String? productType,
    String? type,
    List<PriceList>? priceList,
  }) =>
      WishList(
        id: id ?? this.id,
        name: name ?? this.name,
        location: location ?? this.location,
        offer: offer ?? this.offer,
        distance: distance ?? this.distance,
        rating: rating ?? this.rating,
        deliveryTime: deliveryTime ?? this.deliveryTime,
        image: image ?? this.image,
        isFavourite: isFavourite ?? this.isFavourite,
        productPrice: productPrice ?? this.productPrice,
        productMrp: productMrp ?? this.productMrp,
        productType: productType ?? this.productType,
        type: type ?? this.type,
        priceList: priceList ?? this.priceList,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    if (location != null) {
      map['location'] = location?.toJson();
    }
    map['offer'] = offer;
    map['distance'] = distance;
    map['rating'] = rating;
    map['delivery_time'] = deliveryTime;
    map['image'] = image;
    map['isFavourite'] = isFavourite;
    map['product_price'] = productPrice;
    map['product_mrp'] = productMrp;
    map['product_type'] = productType;
    map['type'] = type;
    if (priceList != null) {
      map['priceList'] = priceList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// place : "Kashmir Rd, Kashmir, Himachal Pradesh 177006, India (Kashmir) "
/// latitude : 31.6318889
/// longitude : 76.4020704

class Location {
  Location({
    this.place,
    this.latitude,
    this.longitude,
  });

  Location.fromJson(dynamic json) {
    place = json['place'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }
  String? place;
  double? latitude;
  double? longitude;
  Location copyWith({
    String? place,
    double? latitude,
    double? longitude,
  }) =>
      Location(
        place: place ?? this.place,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['place'] = place;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    return map;
  }
}
