/// statusCode : 200
/// data : [{"Type":"common","id":20,"url":"https://uae.markkito.com/assets/upload/image/622ca62415dee.jpg"},{"Type":"common","id":21,"url":"https://uae.markkito.com/assets/upload/image/622ca631c7780.jpg"},{"Type":"common","id":22,"url":"https://uae.markkito.com/assets/upload/image/622ca63f9f73c.jpg"}]

class BannersResponse {
  BannersResponse({
    this.statusCode,
    this.banners,
  });

  BannersResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      banners = [];
      json['data'].forEach((v) {
        banners?.add(BannerData.fromJson(v));
      });
    }
  }
  int? statusCode;
  List<BannerData>? banners;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (banners != null) {
      map['data'] = banners?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Type : "common"
/// id : 20
/// url : "https://uae.markkito.com/assets/upload/image/622ca62415dee.jpg"

class BannerData {
  BannerData({
    this.type,
    this.id,
    this.url,
  });

  BannerData.fromJson(dynamic json) {
    type = json['Type'];
    id = json['id'];
    url = json['url'];
  }
  String? type;
  int? id;
  String? url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Type'] = type;
    map['id'] = id;
    map['url'] = url;
    return map;
  }
}
