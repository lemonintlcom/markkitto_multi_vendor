/// statusCode : 200
/// data : {"product_id":897,"product_name":"Brown Tape 1 Inches","product_price":200,"offer_text":"","favorite":0,"product_type":"Stationery","product_description":"Brown Tape 1 Inches","thumb_list":["http://india.markkito.com/assets/upload/image/60dc497895b7d.jpg"],"shop_details":{"shopid":25,"shopname":"Aswathi Vendor","shop_thumb":"60d2c7c5d1344.jpg","shop_category":"Book and stationery "},"varient":[{"varient_id":1,"varient_name":"Black","varient_color":"#000"}],"choice_of_crust":[{"id":6,"name":"Pack","price":75}],"topping":[{"id":1,"name":"mexican","price":100}]}

class ProductDetailResponse {
  ProductDetailResponse({
    this.statusCode,
    this.productDetail,
  });

  ProductDetailResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    productDetail = json['data'] != null ? ProductDetail.fromJson(json['data']) : null;
  }
  int? statusCode;
  ProductDetail? productDetail;
  ProductDetailResponse copyWith({
    int? statusCode,
    ProductDetail? productDetail,
  }) =>
      ProductDetailResponse(
        statusCode: statusCode ?? this.statusCode,
        productDetail: productDetail ?? this.productDetail,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (productDetail != null) {
      map['data'] = productDetail?.toJson();
    }
    return map;
  }
}

/// product_id : 897
/// product_name : "Brown Tape 1 Inches"
/// product_price : 200
/// offer_text : ""
/// favorite : 0
/// product_type : "Stationery"
/// product_description : "Brown Tape 1 Inches"
/// thumb_list : ["http://india.markkito.com/assets/upload/image/60dc497895b7d.jpg"]
/// shop_details : {"shopid":25,"shopname":"Aswathi Vendor","shop_thumb":"60d2c7c5d1344.jpg","shop_category":"Book and stationery "}
/// varient : [{"varient_id":1,"varient_name":"Black","varient_color":"#000"}]
/// choice_of_crust : [{"id":6,"name":"Pack","price":75}]
/// topping : [{"id":1,"name":"mexican","price":100}]

class ProductDetail {
  ProductDetail({
    this.productId,
    this.productName,
    this.productPrice,
    this.offerText,
    this.favorite,
    this.productType,
    this.productDescription,
    this.thumbList,
    this.shopDetails,
    this.varient,
    this.choiceOfCrust,
    this.topping,
  });

  ProductDetail.fromJson(dynamic json) {
    productId = json['product_id'];
    productName = json['product_name'];
    productPrice = json['product_price'];
    offerText = json['offer_text'];
    favorite = json['favorite'];
    productType = json['product_type'];
    productDescription = json['product_description'];
    thumbList = json['thumb_list'] != null ? json['thumb_list'].cast<String>() : [];
    shopDetails = json['shop_details'] != null ? ShopDetails.fromJson(json['shop_details']) : null;
    if (json['varient'] != null) {
      varient = [];
      json['varient'].forEach((v) {
        varient?.add(Varient.fromJson(v));
      });
    }
    if (json['choice_of_crust'] != null) {
      choiceOfCrust = [];
      json['choice_of_crust'].forEach((v) {
        choiceOfCrust?.add(ChoiceOfCrust.fromJson(v));
      });
    }
    if (json['topping'] != null) {
      topping = [];
      json['topping'].forEach((v) {
        topping?.add(Topping.fromJson(v));
      });
    }
  }
  int? productId;
  String? productName;
  int? productPrice;
  String? offerText;
  int? favorite;
  String? productType;
  String? productDescription;
  List<String>? thumbList;
  ShopDetails? shopDetails;
  List<Varient>? varient;
  List<ChoiceOfCrust>? choiceOfCrust;
  List<Topping>? topping;
  ProductDetail copyWith({
    int? productId,
    String? productName,
    int? productPrice,
    String? offerText,
    int? favorite,
    String? productType,
    String? productDescription,
    List<String>? thumbList,
    ShopDetails? shopDetails,
    List<Varient>? varient,
    List<ChoiceOfCrust>? choiceOfCrust,
    List<Topping>? topping,
  }) =>
      ProductDetail(
        productId: productId ?? this.productId,
        productName: productName ?? this.productName,
        productPrice: productPrice ?? this.productPrice,
        offerText: offerText ?? this.offerText,
        favorite: favorite ?? this.favorite,
        productType: productType ?? this.productType,
        productDescription: productDescription ?? this.productDescription,
        thumbList: thumbList ?? this.thumbList,
        shopDetails: shopDetails ?? this.shopDetails,
        varient: varient ?? this.varient,
        choiceOfCrust: choiceOfCrust ?? this.choiceOfCrust,
        topping: topping ?? this.topping,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['product_id'] = productId;
    map['product_name'] = productName;
    map['product_price'] = productPrice;
    map['offer_text'] = offerText;
    map['favorite'] = favorite;
    map['product_type'] = productType;
    map['product_description'] = productDescription;
    map['thumb_list'] = thumbList;
    if (shopDetails != null) {
      map['shop_details'] = shopDetails?.toJson();
    }
    if (varient != null) {
      map['varient'] = varient?.map((v) => v.toJson()).toList();
    }
    if (choiceOfCrust != null) {
      map['choice_of_crust'] = choiceOfCrust?.map((v) => v.toJson()).toList();
    }
    if (topping != null) {
      map['topping'] = topping?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// name : "mexican"
/// price : 100

class Topping {
  Topping({
    this.id,
    this.name,
    this.price,
    this.checked = false,
  });

  Topping.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    // checked = json['checked'];
  }
  int? id;
  String? name;
  int? price;
  bool checked = false;
  Topping copyWith({
    int? id,
    String? name,
    int? price,
    bool checked = false,
  }) =>
      Topping(
        id: id ?? this.id,
        name: name ?? this.name,
        price: price ?? this.price,
        checked: checked,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['price'] = price;
    // map['checked'] = checked;
    return map;
  }
}

/// id : 6
/// name : "Pack"
/// price : 75

class ChoiceOfCrust {
  ChoiceOfCrust({
    this.id,
    this.name,
    this.price,
    this.checked = false,
  });

  ChoiceOfCrust.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    // checked = json['checked'];
  }
  int? id;
  String? name;
  int? price;
  bool checked = false;
  ChoiceOfCrust copyWith({
    int? id,
    String? name,
    int? price,
    bool checked = false,
  }) =>
      ChoiceOfCrust(
        id: id ?? this.id,
        name: name ?? this.name,
        price: price ?? this.price,
        checked: checked,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['price'] = price;
    // map['checked'] = checked;
    return map;
  }
}

/// varient_id : 1
/// varient_name : "Black"
/// varient_color : "#000"

class Varient {
  Varient({
    this.varientId,
    this.varientName,
    this.varientColor,
  });

  Varient.fromJson(dynamic json) {
    varientId = json['varient_id'];
    varientName = json['varient_name'];
    varientColor = json['varient_color'];
  }
  int? varientId;
  String? varientName;
  String? varientColor;
  Varient copyWith({
    int? varientId,
    String? varientName,
    String? varientColor,
  }) =>
      Varient(
        varientId: varientId ?? this.varientId,
        varientName: varientName ?? this.varientName,
        varientColor: varientColor ?? this.varientColor,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['varient_id'] = varientId;
    map['varient_name'] = varientName;
    map['varient_color'] = varientColor;
    return map;
  }
}

/// shopid : 25
/// shopname : "Aswathi Vendor"
/// shop_thumb : "60d2c7c5d1344.jpg"
/// shop_category : "Book and stationery "

class ShopDetails {
  ShopDetails({
    this.shopid,
    this.shopname,
    this.shopThumb,
    this.shopCategory,
  });

  ShopDetails.fromJson(dynamic json) {
    shopid = json['shopid'];
    shopname = json['shopname'];
    shopThumb = json['shop_thumb'];
    shopCategory = json['shop_category'];
  }
  int? shopid;
  String? shopname;
  String? shopThumb;
  String? shopCategory;
  ShopDetails copyWith({
    int? shopid,
    String? shopname,
    String? shopThumb,
    String? shopCategory,
  }) =>
      ShopDetails(
        shopid: shopid ?? this.shopid,
        shopname: shopname ?? this.shopname,
        shopThumb: shopThumb ?? this.shopThumb,
        shopCategory: shopCategory ?? this.shopCategory,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shopid'] = shopid;
    map['shopname'] = shopname;
    map['shop_thumb'] = shopThumb;
    map['shop_category'] = shopCategory;
    return map;
  }
}
