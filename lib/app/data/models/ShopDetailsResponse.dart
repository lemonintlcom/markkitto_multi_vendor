/// statusCode : 200
/// data : {"shop_id":284,"shop_name":"Mabrook Al raidan mandhi house","rating":0,"delivery_time":20,"offer_text":"","offer_code":"","favorite":0,"contact":8078533940,"shop_location":{"place":"Kakkodi Bypass Rd, Kakkodi, Kerala 673611, India","latitude":11.316195052691397,"longitude":75.79909063875677},"categories":[{"CategoryName":"Beverages","CategoryId":3,"ImagePath":"60d3401c643d8.jpg"},{"CategoryName":"Rice & Noodles","CategoryId":256,"ImagePath":"60eb585dcbf38.png"}],"products":[{"id":1112,"GenerateProductListId":4384,"offer":0,"image":"60d41c656f07d.jpg","ImageList":"","product_name":"Pepsi ","catagory_name":"Beverages","price":20,"unit":"Botl","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1112,"price":20,"priceFor":"Botl"}]},{"id":1111,"GenerateProductListId":4405,"offer":0,"image":"60d57944082c7.jpg","ImageList":"61812b3c728c1.jpg","product_name":"Sprite  Bottle","catagory_name":"Beverages","price":40,"unit":"Botl","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1111,"price":40,"priceFor":"Botl"}]},{"id":1110,"GenerateProductListId":4408,"offer":0,"image":"60d5767eebedb.jpg","ImageList":"","product_name":"Bisleri Water ","catagory_name":"Beverages","price":13,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1110,"price":13,"priceFor":"Ltr"}]},{"id":1107,"GenerateProductListId":4383,"offer":0,"image":"5fa2f7642fab3.jpg","ImageList":"","product_name":"Pepsi Party Bottle","catagory_name":"Beverages","price":90,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1107,"price":90,"priceFor":"Ltr"}]},{"id":1106,"GenerateProductListId":4404,"offer":0,"image":"610e3ea4bab10.jpg","ImageList":"","product_name":"Chicken Tandoori","catagory_name":"Barbecue","price":100,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1106,"price":100,"priceFor":"Quarter"},{"generatelistdetailsId":1105,"price":200,"priceFor":"Half"},{"generatelistdetailsId":1104,"price":400,"priceFor":"Full"}]},{"id":1103,"GenerateProductListId":4403,"offer":0,"image":"610e3df2e6411.jpg","ImageList":"","product_name":"Chicken Pollichath","catagory_name":"Barbecue","price":120,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1103,"price":120,"priceFor":"Quarter"},{"generatelistdetailsId":1102,"price":240,"priceFor":"Half"},{"generatelistdetailsId":1101,"price":480,"priceFor":"Full"}]},{"id":1100,"GenerateProductListId":4402,"offer":0,"image":"610e3d4203f40.jpg","ImageList":"","product_name":"Al Faham","catagory_name":"Barbecue","price":110,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1100,"price":110,"priceFor":"Quarter"},{"generatelistdetailsId":1099,"price":220,"priceFor":"Half"},{"generatelistdetailsId":1098,"price":440,"priceFor":"Full"}]},{"id":1097,"GenerateProductListId":4401,"offer":0,"image":"610e3cbe76f67.jpg","ImageList":"","product_name":"Beef Mandhi","catagory_name":"Rice & Noodles","price":200,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1097,"price":200,"priceFor":"Quarter"},{"generatelistdetailsId":1096,"price":400,"priceFor":"Half"},{"generatelistdetailsId":1095,"price":800,"priceFor":"Full"}]},{"id":1094,"GenerateProductListId":4400,"offer":0,"image":"610e3bce12d61.jpg","ImageList":"","product_name":"Alfaham Mandhi Chicken","catagory_name":"Rice & Noodles","price":160,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1094,"price":160,"priceFor":"Quarter"},{"generatelistdetailsId":1093,"price":320,"priceFor":"Half"},{"generatelistdetailsId":1092,"price":630,"priceFor":"Full"}]},{"id":1091,"GenerateProductListId":4397,"offer":0,"image":"60e282c4d3b7a.jpg","ImageList":"","product_name":"Mutton Mandhi","catagory_name":"Rice & Noodles","price":300,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1091,"price":300,"priceFor":"Quarter"},{"generatelistdetailsId":1090,"price":600,"priceFor":"Half"},{"generatelistdetailsId":1089,"price":1200,"priceFor":"Full"}]},{"id":1088,"GenerateProductListId":4398,"offer":0,"image":"60e2827e85346.png","ImageList":"","product_name":"Chicken Pollicha Mandhi","catagory_name":"Rice & Noodles","price":170,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1088,"price":170,"priceFor":"Quarter"},{"generatelistdetailsId":1087,"price":340,"priceFor":"Half"},{"generatelistdetailsId":1086,"price":680,"priceFor":"Full"}]},{"id":1085,"GenerateProductListId":4399,"offer":0,"image":"60e281d601252.jpg","ImageList":"","product_name":"Chicken Mandhi ","catagory_name":"Rice & Noodles","price":150,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1085,"price":150,"priceFor":"Quarter"},{"generatelistdetailsId":1084,"price":290,"priceFor":"Half"},{"generatelistdetailsId":1083,"price":580,"priceFor":"Full"}]},{"id":1078,"GenerateProductListId":4392,"offer":0,"image":"60d41930e2147.jpg","ImageList":"612dce2d37ed2.jpg,612dce36d0313.jpg,612dce37089ee.jpg,61305b15a8bec.jpg,61305b15da384.jpg,61305b15da0b6.jpg","product_name":"7 Up","catagory_name":"Beverages","price":30,"unit":"Can","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1078,"price":30,"priceFor":"Can"}]},{"id":1077,"GenerateProductListId":4382,"offer":0,"image":"60d41a17b1b7a.jpg","ImageList":"61304eb14678b.jpg,61304eb16029c.jpg","product_name":"7up  Party","catagory_name":"Beverages","price":65,"unit":"Botl","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[]},{"id":1076,"GenerateProductListId":4385,"offer":0,"image":"60d57fbbf2563.jpg","ImageList":"6131ac13bc804.jpg,6131ac13d569b.jpg","product_name":" Water Large","catagory_name":"Beverages","price":13,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1076,"price":13,"priceFor":"Ltr"}]}]}

class ShopDetailsResponse {
  ShopDetailsResponse({
    this.statusCode,
    this.shopData,
  });

  ShopDetailsResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    shopData = json['data'] != null ? ShopData.fromJson(json['data']) : null;
  }
  int? statusCode;
  ShopData? shopData;
  ShopDetailsResponse copyWith({
    int? statusCode,
    ShopData? shopData,
  }) =>
      ShopDetailsResponse(
        statusCode: statusCode ?? this.statusCode,
        shopData: shopData ?? this.shopData,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (shopData != null) {
      map['data'] = shopData?.toJson();
    }
    return map;
  }
}

/// shop_id : 284
/// shop_name : "Mabrook Al raidan mandhi house"
/// rating : 0
/// delivery_time : 20
/// offer_text : ""
/// offer_code : ""
/// favorite : 0
/// contact : 8078533940
/// shop_location : {"place":"Kakkodi Bypass Rd, Kakkodi, Kerala 673611, India","latitude":11.316195052691397,"longitude":75.79909063875677}
/// categories : [{"CategoryName":"Beverages","CategoryId":3,"ImagePath":"60d3401c643d8.jpg"},{"CategoryName":"Rice & Noodles","CategoryId":256,"ImagePath":"60eb585dcbf38.png"}]
/// products : [{"id":1112,"GenerateProductListId":4384,"offer":0,"image":"60d41c656f07d.jpg","ImageList":"","product_name":"Pepsi ","catagory_name":"Beverages","price":20,"unit":"Botl","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1112,"price":20,"priceFor":"Botl"}]},{"id":1111,"GenerateProductListId":4405,"offer":0,"image":"60d57944082c7.jpg","ImageList":"61812b3c728c1.jpg","product_name":"Sprite  Bottle","catagory_name":"Beverages","price":40,"unit":"Botl","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1111,"price":40,"priceFor":"Botl"}]},{"id":1110,"GenerateProductListId":4408,"offer":0,"image":"60d5767eebedb.jpg","ImageList":"","product_name":"Bisleri Water ","catagory_name":"Beverages","price":13,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1110,"price":13,"priceFor":"Ltr"}]},{"id":1107,"GenerateProductListId":4383,"offer":0,"image":"5fa2f7642fab3.jpg","ImageList":"","product_name":"Pepsi Party Bottle","catagory_name":"Beverages","price":90,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1107,"price":90,"priceFor":"Ltr"}]},{"id":1106,"GenerateProductListId":4404,"offer":0,"image":"610e3ea4bab10.jpg","ImageList":"","product_name":"Chicken Tandoori","catagory_name":"Barbecue","price":100,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1106,"price":100,"priceFor":"Quarter"},{"generatelistdetailsId":1105,"price":200,"priceFor":"Half"},{"generatelistdetailsId":1104,"price":400,"priceFor":"Full"}]},{"id":1103,"GenerateProductListId":4403,"offer":0,"image":"610e3df2e6411.jpg","ImageList":"","product_name":"Chicken Pollichath","catagory_name":"Barbecue","price":120,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1103,"price":120,"priceFor":"Quarter"},{"generatelistdetailsId":1102,"price":240,"priceFor":"Half"},{"generatelistdetailsId":1101,"price":480,"priceFor":"Full"}]},{"id":1100,"GenerateProductListId":4402,"offer":0,"image":"610e3d4203f40.jpg","ImageList":"","product_name":"Al Faham","catagory_name":"Barbecue","price":110,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1100,"price":110,"priceFor":"Quarter"},{"generatelistdetailsId":1099,"price":220,"priceFor":"Half"},{"generatelistdetailsId":1098,"price":440,"priceFor":"Full"}]},{"id":1097,"GenerateProductListId":4401,"offer":0,"image":"610e3cbe76f67.jpg","ImageList":"","product_name":"Beef Mandhi","catagory_name":"Rice & Noodles","price":200,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1097,"price":200,"priceFor":"Quarter"},{"generatelistdetailsId":1096,"price":400,"priceFor":"Half"},{"generatelistdetailsId":1095,"price":800,"priceFor":"Full"}]},{"id":1094,"GenerateProductListId":4400,"offer":0,"image":"610e3bce12d61.jpg","ImageList":"","product_name":"Alfaham Mandhi Chicken","catagory_name":"Rice & Noodles","price":160,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1094,"price":160,"priceFor":"Quarter"},{"generatelistdetailsId":1093,"price":320,"priceFor":"Half"},{"generatelistdetailsId":1092,"price":630,"priceFor":"Full"}]},{"id":1091,"GenerateProductListId":4397,"offer":0,"image":"60e282c4d3b7a.jpg","ImageList":"","product_name":"Mutton Mandhi","catagory_name":"Rice & Noodles","price":300,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1091,"price":300,"priceFor":"Quarter"},{"generatelistdetailsId":1090,"price":600,"priceFor":"Half"},{"generatelistdetailsId":1089,"price":1200,"priceFor":"Full"}]},{"id":1088,"GenerateProductListId":4398,"offer":0,"image":"60e2827e85346.png","ImageList":"","product_name":"Chicken Pollicha Mandhi","catagory_name":"Rice & Noodles","price":170,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1088,"price":170,"priceFor":"Quarter"},{"generatelistdetailsId":1087,"price":340,"priceFor":"Half"},{"generatelistdetailsId":1086,"price":680,"priceFor":"Full"}]},{"id":1085,"GenerateProductListId":4399,"offer":0,"image":"60e281d601252.jpg","ImageList":"","product_name":"Chicken Mandhi ","catagory_name":"Rice & Noodles","price":150,"unit":"Quarter","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1085,"price":150,"priceFor":"Quarter"},{"generatelistdetailsId":1084,"price":290,"priceFor":"Half"},{"generatelistdetailsId":1083,"price":580,"priceFor":"Full"}]},{"id":1078,"GenerateProductListId":4392,"offer":0,"image":"60d41930e2147.jpg","ImageList":"612dce2d37ed2.jpg,612dce36d0313.jpg,612dce37089ee.jpg,61305b15a8bec.jpg,61305b15da384.jpg,61305b15da0b6.jpg","product_name":"7 Up","catagory_name":"Beverages","price":30,"unit":"Can","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1078,"price":30,"priceFor":"Can"}]},{"id":1077,"GenerateProductListId":4382,"offer":0,"image":"60d41a17b1b7a.jpg","ImageList":"61304eb14678b.jpg,61304eb16029c.jpg","product_name":"7up  Party","catagory_name":"Beverages","price":65,"unit":"Botl","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[]},{"id":1076,"GenerateProductListId":4385,"offer":0,"image":"60d57fbbf2563.jpg","ImageList":"6131ac13bc804.jpg,6131ac13d569b.jpg","product_name":" Water Large","catagory_name":"Beverages","price":13,"unit":"Ltr","favouirte":0,"CurrencySymbol":"₹ ","CountryId":107,"priceList":[{"generatelistdetailsId":1076,"price":13,"priceFor":"Ltr"}]}]

class ShopData {
  ShopData({
    this.shopId,
    this.shopName,
    this.rating,
    this.deliveryTime,
    this.openingStatus,
    this.offerText,
    this.offerCode,
    this.profilePic,
    this.favorite,
    this.contact,
    this.shopLocation,
    this.categories,
    this.type,
    this.isIncart,
    this.products,
  });

  ShopData.fromJson(dynamic json) {
    shopId = json['shop_id'];
    shopName = json['shop_name'];
    rating = json['rating'];
    openingStatus = json['openingStatus'];
    deliveryTime = json['delivery_time'];
    offerText = json['offer_text'];
    profilePic = json['profilePic'];
    offerCode = json['offer_code'];
    favorite = json['favorite'];
    type = json['type'];
    isIncart = json['isIncart'];
    contact = json['contact'];
    shopLocation = json['shop_location'] != null ? ShopLocation.fromJson(json['shop_location']) : null;
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories?.add(Categories.fromJson(v));
      });
    }
    if (json['products'] != null) {
      products = [];
      json['products'].forEach((v) {
        products?.add(Products.fromJson(v));
      });
    }
  }
  int? shopId;
  String? shopName;
  int? rating;
  int? openingStatus;
  int? deliveryTime;
  String? offerText;
  String? profilePic;
  String? offerCode;
  String? type;
  int? favorite;
  int? contact;
  int? isIncart;
  ShopLocation? shopLocation;
  List<Categories>? categories;
  List<Products>? products;
  ShopData copyWith({
    int? shopId,
    String? shopName,
    int? rating,
    int? openingStatus,
    int? deliveryTime,
    String? offerText,
    String? profilePic,
    String? offerCode,
    String? type,
    int? favorite,
    int? contact,
    int? isIncart,
    ShopLocation? shopLocation,
    List<Categories>? categories,
    List<Products>? products,
  }) =>
      ShopData(
        shopId: shopId ?? this.shopId,
        shopName: shopName ?? this.shopName,
        rating: rating ?? this.rating,
        openingStatus: openingStatus ?? this.openingStatus,
        deliveryTime: deliveryTime ?? this.deliveryTime,
        offerText: offerText ?? this.offerText,
        profilePic: profilePic ?? this.profilePic,
        offerCode: offerCode ?? this.offerCode,
        type: type ?? this.type,
        favorite: favorite ?? this.favorite,
        contact: contact ?? this.contact,
        shopLocation: shopLocation ?? this.shopLocation,
        categories: categories ?? this.categories,
        products: products ?? this.products,
        isIncart: isIncart ?? this.isIncart,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['shop_id'] = shopId;
    map['shop_name'] = shopName;
    map['rating'] = rating;
    map['openingStatus'] = openingStatus;
    map['delivery_time'] = deliveryTime;
    map['profilePic'] = profilePic;
    map['offer_text'] = offerText;
    map['type'] = type;
    map['offer_code'] = offerCode;
    map['favorite'] = favorite;
    map['contact'] = contact;
    map['isIncart'] = isIncart;
    if (shopLocation != null) {
      map['shop_location'] = shopLocation?.toJson();
    }
    if (categories != null) {
      map['categories'] = categories?.map((v) => v.toJson()).toList();
    }
    if (products != null) {
      map['products'] = products?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1112
/// GenerateProductListId : 4384
/// offer : 0
/// image : "60d41c656f07d.jpg"
/// ImageList : ""
/// product_name : "Pepsi "
/// catagory_name : "Beverages"
/// price : 20
/// unit : "Botl"
/// favouirte : 0
/// CurrencySymbol : "₹ "
/// CountryId : 107
/// priceList : [{"generatelistdetailsId":1112,"price":20,"priceFor":"Botl"}]

class Products {
  Products({
    this.id,
    this.generateProductListId,
    this.offer,
    this.image,
    this.imageList,
    this.productName,
    this.catagoryName,
    this.price,
    this.unit,
    this.favouirte,
    this.currencySymbol,
    this.countryId,
    this.isIncart,
    this.priceList,
    this.selectedPriceListId,
    this.cartTempCount = 0,
  });

  Products.fromJson(dynamic json) {
    id = json['id'];
    generateProductListId = json['GenerateProductListId'];
    offer = json['offer'];
    image = json['image'];
    imageList = json['ImageList'];
    productName = json['product_name'];
    catagoryName = json['catagory_name'];
    price = json['price'];
    unit = json['unit'];
    favouirte = json['favouirte'];
    currencySymbol = json['CurrencySymbol'];
    isIncart = json['isIncart'];
    countryId = json['CountryId'];
    if (json['priceList'] != null) {
      priceList = [];
      json['priceList'].forEach((v) {
        priceList?.add(PriceList.fromJson(v));
      });
    }
  }
  int? id;
  int? generateProductListId;
  int? offer;
  String? image;
  String? imageList;
  String? productName;
  String? catagoryName;
  dynamic? price;
  String? unit;
  int? favouirte;
  String? currencySymbol;
  int? countryId;
  int? isIncart;
  int cartTempCount = 0;
  int? selectedPriceListId;
  List<PriceList>? priceList;
  Products copyWith({
    int? id,
    int? generateProductListId,
    int? offer,
    String? image,
    String? imageList,
    String? productName,
    String? catagoryName,
    int? price,
    String? unit,
    int? favouirte,
    String? currencySymbol,
    int? countryId,
    int? isIncart,
    int? cartTempCount,
    int? selectedPriceListId,
    List<PriceList>? priceList,
  }) =>
      Products(
        id: id ?? this.id,
        generateProductListId: generateProductListId ?? this.generateProductListId,
        offer: offer ?? this.offer,
        image: image ?? this.image,
        imageList: imageList ?? this.imageList,
        productName: productName ?? this.productName,
        catagoryName: catagoryName ?? this.catagoryName,
        price: price ?? this.price,
        unit: unit ?? this.unit,
        favouirte: favouirte ?? this.favouirte,
        currencySymbol: currencySymbol ?? this.currencySymbol,
        countryId: countryId ?? this.countryId,
        isIncart: isIncart ?? this.isIncart,
        cartTempCount: cartTempCount ?? this.cartTempCount,
        selectedPriceListId: selectedPriceListId ?? this.selectedPriceListId,
        priceList: priceList ?? this.priceList,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['GenerateProductListId'] = generateProductListId;
    map['offer'] = offer;
    map['image'] = image;
    map['ImageList'] = imageList;
    map['product_name'] = productName;
    map['catagory_name'] = catagoryName;
    map['price'] = price;
    map['unit'] = unit;
    map['favouirte'] = favouirte;
    map['CurrencySymbol'] = currencySymbol;
    map['isIncart'] = isIncart;
    map['CountryId'] = countryId;
    if (priceList != null) {
      map['priceList'] = priceList?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// generatelistdetailsId : 1112
/// price : 20
/// priceFor : "Botl"

class PriceList {
  PriceList({
    this.generatelistdetailsId,
    this.price,
    this.cartQty,
    this.priceFor,
  });

  PriceList.fromJson(dynamic json) {
    generatelistdetailsId = json['generatelistdetailsId'];
    price = json['price'];
    cartQty = json['cartQty'];
    priceFor = json['priceFor'];
  }
  int? generatelistdetailsId;
  dynamic price;
  int? cartQty;
  String? priceFor;
  PriceList copyWith({
    int? generatelistdetailsId,
    int? price,
    int? cartQty,
    String? priceFor,
  }) =>
      PriceList(
        generatelistdetailsId: generatelistdetailsId ?? this.generatelistdetailsId,
        price: price ?? this.price,
        cartQty: cartQty ?? this.cartQty,
        priceFor: priceFor ?? this.priceFor,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['generatelistdetailsId'] = generatelistdetailsId;
    map['price'] = price;
    map['cartQty'] = cartQty;
    map['priceFor'] = priceFor;
    return map;
  }
}

/// CategoryName : "Beverages"
/// CategoryId : 3
/// ImagePath : "60d3401c643d8.jpg"

class Categories {
  Categories({
    this.categoryName,
    this.categoryId,
    this.imagePath,
  });

  Categories.fromJson(dynamic json) {
    categoryName = json['CategoryName'];
    categoryId = json['CategoryId'];
    imagePath = json['ImagePath'];
  }
  String? categoryName;
  int? categoryId;
  String? imagePath;
  Categories copyWith({
    String? categoryName,
    int? categoryId,
    String? imagePath,
  }) =>
      Categories(
        categoryName: categoryName ?? this.categoryName,
        categoryId: categoryId ?? this.categoryId,
        imagePath: imagePath ?? this.imagePath,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['CategoryName'] = categoryName;
    map['CategoryId'] = categoryId;
    map['ImagePath'] = imagePath;
    return map;
  }
}

/// place : "Kakkodi Bypass Rd, Kakkodi, Kerala 673611, India"
/// latitude : 11.316195052691397
/// longitude : 75.79909063875677

class ShopLocation {
  ShopLocation({
    this.place,
    this.latitude,
    this.longitude,
  });

  ShopLocation.fromJson(dynamic json) {
    place = json['place'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }
  String? place;
  double? latitude;
  double? longitude;
  ShopLocation copyWith({
    String? place,
    double? latitude,
    double? longitude,
  }) =>
      ShopLocation(
        place: place ?? this.place,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
      );
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['place'] = place;
    map['latitude'] = latitude;
    map['longitude'] = longitude;
    return map;
  }
}
