/// statusCode : 200
/// data : [{"Id":"107","CountryCode":"91","CountryName":"India","TotalMobileNumberDigits":"10","currency":"INR","base_url":"http://india.markkito.com/"},{"Id":"2","CountryCode":"971","CountryName":"UAE","TotalMobileNumberDigits":"9","currency":"UAE","base_url":"http://uae.markkito.com/"}]

class CountryList {
  CountryList({
    this.statusCode,
    this.data,
  });

  CountryList.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      data = [];
      json['data'].forEach((v) {
        data?.add(CountryData.fromJson(v));
      });
    }
  }
  int? statusCode;
  List<CountryData>? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (data != null) {
      map['data'] = data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// Id : "107"
/// CountryCode : "91"
/// CountryName : "India"
/// TotalMobileNumberDigits : "10"
/// currency : "INR"
/// base_url : "http://india.markkito.com/"

class CountryData {
  CountryData({
    this.id,
    this.countryCode,
    this.countryName,
    this.totalMobileNumberDigits,
    this.currency,
    this.baseUrl,
  });

  CountryData.fromJson(dynamic json) {
    id = json['Id'];
    countryCode = json['CountryCode'];
    countryName = json['CountryName'];
    totalMobileNumberDigits = json['TotalMobileNumberDigits'];
    currency = json['currency'];
    baseUrl = json['base_url'];
  }
  int? id;
  int? countryCode;
  String? countryName;
  int? totalMobileNumberDigits;
  String? currency;
  String? baseUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Id'] = id;
    map['CountryCode'] = countryCode;
    map['CountryName'] = countryName;
    map['TotalMobileNumberDigits'] = totalMobileNumberDigits;
    map['currency'] = currency;
    map['base_url'] = baseUrl;
    return map;
  }
}
