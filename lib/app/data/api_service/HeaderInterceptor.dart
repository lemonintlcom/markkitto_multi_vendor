import 'package:dio/dio.dart';

class HeaderInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // final accessToken = GetStorage().read(PrefKeys.ACCESS_TOKEN);
    // if(accessToken !=null) {
    //   options.headers["Authorization"] = "Bearer " + accessToken;
    // }
    options.headers["Content-Type"] = "application/json";
    options.headers["API-KEY"] = "12345";
    options.headers["DeviceId"] = "3435cc1f2a1f3626";
    options.headers["API-Auth"] =
        "5abbdb504ba426d0c1c4aefc65a2258214a4000f13b3603934799dc69b6e086aa727f56adb80a0e86d2f7c1f3bd608bfcebea5f1fa1b6efee46479e4c248aba186OYXojWS/8Hy3OLUWyxiRYVH3wLHywWngqO+Wm/FxiAqC1dAxdVvvqook9qvCVlYBHmXLRAIIn0SrR+tveIPDdDPKEi/6fkw+l0LSAG2Ls9IOH3DNUFJagNCZ+E2DGxRdfi08sO1T7sc6+xbAb3mBhrw+rzYU1ZYcYqtF3srYAwCHgNaWmjXEQ4jbOpd9G7Q5X2HEcXfi2o0r5Lflez+14XJc6ZL/ONuSDUxloJLqwMjzOdCa6kmVyJNELHV003InoOxw==";
    super.onRequest(options, handler);
  }
}
