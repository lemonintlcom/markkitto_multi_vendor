part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const INTRO = _Paths.INTRO;
  static const MARKET_DETAILS = _Paths.MARKET_DETAILS;
  static const PRODUCT_DETAILS = _Paths.PRODUCT_DETAILS;
  static const STORE_DETAILS = _Paths.STORE_DETAILS;
  static const LOGIN = _Paths.LOGIN;
  static const SPLASH = _Paths.SPLASH;
  static const SELECT_LOCATION_MAP = _Paths.SELECT_LOCATION_MAP;
  static const ORDERS = _Paths.ORDERS;
  static const OFFERS = _Paths.OFFERS;
  static const ADDRESS_BOOK = _Paths.ADDRESS_BOOK;
  static const ABOUT = _Paths.ABOUT;
  static const WISHLIST = _Paths.WISHLIST;
  static const NOTIFICATIONS = _Paths.NOTIFICATIONS;
  static const SETTINGS = _Paths.SETTINGS;
  static const PAYMENTS = _Paths.PAYMENTS;
  static const EDIT_PROFILE = _Paths.EDIT_PROFILE;
  static const CONFIRM_ORDER = _Paths.CONFIRM_ORDER;
  static const ORDER_DETAILS = _Paths.ORDER_DETAILS;
  static const ADDRESS_BOOK_EDIT = _Paths.ADDRESS_BOOK_EDIT;
  static const REVIEW = _Paths.REVIEW;

  static const SEARCH = _Paths.SEARCH;
  static const CATEGORY_PRODUCTS = _Paths.CATEGORY_PRODUCTS;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const INTRO = '/intro';
  static const MARKET_DETAILS = '/market-details';
  static const PRODUCT_DETAILS = '/product-details';
  static const STORE_DETAILS = '/store-details';
  static const LOGIN = '/login';
  static const SPLASH = '/splash';
  static const SELECT_LOCATION_MAP = '/select-location-map';
  static const ORDERS = '/orders';
  static const OFFERS = '/offers';
  static const ADDRESS_BOOK = '/address-book';
  static const ABOUT = '/about';
  static const WISHLIST = '/wishlist';
  static const NOTIFICATIONS = '/notifications';
  static const SETTINGS = '/settings';
  static const PAYMENTS = '/payments';
  static const EDIT_PROFILE = '/edit-profile';
  static const CONFIRM_ORDER = '/confirm-order';
  static const ORDER_DETAILS = '/order-details';
  static const ADDRESS_BOOK_EDIT = '/address-book-edit';
  static const REVIEW = '/review';

  static const SEARCH = '/search';
  static const CATEGORY_PRODUCTS = '/category-products';
}
