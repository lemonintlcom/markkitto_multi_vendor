import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/CartResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/cart_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../generated/assets.dart';

class CartItem extends StatelessWidget {
  final CartProductItems? cartProductItems;

  const CartItem({
    Key? key,
    this.cartProductItems,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Image.network(
                // "https://via.placeholder.com/300.png",
                cartProductItems!.productImage!.isNotEmpty
                    ? "${cartProductItems!.productImage}"
                    : "https://via.placeholder.com/300.png",
                height: 40,
                width: 40,
              ),
              const SizedBox(
                width: 14,
              ),
              SizedBox(
                width: Get.width * .4,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${cartProductItems?.name}',
                      // 'dsafsdfsdfsdfasdsafsfs',
                      softWrap: false,
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      style: captionLite.copyWith(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    Text(
                      '${cartProductItems?.category}',
                      softWrap: false,
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      style: caption,
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      if (cartProductItems!.count! > 1) {
                        Get.find<CartController>()
                            .changeItemFromCart(cartProductItems?.productId!, cartProductItems!.count! - 1);
                      }
                      if (cartProductItems!.count! == 1) {
                        Get.find<CartController>().deleteFromCartPromptDialog(cartProductItems!.productId!, context);
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
                      child: Text(
                        '-',
                        style: subtitle2.copyWith(color: AppColors.white),
                      ),
                      decoration: const BoxDecoration(
                        color: AppColors.primary_color,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(4),
                          bottomLeft: Radius.circular(4),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
                    child: Text(
                      '${cartProductItems?.count}',
                      style: subtitle2,
                    ),
                    color: Colors.grey.shade300,
                  ),
                  InkWell(
                    onTap: () {
                      Get.find<CartController>()
                          .changeItemFromCart(cartProductItems?.productId!, cartProductItems!.count! + 1);
                    },
                    child: Container(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
                      child: Text(
                        '+',
                        style: subtitle2.copyWith(color: AppColors.white),
                      ),
                      decoration: const BoxDecoration(
                        color: AppColors.primary_color,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(4),
                          bottomRight: Radius.circular(4),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  InkWell(
                    onTap: () {
                      Get.find<CartController>().deleteFromCartPromptDialog(cartProductItems!.productId!, context);
                    },
                    child: Container(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
                      child: const Icon(
                        Icons.delete,
                        size: 20,
                        color: AppColors.white,
                      ),
                      decoration: const BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.all(
                          Radius.circular(4),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 4,
              ),
              Row(
                children: [
                  Text(
                    '${Storage.box.get(Constants.currency)} ${cartProductItems?.price}',
                    style: captionSmall.copyWith(decoration: TextDecoration.lineThrough),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    '${Storage.box.get(Constants.currency)} ${cartProductItems?.finalPrice.toStringAsFixed(2)}',
                    style: caption,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
