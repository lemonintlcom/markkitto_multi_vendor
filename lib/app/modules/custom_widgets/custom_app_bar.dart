import 'package:flutter/material.dart';

import '../../../themes/custom_theme.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String titleText;
  final Color iconColor;
  final Color titleTextColor;
  final bool automaticallyImplyLeading;
  final List<Widget> actions;
  final Widget titleWidget;

  const CustomAppBar({
    required this.titleText,
    this.iconColor = Colors.black,
    this.titleTextColor = Colors.black,
    this.automaticallyImplyLeading = true,
    this.titleWidget = const Text(''),
    this.actions = const [],
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      titleTextStyle: title.copyWith(
        color: Colors.black,
      ),
      automaticallyImplyLeading: automaticallyImplyLeading,
      title: titleText.isEmpty ? titleWidget : Text(titleText),
      backgroundColor: Colors.white,
      elevation: 0,
      iconTheme: const IconThemeData(
        color: Colors.black, //change your color here
      ),
      centerTitle: false,
      actions: actions,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
