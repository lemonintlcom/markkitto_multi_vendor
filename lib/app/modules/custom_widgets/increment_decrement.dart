import 'package:flutter/material.dart';
import 'package:markkito_customer/app/data/models/CartResponse.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class IncrementDecrement extends StatelessWidget {
  const IncrementDecrement({
    Key? key,
    required this.cartProductItems,
    required this.onIncrement,
    required this.onDecrement,
    this.count = 0,
  }) : super(key: key);

  final CartProductItems? cartProductItems;
  final int count;
  final VoidCallback? onIncrement;
  final VoidCallback? onDecrement;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: InkWell(
            onTap: onDecrement,
            child: Container(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
              child: Center(
                child: Text(
                  '-',
                  style: subtitle2.copyWith(color: AppColors.white),
                ),
              ),
              decoration: const BoxDecoration(
                color: AppColors.primary_color,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(4),
                  bottomLeft: Radius.circular(4),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
            child: Center(
              child: Text(
                '$count',
                style: subtitle2,
              ),
            ),
            color: Colors.grey.shade300,
          ),
        ),
        Expanded(
          child: InkWell(
            onTap: onIncrement,
            child: Container(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 4.0, bottom: 4.0),
              child: Center(
                child: Text(
                  '+',
                  style: subtitle2.copyWith(color: AppColors.white),
                ),
              ),
              decoration: const BoxDecoration(
                color: AppColors.primary_color,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(4),
                  bottomRight: Radius.circular(4),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}