import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/MarketListResponse.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/functions.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class MarketItemCard extends StatelessWidget {
  final bool isFullWidth;
  final Market? market;

  const MarketItemCard({
    this.isFullWidth = false,
    this.market,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: isFullWidth ? 0 : paddingLarge),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: InkWell(
          onTap: () {
            Get.toNamed(Routes.MARKET_DETAILS, arguments: {'marketId': market?.marketId});
          },
          child: Container(
            width: isFullWidth ? Get.width : Get.width * .85,
            height: 200,
            color: AppColors.card_bg_color,
            child: Stack(
              children: [
                Positioned(
                  left: 20,
                  top: 20,
                  child: Image.asset(Assets.imagesBookmarkOutline),
                ),
                Positioned(
                  right: Get.width * .35,
                  top: 2,
                  child: SvgPicture.asset(Assets.svgGridDot),
                ),
                Positioned(
                  right: -60,
                  top: -80,
                  child: Container(
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 4, color: Colors.white),
                      boxShadow: [
                        BoxShadow(color: AppColors.primary_color.withOpacity(.2), blurRadius: 20, spreadRadius: 10)
                      ],
                    ),
                    child: ShapeOfView(
                      shape: CircleShape(),
                      child: Image.network(
                        market != null ? "${market?.marketAvatar}" : "https://picsum.photos/200/300",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 70,
                  left: -10,
                  child: Image.asset(
                    Assets.imagesCurvedCurve,
                    fit: BoxFit.fill,
                    height: 100,
                    width: Get.width / 1.8,
                  ),
                ),
                Positioned(
                  bottom: paddingMedium,
                  left: paddingMedium,
                  child: SizedBox(
                    width: isFullWidth ? Get.width * .87 : Get.width * .8,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: AppColors.white.withOpacity(.5),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 14.0, top: 8, right: 14, bottom: 8),
                            child: Text(
                              '50% OFF',
                              style: body2.copyWith(color: AppColors.primary_color, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 3.0),
                          child: Text(
                            '${market?.marketName}',
                            style: headline6.copyWith(fontWeight: FontWeight.bold, color: AppColors.primary_color),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(
                                '${market?.marketLocation?.place}'.useCorrectEllipsis(),
                                softWrap: false,
                                overflow: TextOverflow.fade,
                                maxLines: 1,
                                style: subtitle2.copyWith(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: AppColors.primary_color),
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0, right: 10, top: 2, bottom: 2),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.star,
                                          size: 18,
                                          color: AppColors.primary_color,
                                        ),
                                        const SizedBox(
                                          width: 2,
                                        ),
                                        Text(
                                          '4.3',
                                          overflow: TextOverflow.fade,
                                          style: subtitle1.copyWith(
                                              color: AppColors.primary_color, fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: isFullWidth ? 8 : 4,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey.shade700),
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0, right: 10, top: 2, bottom: 2),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                          Assets.svgStores,
                                          color: AppColors.primary_color,
                                          height: 18,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 4.0),
                                          child: Text(
                                            '${market?.distance} km',
                                            overflow: TextOverflow.fade,
                                            style: subtitle1.copyWith(fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
