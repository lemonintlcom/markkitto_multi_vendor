import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:markkito_customer/app/modules/custom_widgets/secondary_button.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class BottomSheetModal {
  void showLoginRequired(BuildContext context) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.transparent,
      builder: (context) {
        return Container(
          margin: EdgeInsets.all(8),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 15.0,
                sigmaY: 15.0,
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: AppColors.primary_color.withOpacity(.1),
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  border: Border.all(
                    color: AppColors.primary_color,
                    width: 0.8,
                  ),
                ),
                child: Column(
                  children: [
                    Center(
                      child: FractionallySizedBox(
                        widthFactor: 0.25,
                        child: Container(
                          margin: const EdgeInsets.symmetric(
                            vertical: 8,
                          ),
                          height: 4,
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(2),
                            border: Border.all(
                              color: Colors.black12,
                              width: 0.5,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.person,
                            color: AppColors.primary_color,
                            size: 72,
                          ),
                          Text(
                            'Hello Guest ...',
                            style: headline5,
                          ),
                          Text(
                            'Please login to unlock this feature.',
                            style: captionLite.copyWith(fontSize: 14),
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          SecondaryButton(
                              text: 'Login',
                              textColor: Colors.black,
                              height: 38,
                              borderColor: AppColors.primary_color,
                              onTap: () {}),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
