import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../constants/dimens.dart';
import '../../../generated/assets.dart';

class CartOverviewHome extends StatelessWidget {
  const CartOverviewHome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var homeC = Get.find<HomeContentController>();
    return Column(
      children: [
        Container(
          height: 30,
          color: AppColors.card_offer_color.withOpacity(.1),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                'Great! You are saving ${Storage.instance.getValue(Constants.currency)} ${homeC.numberFormat.format(homeC.cartDatas.value?.total_savings)}',
                style: body1.copyWith(
                  color: AppColors.card_offer_color,
                ),
              ),
            ),
          ),
        ),
        Container(
          height: 55,
          color: AppColors.primary_color,
          child: Padding(
            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${homeC.cartDatas.value?.total_qty} Items | ${Storage.instance.getValue(Constants.currency)} ${homeC.numberFormat.format(homeC.cartDatas.value?.total_amount)}',
                        style: body1.copyWith(color: AppColors.white, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        '${homeC.cartDatas.value?.total_qty} Items | ${Storage.instance.getValue(Constants.currency)} ${homeC.numberFormat.format(homeC.cartDatas.value?.total_amount)}',
                        style: caption.copyWith(color: AppColors.white),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  onTap: () {
                    HomeController homeController = Get.find();
                    homeController.selectIndex(3);
                    homeController.cartWindowVisibilty.value = false;
                    homeController.willPopCallbackCartRefresh();
                  },
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'VIEW CART',
                          style: body1.copyWith(color: AppColors.white, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        SvgPicture.asset(Assets.svgShoppingBag)
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
