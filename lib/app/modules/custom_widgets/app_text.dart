import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppText extends StatelessWidget {
  final Color textColor;
  final FontWeight fontWeight;
  final double fontSize;
  final String text;
  final TextAlign textAlign;
  final TextOverflow overflow;

  const AppText({
    Key? key,
    required this.text,
    this.textColor = Colors.black,
    this.fontWeight = FontWeight.normal,
    this.fontSize = 20,
    this.textAlign = TextAlign.center,
    this.overflow = TextOverflow.visible,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.nunito(color: textColor, fontWeight: fontWeight, fontSize: fontSize),
      textAlign: textAlign,
      overflow: overflow,
    );
  }
}

class AppTextSingleLine extends StatelessWidget {
  final Color textColor;
  final FontWeight fontWeight;
  final double fontSize;
  final String text;
  final TextAlign textAlign;
  final TextOverflow overflow;

  const AppTextSingleLine({
    Key? key,
    required this.text,
    this.textColor = Colors.black,
    this.fontWeight = FontWeight.normal,
    this.fontSize = 20,
    this.textAlign = TextAlign.center,
    this.overflow = TextOverflow.visible,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.nunito(color: textColor, fontWeight: fontWeight, fontSize: fontSize),
      textAlign: textAlign,
      overflow: overflow,
      maxLines: 1,
      softWrap: false,
    );
  }
}
