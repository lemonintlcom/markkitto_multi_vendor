import 'package:flutter/material.dart';
import 'package:markkito_customer/app/modules/custom_widgets/app_text.dart';
import 'package:markkito_customer/constants/colors.dart';

class PrimaryButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final double height;
  final double elevation;
  final double radius;
  final VoidCallback onTap;
  final String text;

  const PrimaryButton({
    required this.text,
    required this.onTap,
    Key? key,
    this.color = AppColors.primary_color,
    this.textColor = Colors.white,
    this.height = 45,
    this.elevation = 0,
    this.radius = 8,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: color,
      textColor: textColor,
      height: height,
      elevation: elevation,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
      ),
      onPressed: onTap,
      child: AppText(
        text: text,
        textColor: Colors.white,
        fontSize: 16,
      ),
    );
  }
}
