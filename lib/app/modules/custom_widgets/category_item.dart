import 'package:flutter/material.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

class CategoryItem extends StatelessWidget {
  final Category? categoryItem;
  const CategoryItem({
    this.categoryItem,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 1.5, color: Colors.lime),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ShapeOfView(
                shape: CircleShape(),
                elevation: 0,
                child: Image.network(
                  categoryItem!.image!.isNotEmpty ? "${categoryItem?.image}" : "https://picsum.photos/200/300",
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Text(
              '${categoryItem?.businessName}',
              softWrap: false,
              maxLines: 1,
              overflow: TextOverflow.fade,
              style: body2.copyWith(
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
