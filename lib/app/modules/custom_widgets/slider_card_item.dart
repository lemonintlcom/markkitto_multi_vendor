import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart' as Home;
import 'package:markkito_customer/app/data/models/HomePageResponse.dart';

import '../../../constants/dimens.dart';

class SliderCartItem extends StatelessWidget {
  final BannerData? bannerItem;
  const SliderCartItem({
    this.bannerItem,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: paddingLarge),
      child: InkWell(
        onTap: () {},
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Container(
            width: Get.width * .8,
            child: Image.network(
              bannerItem!.url!.isNotEmpty ? "${bannerItem?.url}" : "https://picsum.photos/200/300",
              fit: BoxFit.cover,
              width: Get.width * .8,
            ),

            /*Stack(
              children: [
                Image.network(
                  bannerItem!.url!.isNotEmpty ? "${bannerItem?.url}" : "https://picsum.photos/200/300",
                  fit: BoxFit.cover,
                  width: Get.width * .8,
                ),
                Positioned.fill(
                  child: Container(
                    decoration: const BoxDecoration(
                        // gradient: LinearGradient(
                        //   begin: Alignment.bottomCenter,
                        //   end: Alignment.topCenter,
                        //   colors: <Color>[AppColors.shader_color, Colors.transparent],
                        // ),
                        ),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // Text(
                          //   '45% OFF!',
                          //   style: headline6.copyWith(color: AppColors.white, fontWeight: FontWeight.bold),
                          // ),
                          // Text(
                          //   'For all Biriyani',
                          //   style: subtitle1.copyWith(color: AppColors.white, fontWeight: FontWeight.bold),
                          // ),
                          // const SizedBox(
                          //   height: 4,
                          // ),
                          // Text(
                          //   'IN BURGER JUNCTION',
                          //   style: body2.copyWith(color: AppColors.white, fontWeight: FontWeight.bold),
                          // ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),*/
          ),
        ),
      ),
    );
  }
}
