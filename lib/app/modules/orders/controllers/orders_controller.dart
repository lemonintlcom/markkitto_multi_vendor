import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/OrderListResponse.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class OrdersController extends GetxController {
  var isLoading = true.obs;
  var pageNo = 0;

  // List<Order>? ordersList;
  var ordersList = <Order>[];
  NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getOrdersList();
  }

  @override
  void onClose() {}

  getOrdersList() async {
    var data = {
      'pageNo': pageNo,
    };
    var result = await XHttp.request(Endpoints.r_order_listv2, method: XHttp.POST, data: data);
    var orderListResponse = OrderListResponse.fromJson(jsonDecode(result.data));
    if (orderListResponse.statusCode == 200) {
      if (orderListResponse.ordersList!.isNotEmpty) {
        orderListResponse.ordersList?.forEach((element) {
          ordersList.add(element);
        });

        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }
}
