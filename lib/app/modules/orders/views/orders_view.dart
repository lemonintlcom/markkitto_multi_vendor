import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/orders_controller.dart';

class OrdersView extends GetView<OrdersController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: const CustomAppBar(
          titleText: 'Orders',
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Stack(
              children: [
                Obx(() {
                  return controller.isLoading.value
                      ? Container()
                      : controller.ordersList.isEmpty
                          ? Center(
                              child: Text(
                                'No orders found',
                                style: headline5,
                              ),
                            )
                          : ListView.separated(
                              // return controller.isLoading.value ? Container() :ListView.separated(
                              itemCount: controller.ordersList.length,
                              itemBuilder: (context, index) {
                                var item = controller.ordersList[index];
                                return Padding(
                                  padding: const EdgeInsets.only(
                                      left: paddingLarge, right: paddingLarge, top: paddingSmall, bottom: paddingSmall),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Expanded(
                                            child: Row(
                                              children: [
                                                Image.network(
                                                  "${item.productThumb}",
                                                  height: 40,
                                                  width: 40,
                                                ),
                                                const SizedBox(
                                                  width: 14,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        "${item.productName}",
                                                        softWrap: false,
                                                        overflow: TextOverflow.fade,
                                                        maxLines: 1,
                                                        style: subtitle1.copyWith(
                                                            color: Colors.black, fontWeight: FontWeight.bold),
                                                      ),
                                                      item.deliveredTime!.isEmpty
                                                          ? Container()
                                                          : Text(
                                                              "${item.deliveredTime}",
                                                              style: caption.copyWith(color: Colors.grey),
                                                            )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 12,
                                          ),
                                          Text(
                                            "${item.deliveryStatus}",
                                            style: body1.copyWith(color: Colors.green, fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      const Divider(),
                                      Text(
                                        "Items",
                                        style: captionLite.copyWith(color: Colors.grey),
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: List.generate(item.items!.length, (index) {
                                          return Text(
                                            "${item.items?[index].itemName}",
                                            softWrap: false,
                                            overflow: TextOverflow.fade,
                                            maxLines: 1,
                                            style: caption.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                          );
                                        }),
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        "Order on",
                                        style: captionLite.copyWith(color: Colors.grey),
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "${item.orderTime}",
                                            softWrap: false,
                                            overflow: TextOverflow.fade,
                                            maxLines: 1,
                                            style: caption.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                          ),
                                          // Text(
                                          //   "Delivery Boy",
                                          //   style: captionLite.copyWith(color: Colors.grey),
                                          // ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Total Amount",
                                                style: captionLite.copyWith(color: Colors.grey),
                                              ),
                                              Text(
                                                "${Storage.instance.getValue(Constants.currency)} ${controller.numberFormat.format(item.totalPrice)}",
                                                style: subtitle2.copyWith(
                                                    color: Colors.black, fontWeight: FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Get.toNamed(Routes.ORDER_DETAILS,
                                                      arguments: {'orderId': item.orderId, 'fromCart': false});
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: Colors.grey),
                                                    borderRadius: BorderRadius.circular(6),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
                                                    child: Text(
                                                      'View',
                                                      overflow: TextOverflow.fade,
                                                      style: caption.copyWith(
                                                        color: Colors.black,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 8,
                                              ),
                                              InkWell(
                                                onTap: () {},
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: AppColors.primary_color),
                                                    borderRadius: BorderRadius.circular(6),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(left: 12, right: 12, top: 6, bottom: 6),
                                                    child: Text(
                                                      'Track Order',
                                                      overflow: TextOverflow.fade,
                                                      style: caption.copyWith(color: AppColors.primary_color),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                    ],
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return Container(
                                    width: double.infinity, height: paddingSmall, color: Colors.grey.shade300);
                              },
                            );
                })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
