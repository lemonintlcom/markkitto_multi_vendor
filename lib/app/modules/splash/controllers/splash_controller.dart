import 'dart:convert';
import 'dart:io';

import 'package:country_codes/country_codes.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/CountryList.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/api_service/remote_services.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';

import '../../../routes/app_pages.dart';

class SplashController extends GetxController {
  var box = Hive.box(Constants.configName);

  var test = "";

  var countryDatas = <CountryData>[].obs;
  final Rx<CountryData?> selectedCountryData = Rx<CountryData?>(null);

  var isLoading = true.obs;
  @override
  void onInit() {
    super.onInit();
    // setConfigAndLoadApp();
    Logger().e(box.get(Constants.isCountrySelected, defaultValue: false));
    if (box.get(Constants.isCountrySelected, defaultValue: false)) {
      Future.delayed(const Duration(seconds: 1), () {
        if (box.get(Constants.isUserLoggedIn, defaultValue: false)) {
          Get.offAndToNamed(Routes.HOME);
        } else {
          Get.offAndToNamed(Routes.INTRO);
        }
      });
    } else {
      getCountryDataDropDown();
    }
  }

  @override
  void onClose() {}

  setConfigAndLoadApp() async {
    Future.delayed(const Duration(milliseconds: 1), () {
      final CountryDetails details = CountryCodes.detailsForLocale();
      print(details.alpha2Code); // Displays alpha2Code, for example US.
      print(details.dialCode); // Displays the dial code, for example +1.
      print(details.name); // Displays the extended name, for example United States.
      print(details
          .localizedName); // Displays the extended name based on device's language (or other, if provided on init)

      box.put(Constants.countryMobileCode, details.dialCode);
      box.put(Constants.countryName, details.localizedName);
      box.put(Constants.countryCode, details.alpha2Code);
      Logger().i('Device id ${box.get(Constants.deviceId)}');
      box.get(Constants.deviceId) ?? getDeviceId();

      var token = box.get(Constants.authToken, defaultValue: null);
      Logger().e(token);
      if (token != null) {
        XHttp.setAuthToken(Constants.authToken);
      }

      getCountryData();
    });
  }

  getDeviceId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      // import 'dart:io'
      var iosDeviceInfo = await deviceInfo.iosInfo;
      box.put(Constants.deviceId, iosDeviceInfo.identifierForVendor);
      // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      box.put(Constants.deviceId, androidDeviceInfo.androidId); // unique ID on Android
    }
    Logger().i('Device id ${box.get(Constants.deviceId)}');
  }

  void getCountryData() async {
    var result = await RemoteService.getHTTP(Endpoints.r_country_list);
    if (result?.statusCode == 200) {
      var data = CountryList.fromJson(jsonDecode(result?.data));
      for (var country in data.data!) {
        if (country.countryName == box.get(Constants.countryName, defaultValue: "India")) {
          box.put(Constants.countryMobileCode, country.countryCode);
          box.put(Constants.totalMobileNumberDigits, country.totalMobileNumberDigits);
          box.put(Constants.currency, country.currency);
          box.put(Constants.baseUrl, country.baseUrl);
          box.put(Constants.countryId, country.id);
          Logger().e(box.get(Constants.baseUrl));
          Logger().e(box.get(Constants.isUserLoggedIn, defaultValue: false));

          if (box.get(Constants.isUserLoggedIn, defaultValue: false)) {
            Get.offAndToNamed(Routes.HOME);
          } else {
            Get.offAndToNamed(Routes.INTRO);
          }

          break;
        }
      }
    }
    Logger().e(result);
  }

  void getCountryDataDropDown() async {
    var result = await RemoteService.getHTTP(Endpoints.r_country_list);
    if (result?.statusCode == 200) {
      var data = CountryList.fromJson(jsonDecode(result?.data));
      if (data.statusCode == 200) {
        countryDatas.addAll(data.data!);
        box.get(Constants.deviceId) ?? getDeviceId();
      }
    }
    isLoading.value = false;
    Logger().e(result);
  }

  void setSelectedCountry(CountryData country) {
    selectedCountryData.value = country;
    box.put(Constants.countryMobileCode, country.countryCode);
    box.put(Constants.totalMobileNumberDigits, country.totalMobileNumberDigits);
    box.put(Constants.currency, country.currency);
    box.put(Constants.baseUrl, country.baseUrl);
    box.put(Constants.countryId, country.id);
    box.put(Constants.isCountrySelected, true);
    update();
    Logger().e(box.get(Constants.baseUrl));
    Logger().e(country.countryName);
    Logger().e(box.get(Constants.isUserLoggedIn, defaultValue: false));
    Get.toNamed(Routes.INTRO);
  }
}
