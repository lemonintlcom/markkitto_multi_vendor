import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/CountryList.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Expanded(
          child: Center(
            child: Image.asset(Assets.imagesLogo),
          ),
        ),
        Obx(() {
          return Padding(
            padding: const EdgeInsets.all(paddingLarge),
            child: controller.isLoading.value
                ? const CircularProgressIndicator()
                : Padding(
                    padding: const EdgeInsets.all(28.0),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        border: Border.all(color: AppColors.primary_color, style: BorderStyle.solid, width: 0.80),
                      ),
                      child: Theme(
                        data: Theme.of(context).copyWith(
                          canvasColor: Colors.grey.shade300,
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<CountryData>(
                            items: controller.countryDatas.value.map((CountryData value) {
                              return DropdownMenuItem<CountryData>(
                                value: value,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(value.countryName!),
                                ),
                              );
                            }).toList(),
                            onChanged: (selectedCountry) {
                              controller.setSelectedCountry(selectedCountry!);
                            },
                            value: controller.selectedCountryData.value,
                            isExpanded: true,
                            elevation: 0,
                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            hint: Text(
                              'Choose your country',
                              style: subtitleLite.copyWith(fontWeight: FontWeight.normal, color: Colors.grey),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
          );
        }),
        Padding(
          padding: const EdgeInsets.only(bottom: 48.0),
          child: Text(
            'Buy Everywhere ${controller.test}',
            style: headline6.copyWith(fontSize: 20, fontWeight: FontWeight.normal, color: Colors.grey),
          ),
        )
      ],
    ));
  }
}
