import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../controllers/select_location_map_controller.dart';

class SelectLocationMapView extends GetView<SelectLocationMapController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: Get.height * .7,
            child: Obx(() {
              return GoogleMap(
                onMapCreated: controller.onMapCreated,
                mapType: MapType.normal,
                initialCameraPosition: controller.currentPoistion,
                myLocationEnabled: true,
                zoomGesturesEnabled: true,
                onCameraMove: controller.onCameraMove,
                markers: <Marker>{
                  Marker(
                    draggable: true,
                    markerId: const MarkerId("1"),
                    onDragEnd: (position) {
                      controller.updateNewMarkerPosition(position);
                    },
                    position:
                        LatLng(controller.initialPosition.value.latitude, controller.initialPosition.value.longitude),
                    icon: BitmapDescriptor.defaultMarker,
                  ),
                },
                circles: {
                  Circle(
                      circleId: const CircleId('currentCircle'),
                      center:
                          LatLng(controller.initialPosition.value.latitude, controller.initialPosition.value.longitude),
                      radius: 400,
                      fillColor: AppColors.primary_color.withOpacity(0.1),
                      strokeColor: AppColors.primary_color.withOpacity(0.8),
                      strokeWidth: 1),
                },
              );
            }),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(paddingLarge),
              child: Obx(() {
                return controller.isLoading.value
                    ? Center(
                        child: SizedBox(
                            height: 40,
                            width: 40,
                            child: CircularProgressIndicator(
                              color: AppColors.black1,
                            )))
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      Assets.svgLocation,
                                      width: 16,
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Obx(() {
                                      return Text(
                                        '${controller.locality.value}',
                                        style: subtitleLite.copyWith(fontWeight: FontWeight.bold),
                                        maxLines: 1,
                                        textAlign: TextAlign.start,
                                        overflow: TextOverflow.fade,
                                        softWrap: true,
                                      );
                                    }),
                                  ],
                                ),
                              ),
                              MaterialButton(
                                color: Colors.grey.shade200,
                                textColor: AppColors.primary_color,
                                height: 32,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(4),
                                    ),
                                    side: BorderSide(color: Colors.grey.shade300)),
                                onPressed: () async {
                                  controller.showOverlayPlaces();
                                },
                                child: Text(
                                  'CHANGE',
                                  style: caption,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            controller.placemarks.value,
                            style: captionLite,
                            maxLines: 2,
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.fade,
                            softWrap: true,
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: PrimaryButton(
                                  text: 'Confirm Location',
                                  onTap: () {
                                    if (Get.arguments['toLogin'] as bool) {
                                      Get.toNamed(Routes.LOGIN);
                                    } else {
                                      Get.arguments['fromHome'] as bool ? Get.back() : Get.offAllNamed(Routes.HOME);
                                    }
                                    // Get.parameters['fromHome'];
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      );
              }),
            ),
          )
        ],
      ),
    );
  }
}
