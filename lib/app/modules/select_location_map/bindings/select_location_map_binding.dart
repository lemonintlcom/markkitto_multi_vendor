import 'package:get/get.dart';

import '../controllers/select_location_map_controller.dart';

class SelectLocationMapBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectLocationMapController>(
      () => SelectLocationMapController(),
    );
  }
}
