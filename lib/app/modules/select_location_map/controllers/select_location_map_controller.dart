import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/utils/storage.dart';

class SelectLocationMapController extends GetxController {
  var box = Hive.box(Constants.configName);

  GoogleMapController? _controller;
  var initialPosition = LatLng(-15.4630239974464, 28.363397732282127).obs;
  final Set<Marker> _markers = {};
  LatLng? lastMapPosition;

  var isLoading = true.obs;

  var placemarks = ''.obs;
  var locality = ''.obs;

  var currentPoistion = CameraPosition(
    target: Storage.box.get(Constants.hasUserLocation, defaultValue: false)
        ? LatLng(Storage.box.get(Constants.latitude), Storage.box.get(Constants.latitude))
        : LatLng(-15.4630239974464, 28.363397732282127),
    zoom: 15,
  );

  onMapCreated(GoogleMapController controller) {
    Future.delayed(const Duration(milliseconds: 500), () {
      _controller = controller;
    });
  }

  onCameraMove(CameraPosition position) async {
    // initialPosition.value = position.target;
    log("onCameraMove $position");
  }

  getGeoCodingAddress() async {
    String? placeText;
    List<Placemark> placemarks1 =
        await placemarkFromCoordinates(initialPosition.value.latitude, initialPosition.value.longitude);
    if (placemarks1.isNotEmpty) {
      Logger().e(placemarks1[0]);
      placeText =
          "${placemarks1[0].street} ${placemarks1[0].locality},${placemarks1[0].name},${placemarks1[0].subAdministrativeArea}";
      placemarks.value = placeText;
      locality.value = "${placemarks1[0].locality}";
    }
    box.put(Constants.latitude, initialPosition.value.latitude);
    box.put(Constants.longitude, initialPosition.value.longitude);
    box.put(Constants.locationText, placeText);
    box.put(Constants.hasUserLocation, true);
    box.put(Constants.buildingDetails, placemarks1[0].name);
    box.put(Constants.streetDetails, placemarks1[0].street);
    box.put(Constants.locality, placemarks1[0].locality);
    box.put(Constants.state, placemarks1[0].administrativeArea);
    box.put(Constants.district, placemarks1[0].subAdministrativeArea);
    box.put(Constants.country, placemarks1[0].country);
    box.put(Constants.city, placemarks1[0].subAdministrativeArea);
    box.put(Constants.pinCode, placemarks1[0].postalCode);
    box.put(Constants.landmark, placemarks1[0].thoroughfare);
  }

  /*Name: Karthikappally, <…>
  flutter: \^[[38;5;196m│ ⛔       Street: , <…>
  flutter: \^[[38;5;196m│ ⛔       ISO Country Code: IN, <…>
  flutter: \^[[38;5;196m│ ⛔       Country: India, <…>
  flutter: \^[[38;5;196m│ ⛔       Postal code: 690502, <…>
  flutter: \^[[38;5;196m│ ⛔       Administrative area: KL, <…>
  flutter: \^[[38;5;196m│ ⛔       Subadministrative area: Alappuzha,<…>
  flutter: \^[[38;5;196m│ ⛔       Locality: Kayamkulam,<…>
  flutter: \^[[38;5;196m│ ⛔       Sublocality: Karthikappally,<…>
  flutter: \^[[38;5;196m│ ⛔       Thoroughfare: ,<…>
  flutter: \^[[38;5;196m│ ⛔       Subthoroughfare: <…>*/

  @override
  void onInit() {
    super.onInit();
    Logger().e("before ${initialPosition}");
    Logger().e("before ${box.get(Constants.hasUserLocation, defaultValue: false)}");
    Logger().e("before ${box.get(Constants.locationText)}");
    if (box.get(Constants.hasUserLocation, defaultValue: false)) {
      locality.value = box.get(Constants.locality, defaultValue: "");
      placemarks.value = box.get(Constants.locationText, defaultValue: "");
      initialPosition.value = LatLng(box.get(Constants.latitude), box.get(Constants.latitude));

      _controller?.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: initialPosition.value, zoom: 15),
        ),
      );
      // await getGeoCodingAddress();
      Logger().e("before ${box.get(Constants.locationText)}");
      Future.delayed(Duration(seconds: 2));
      isLoading.value = false;
    } else {
      _determinePosition();
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }

    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    initialPosition.value = LatLng(position.latitude, position.longitude);

    _controller?.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: initialPosition.value, zoom: 15),
      ),
    );
    await getGeoCodingAddress();
    isLoading.value = false;
    return position;
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void updateNewMarkerPosition(LatLng position) async {
    isLoading.value = true;
    initialPosition.value = position;
    await getGeoCodingAddress();
    isLoading.value = false;
  }

  Future<void> showOverlayPlaces() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction? p = await PlacesAutocomplete.show(
      context: Get.context!,
      apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
      onError: (e) {},
      mode: Mode.overlay,
      strictbounds: false,
      types: [],
      language: "en",
      decoration: InputDecoration(
        hintText: 'Search',
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      components: [Component(Component.country, "in"), Component(Component.country, "ae")],
    );

    displayPrediction(p, Get.context!);
  }

  Future<void> displayPrediction(Prediction? p, BuildContext context) async {
    if (p != null) {
      // get detail (lat/lng)
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
        apiHeaders: await const GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId!);
      final lat = detail.result.geometry!.location.lat;
      final lng = detail.result.geometry!.location.lng;

      initialPosition.value = LatLng(lat, lng);
      _controller?.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: initialPosition.value, zoom: 15),
        ),
      );
      isLoading.value = true;
      await getGeoCodingAddress();
      isLoading.value = false;
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text("${p.description} - $lat/$lng")),
      // );
    }
  }
}
