import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/NotificationsResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../data/api_service/XHttp.dart';

class NotificationsController extends GetxController {
  //TODO: Implement NotificationsController

  var isLoading = true.obs;
  var pageNo = 0;

  ScrollController scrollController = ScrollController();

  var notifications = <Notifications>[].obs;

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(pagination);
    getNotificationData();
  }

  void pagination() {
    if ((scrollController.position.pixels == scrollController.position.maxScrollExtent)) {
      pageNo += 1;
      //add api for load the more data according to new page
      getNotificationData();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    scrollController.dispose();
  }

  void increment() => count.value++;

  final _setSampleNotificationList = [
    Notifications(
        title: 'subtitle1',
        id: '1',
        createdAt: "2022-02-03 11:37:40",
        message: "bsdsbjdbsnjdashjdb sndjksbdksbjkbds sdfbndsnb"),
    Notifications(
        title: 'subtitle2',
        id: '2',
        createdAt: "2022-02-03 11:37:40",
        message: "bsdsbjdbsnjdashjdb sndjksbdksbjkbds sdfbndsnbsdsdfsdf sdfdsf"),
  ];

  void getNotificationData() async {
    var data = {
      'UserID': Get.find<HomeController>().user?.userId,
      'pageNo': pageNo,
    };
    var result = await XHttp.request(Endpoints.r_get_notification_data, method: XHttp.POST, data: data);
    var notificationResponse = NotificationsResponse.fromJson(jsonDecode(result.data));
    if (notificationResponse.statusCode == 200) {
      notifications.addAll(notificationResponse.data!.notifications!);
      notifications.addAll(_setSampleNotificationList);
      isLoading.value = false;
      // SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }
}
