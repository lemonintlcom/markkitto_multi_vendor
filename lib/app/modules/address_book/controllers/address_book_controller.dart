import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/AddressListResponse.dart';
import 'package:markkito_customer/app/data/models/CartStatusResponse.dart';
import 'package:markkito_customer/app/data/models/DeleteAddressResponse.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class AddressBookController extends GetxController {
  var isLoading = true.obs;
  var addressList = <Address>[].obs;
  @override
  void onInit() {
    super.onInit();
    getAddress();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getAddress() async {
    var result = await XHttp.request(Endpoints.r_user_address, method: XHttp.POST);
    Logger().e(jsonDecode(result.data).toString());
    var addressListResponse = AddressListResponse.fromJson(jsonDecode(result.data));
    if (addressListResponse.statusCode == 200) {
      if (addressListResponse.data!.address!.isNotEmpty) {
        addressList.clear();
        addressList.addAll(addressListResponse.data!.address!);
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }
    isLoading.value = false;
  }

  void onDeleteAddress(Address item, int index, BuildContext context) {
    Widget cancelButton = TextButton(
      child: const Text("Cancel"),
      onPressed: () {
        Get.back();
      },
    );
    Widget continueButton = TextButton(
      child: const Text("Confirm"),
      onPressed: () {
        Get.back();
        deleteAddressApi(item, index);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Delete?"),
      content: const Text("Would you like to delete address?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void onEditAddress(Address item, int index) {
    var encodedAddress = jsonEncode(item.toJson());
    Get.toNamed(Routes.ADDRESS_BOOK_EDIT, arguments: {'position': index, 'data': item.toJson()});
  }

  void deleteAddressApi(Address item, int index) async {
    var data = {
      'addressid': item.addressId,
    };
    var result = await XHttp.request(Endpoints.d_user_address, method: XHttp.POST, data: data);
    var deleteAddressResponse = DeleteAddressResponse.fromJson(jsonDecode(result.data));
    if (deleteAddressResponse.statusCode == 200) {
      getAddress();
      SnackBarSuccess(titleText: "Success", messageText: "${deleteAddressResponse.data?.msg}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Unable to remove address").show();
    }
  }
}
