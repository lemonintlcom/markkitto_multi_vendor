import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../controllers/address_book_controller.dart';

class AddressBookView extends GetView<AddressBookController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
        titleText: 'Saved Address',
      ),
      body: Obx(() {
        return controller.isLoading.value
            ? Container()
            : controller.addressList.isEmpty
                ? Center(
                    child: Text(
                      'No saved address found',
                      style: headline5,
                    ),
                  )
                : ListView.separated(
                    itemCount: controller.addressList.length,
                    separatorBuilder: (BuildContext context, int index) => const Divider(height: 1),
                    itemBuilder: (BuildContext context, int index) {
                      var item = controller.addressList[index];
                      return Padding(
                        padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                        child: ListTile(
                          trailing: Wrap(
                            spacing: 12,
                            children: [
                              TextButton(
                                onPressed: () {
                                  controller.onEditAddress(item, index);
                                },
                                child: const Icon(Icons.edit, color: AppColors.primary_color),
                                style: TextButton.styleFrom(
                                  minimumSize: Size.zero,
                                  padding: EdgeInsets.zero,
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  controller.onDeleteAddress(item, index, context);
                                },
                                child: const Icon(Icons.delete, color: Colors.red),
                                style: TextButton.styleFrom(
                                  minimumSize: Size.zero,
                                  padding: EdgeInsets.zero,
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                ),
                              )
                            ],
                          ),
                          leading: const Icon(Icons.location_pin),
                          subtitle: Text('${item.address1}\n${item.contact1}',
                              softWrap: true,
                              overflow: TextOverflow.fade,
                              style:
                                  captionLite.copyWith(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
                        ),
                      );
                    },
                  );
      }),
    );
  }
}
