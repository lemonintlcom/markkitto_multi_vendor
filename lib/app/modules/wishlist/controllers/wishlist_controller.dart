import 'dart:convert';

import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/WishListResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class WishlistController extends GetxController {
  var defaultChoiceIndex = 0.obs;
  List wishlistTypes = [
    'Products',
    'Shops',
    'Markets',
  ].obs;

  var wishListdata = <WishList>[].obs;
  var isLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    getWishList();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getWishList() async {
    var data = {
      'latitude': Storage.instance.getValue(Constants.latitude),
      'longitude': Storage.instance.getValue(Constants.longitude),
    };
    var result = await XHttp.request(Endpoints.r_wishlist, method: XHttp.POST, data: data);
    var wishListResponse = WishListResponse.fromJson(jsonDecode(result.data));
    if (wishListResponse.statusCode == 200) {
      if (wishListResponse.wishListdata!.isNotEmpty) {
        wishListdata.addAll(wishListResponse.wishListdata!);

        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }
}
