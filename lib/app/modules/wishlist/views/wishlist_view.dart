import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/MarketListResponse.dart';
import 'package:markkito_customer/app/data/models/ShopListResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/market_item_card.dart';
import 'package:markkito_customer/app/modules/custom_widgets/shop_item_card.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../controllers/wishlist_controller.dart';

class WishlistView extends GetView<WishlistController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: const CustomAppBar(
          titleText: 'Wishlist',
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Padding(
              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
              child: CustomScrollView(
                slivers: [
                  const SliverToBoxAdapter(
                    child: SizedBox(
                      height: paddingLarge,
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      child: Obx(() {
                        return Wrap(
                          spacing: 8,
                          children: List.generate(controller.wishlistTypes.length, (index) {
                            return ChoiceChip(
                              labelPadding: EdgeInsets.all(2.0),
                              label: Text(
                                controller.wishlistTypes[index],
                                style: captionLite.copyWith(
                                    color: controller.defaultChoiceIndex.value == index
                                        ? AppColors.white
                                        : AppColors.black1),
                              ),
                              selected: controller.defaultChoiceIndex.value == index,
                              selectedColor: AppColors.primary_color,
                              pressElevation: 0,
                              backgroundColor: AppColors.white,
                              onSelected: (value) {
                                controller.defaultChoiceIndex.value =
                                    value ? index : controller.defaultChoiceIndex.value;
                              },
                              // backgroundColor: color,
                              elevation: 1,
                              padding: const EdgeInsets.symmetric(horizontal: paddingLarge),
                            );
                          }),
                        );
                      }),
                    ),
                  ),
                  const SliverToBoxAdapter(
                    child: SizedBox(
                      height: paddingLarge,
                    ),
                  ),
                  Obx(() {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) {
                          var wishlist = controller.wishListdata[index];
                          if (wishlist.type == 'shop') {
                            var shop = Shop(
                                shopId: wishlist.id,
                                shopName: wishlist.name,
                                shopLocation: ShopLocation(
                                  place: wishlist.location?.place,
                                  latitude: wishlist.location?.latitude,
                                  longitude: wishlist.location?.longitude,
                                ),
                                rating: wishlist.rating,
                                deliveryTime: wishlist.deliveryTime,
                                offerCode: wishlist.offer,
                                offerText: wishlist.offer,
                                favorite: wishlist.isFavourite,
                                profilePic: wishlist.image,
                                km: wishlist.distance);
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 12.0),
                              child: ShopItemCard(
                                shop: shop,
                              ),
                            );
                          } else if (wishlist.type == 'market') {
                            var market = Market();
                            market.marketName = wishlist.name;
                            market.marketId = wishlist.id;
                            market.marketAvatar = wishlist.image;
                            market.distance = wishlist.distance;
                            market.marketLocation = MarketLocation(
                              place: wishlist.location?.place,
                              latitude: wishlist.location?.latitude,
                              longitude: wishlist.location?.longitude,
                            );
                            market.rating = wishlist.rating;
                            market.offer = wishlist.offer;
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 12.0),
                              child: MarketItemCard(
                                market: market,
                                isFullWidth: true,
                              ),
                            );
                          }
                        },
                        childCount: controller.wishListdata.length,
                      ),
                    );
                  }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
