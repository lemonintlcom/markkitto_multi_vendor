import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart';
import 'package:markkito_customer/app/data/models/MarketListResponse.dart';
import 'package:markkito_customer/app/data/models/ShopListResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class HomeContentController extends GetxController {
  var box = Hive.box(Constants.configName);

  final count = 0.obs;
  var pageNo = 0;
  var marketList = <Market>[].obs;
  var categoryList = <Category>[].obs;
  var bannerList = <BannerData>[].obs;
  var shopList = <Shop>[].obs;
  Rx<CartDatas?> cartDatas = Rx(null);
  HomeData? homeData;

  NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");

  ScrollController scrollController = ScrollController();

  var isLoading = true.obs;

  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(pagination);
    getHomeData();
  }

  void pagination() {
    if ((scrollController.position.pixels == scrollController.position.maxScrollExtent)) {
      isLoading.value = true;
      pageNo += 1;
      //add api for load the more data according to new page
      getHomeData();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    scrollController.dispose();
  }

  void increment() => count.value++;

  getHomeData() async {
    var data = {
      'pageNo': pageNo,
      'latitude': Storage.instance.getValue(Constants.latitude),
      'longitude': Storage.instance.getValue(Constants.longitude),
    };
    var result = await XHttp.request(Endpoints.r_home_page_details, method: XHttp.POST, data: data);
    var homeDataResponse = HomePageResponse.fromJson(jsonDecode(result.data));
    if (homeDataResponse.statusCode == 200) {
      if (homeDataResponse.homeData != null) {
        homeData = homeDataResponse.homeData;
        bannerList.addAll(homeDataResponse.homeData!.banner!);
        categoryList.addAll(homeDataResponse.homeData!.category!);
        marketList.addAll(homeDataResponse.homeData!.marketList!);
        shopList.addAll(homeDataResponse.homeData!.shopList!);
        if (homeDataResponse.homeData?.cartDatas != null) {
          cartDatas.value = homeDataResponse.homeData?.cartDatas;
          Get.find<HomeController>().cartWindowVisibilty.value = true;
        } else {
          Get.find<HomeController>().cartWindowVisibilty.value = false;
        }
        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  updateCartData({cartData, isAdded}) {
    cartDatas.value = cartData;
    isAdded
        ? Get.find<HomeController>().cartWindowVisibilty.value = true
        : Get.find<HomeController>().cartWindowVisibilty.value = false;
  }
}
