import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/ShopListResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class StoresController extends GetxController {
  var shopList = <Shop>[].obs;
  var pageNo = 0;
  ScrollController scrollController = ScrollController();
  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(pagination);
    getStores();
  }

  void pagination() {
    if ((scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) ) {

      pageNo += 1;
      //add api for load the more data according to new page
      getStores();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    scrollController.dispose();
  }

  getStores() async {
    var data = {
      'pageNo': pageNo,
      'latitude': Storage.instance.getValue(Constants.latitude),
      'longitude': Storage.instance.getValue(Constants.longitude)
    };
    var result = await XHttp.request(Endpoints.customer_shop_list, method: XHttp.POST, data: data);
    var shopDataList = ShopListResponse.fromJson(jsonDecode(result.data));
    if (shopDataList.statusCode == 200) {
      if (shopDataList.data!.isNotEmpty) {
        shopList.addAll(shopDataList.data!);
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }
  }
}
