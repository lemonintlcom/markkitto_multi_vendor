import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/MarketListResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class MarketsController extends GetxController {
  var marketList = <Market>[].obs;
  var pageNo = 0;

  ScrollController scrollController = ScrollController();
  @override
  void onInit() {
    super.onInit();
    scrollController.addListener(pagination);
    getStores();
  }

  @override
  void onReady() {
    super.onReady();

  }

  @override
  void onClose() {
    scrollController.dispose();
  }

  void pagination() {
    if ((scrollController.position.pixels ==
        scrollController.position.maxScrollExtent) ) {

      // isLoading.value = true;
      pageNo += 1;
      //add api for load the more data according to new page
      getStores();
    }
  }


  getStores() async {
    var data = {
      'pageNo': pageNo,
      'latitude': Storage.instance.getValue(Constants.latitude),
      'longitude': Storage.instance.getValue(Constants.longitude)
    };
    var result = await XHttp.request(Endpoints.customer_market_list, method: XHttp.POST, data: data);
    Logger().e(jsonDecode(result.data).toString());
    var shopDataList = MarketListResponse.fromJson(jsonDecode(result.data));
    if (shopDataList.statusCode == 200) {
      if (shopDataList.data!.marketlist!.isNotEmpty) {
        marketList.addAll(shopDataList.data!.marketlist!);
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }
  }
}
