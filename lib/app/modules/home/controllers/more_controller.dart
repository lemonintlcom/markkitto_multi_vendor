import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart' as dio;
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/UserData.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/ProfileUpdateResponse.dart';
import 'package:markkito_customer/app/data/models/ProfileUpdatedResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:url_launcher/url_launcher.dart';

class MoreController extends GetxController {
  var box = Storage.instance;

  // User? user;
  Rx<User?> user = Rx(null);

  var isNotification = true.obs;

  var nameController = TextEditingController();
  var emailController = TextEditingController();

  final ImagePicker _picker = ImagePicker();
  var profilePic = ''.obs;
  // Rx<File?> profilePic = Rx(null);
  var isProfileFromLocal = false.obs;

  @override
  void onInit() {
    super.onInit();
    log("${Get.find<HomeController>().isLoggedIn}");
    if (Get.find<HomeController>().isLoggedIn) {
      user.value = User.fromJson((jsonDecode(Storage.box.get(Constants.userdb))));
      Logger().e(jsonDecode(Storage.box.get(Constants.userdb)));
      profilePic.value = user.value!.profilePic!;
    }
  }

  void launchURL() async {
    print('${Storage.instance.getValue(Constants.email)}');
    final Uri params = Uri(
      scheme: 'mailto',
      path: '${Storage.instance.getValue(Constants.email)}',
      query: 'subject=App Feedback',
    );
    String url = params.toString();
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Unable to send feedback").show();
    }
  }

  launchCaller(String number) async {
    String url = Platform.isIOS ? 'tel://$number' : 'tel:$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Could not launch dialer").show();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void logout() async {
    await Storage.box.clear();
    Get.offAllNamed(Routes.SPLASH);
  }

  void profileUpdate(BuildContext context) {
    nameController.text = user.value?.name != null ? user.value!.name! : '';
    emailController.text = user.value?.email != null ? user.value!.email! : '';
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingLarge),
            child: Container(
              height: Get.height * .7,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Update your profile',
                      style: subtitle1,
                    ),
                  ),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                  CircleAvatar(
                    radius: 32,
                    backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                        ? AppColors.primary_color
                        : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                    child: Obx(() {
                      return CircleAvatar(
                          radius: 30,
                          backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                              ? AppColors.primary_color
                              : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                          backgroundImage: Image.network(
                            "${user.value?.profilePic}",
                            errorBuilder: (x, y, z) {
                              return const Icon(
                                Icons.person,
                                size: 48,
                              );
                            },
                          ).image);
                    }),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextButton(
                        onPressed: () {
                          pickImageFromGallery(context);
                        },
                        child: Text('Change avatar',
                            style: subtitle1.copyWith(
                                color: Storage.instance.getValue(Constants.colorCode) == null
                                    ? AppColors.primary_color
                                    : hexToColor(Storage.instance.getValue(Constants.colorCode))))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(8),
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          // width: 0.0 produces a thin "hairline" border
                          borderSide: BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(
                              color: Storage.instance.getValue(Constants.colorCode) == null
                                  ? AppColors.primary_color
                                  : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                              width: 1),
                        ),
                        hintText: 'Your name',
                        hintStyle: subtitle1,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
                    child: TextField(
                      controller: emailController,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(8),
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          // width: 0.0 produces a thin "hairline" border
                          borderSide: BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(Radius.circular(8)),
                          borderSide: BorderSide(
                              color: Storage.instance.getValue(Constants.colorCode) == null
                                  ? AppColors.primary_color
                                  : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                              width: 1),
                        ),
                        hintText: 'Your name',
                        hintStyle: subtitle1,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
                    child: Row(
                      children: [
                        Expanded(
                          child: PrimaryButton(
                            text: 'UPDATE',
                            onTap: () {
                              updateProfileApi();
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }).whenComplete(() {
      isProfileFromLocal.value = false;
      nameController.text = '';
      emailController.text = '';
    });
  }

  void pickImageFromGallery(BuildContext context) async {
    // Pick an image
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      // profilePic.value = File(image.path);
      isProfileFromLocal.value = true;
      uploadImage(image.path, context);
    } else {
      SnackBarFailure(titleText: 'Oops..', messageText: 'Unable to pick image from gallery.. Try again');
    }
  }

  void updateProfileApi() async {
    var data = {
      'Name': nameController.text,
      'Contact1': user.value!.mobileNo,
      'Email': emailController.text,
      'is_register': 1,
    };
    var result = await XHttp.request(Endpoints.r_update_profile, method: XHttp.POST, data: data);
    if (result.code == 200) {
      var response = ProfileUpdateResponse.fromJson(jsonDecode(result.data));
      SnackBarSuccess(titleText: "Success", messageText: "Profile updated successfully").show();
      user.value?.name = nameController.text;
      user.value?.email = emailController.text;
      saveUsrDataToDB(user.value);
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }
  }

  uploadImage(String file, BuildContext context) async {
    String fileName = file.split('/').last;

    dio.FormData formData = dio.FormData.fromMap({
      "customerId": user.value!.userId,
      "profileImage": await dio.MultipartFile.fromFile(file, filename: fileName),
    });
    Navigator.pop(context);

    var result = await XHttp.fileRequest(Endpoints.c_profile_upload, method: XHttp.POST, data: formData);
    if (result.code == 200) {
      var response = ProfileUpdatedResponse.fromJson(jsonDecode(result.data));
      SnackBarSuccess(titleText: "Success", messageText: "Profile updated successfully").show();
      // user.value?.name = nameController.text;
      profilePic.value = response.data!.image!;
      user.value?.profilePic = response.data?.image;
      saveUsrDataToDB(user.value);
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }
  }

  Future<void> saveUsrDataToDB(User? user) async {
    box.setValue(Constants.userdb, jsonEncode(user?.toJson()));
    Logger().e((box.getValue(Constants.userdb)));
  }
}
