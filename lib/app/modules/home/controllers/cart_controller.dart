import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/CartChangeItemResponse.dart';
import 'package:markkito_customer/app/data/models/CartCouponResponse.dart';
import 'package:markkito_customer/app/data/models/CartResponse.dart';
import 'package:markkito_customer/app/data/models/CartStatusResponse.dart';
import 'package:markkito_customer/app/data/models/ConfirmOrderResponse.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart' as HomePageResponse;
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class CartController extends GetxController {
  List item = [
    {"title": "Button One", "color": 50},
    {"title": "Button Two", "color": 100},
    {"title": "Button Three", "color": 200},
    {"title": "No show", "color": 0, "hide": '1'},
  ];

  List deliveryTypes = [
    'Fast Delivery',
    'Scheduled',
    'Take Away',
  ].obs;

  List deliveryTips = [
    'AED 3',
    'AED 5',
    'AED 7',
    'AED 9',
  ].obs;

  var couponController = TextEditingController();

  var defaultDeliveryTypeChoiceIndex = 0.obs;
  var defaultDeliveryTipIndex = 0.obs;

  var selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();
  var formattedShceduledDate = '';
  var formattedShceduledTime = '';

  TextEditingController textController = TextEditingController();
  TextEditingController textTimeController = TextEditingController();

  chooseDate() async {
    DateTime? pickedDate = await showDatePicker(
        context: Get.context!,
        initialDate: selectedDate,
        firstDate: selectedDate,
        lastDate: DateTime(2024),
        //initialEntryMode: DatePickerEntryMode.input,
        // initialDatePickerMode: DatePickerMode.year,
        helpText: 'Select Delivery Date',
        cancelText: 'CLOSE',
        confirmText: 'CONFIRM',
        errorFormatText: 'Enter valid date',
        errorInvalidText: 'Enter valid date range',
        selectableDayPredicate: disableDate,
        builder: (BuildContext context, Widget? child) {
          return Theme(
            data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.dark(
                primary: AppColors.primary_color,
                onPrimary: Colors.white,
                surface: Colors.white,
                onSurface: Colors.black,
              ),
              dialogBackgroundColor: Colors.white,
            ),
            child: child!,
          );
        });
    if (pickedDate != null && pickedDate != selectedDate) {
      selectedDate = pickedDate;
      textController.text = DateFormat("dd MMMM,yyyy").format(selectedDate).toString();
      formattedShceduledDate = DateFormat("yyyy-MM-dd,").format(selectedDate).toString();
      log(formattedShceduledDate);
    }
  }

  bool disableDate(DateTime day) {
    if ((day.isAfter(DateTime.now().subtract(const Duration(days: 1))) &&
        day.isBefore(DateTime.now().add(const Duration(days: 30))))) {
      return true;
    }
    return false;
  }

  chooseTime() async {
    final TimeOfDay? picked_s = await showTimePicker(
        context: Get.context!,
        initialTime: selectedTime,
        builder: (BuildContext context, Widget? child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false),
            child: child!,
          );
        });

    if (picked_s != null) {
      selectedTime = picked_s;
      print('ppick_s ${picked_s.format(Get.context!)}');
      textTimeController.text = picked_s.format(Get.context!);
      formattedShceduledTime = '${picked_s.hour}:${picked_s.minute}:00';
      log(formattedShceduledTime);
    }
  }

  CartData? cartData;
  var cartProductItems = <CartProductItems>[].obs;
  var deliveryMethods = <DeliveryMethod>[].obs;
  var deliveryAddress = <DeliveryAddress>[].obs;
  var deliveryType = <DeliveryType>[].obs;
  var tips = <Tips>[].obs;
  var isLoading = true.obs;
  var isCouponApplied = false.obs;

  NumberFormat numberFormat = NumberFormat("#,##0.00", "en_US");

  // var totalSaving = 0.obs;
  // var grandTotal = 0.obs;
  Rx<double> totalSaving = Rx<double>(0.0);
  Rx<double> grandTotal = Rx<double>(0.0);
  Rx<DeliveryMethod?> selectedDeliveryMethod = Rx<DeliveryMethod?>(null);
  Rx<DeliveryAddress?> selectedDeliveryAddress = Rx<DeliveryAddress?>(null);
  Rx<DeliveryType?> selectedDeliveryType = Rx<DeliveryType?>(null);
  Rx<Tips?> selectedTips = Rx<Tips?>(null);

  ConfirmOrderResponse? confirmOrderResponse;

  @override
  void onInit() {
    super.onInit();
    // getCartDetails();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getCartDetails() async {
    var data = {};
    var result = await XHttp.request(
      Endpoints.r_ProductCart,
      method: XHttp.POST,
    );
    var cartDataResponse = CartResponse.fromJson(jsonDecode(result.data));
    if (cartDataResponse.statusCode == 200) {
      if (cartDataResponse.cartData != null) {
        cartData = cartDataResponse.cartData;
        cartProductItems.clear();
        deliveryMethods.clear();
        deliveryAddress.clear();
        deliveryType.clear();
        tips.clear();
        selectedDeliveryMethod.value = null;
        selectedDeliveryAddress.value = null;
        selectedDeliveryType.value = null;
        selectedTips.value = null;
        totalSaving.value = 0;
        grandTotal.value = 0;
        isCouponApplied.value = false;
        if (cartDataResponse.cartData!.cartProductItems!.isNotEmpty) {
          cartProductItems.addAll(cartDataResponse.cartData!.cartProductItems!);
        }
        deliveryMethods.addAll(cartDataResponse.cartData!.deliveryMethod!);
        if (cartDataResponse.cartData!.deliveryMethod!.isNotEmpty) {
          selectedDeliveryMethod.value = cartDataResponse.cartData!.deliveryMethod?[0];
        }
        if (cartDataResponse.cartData!.deliveryAddress!.isNotEmpty) {
          selectedDeliveryAddress.value = cartDataResponse.cartData!.deliveryAddress?[0];
        }
        if (cartDataResponse.cartData!.deliveryType!.isNotEmpty) {
          selectedDeliveryType.value = cartDataResponse.cartData!.deliveryType?[0];
        }
        if (cartDataResponse.cartData!.tips!.isNotEmpty) {
          selectedTips.value = cartDataResponse.cartData!.tips?[0];
        }
        deliveryAddress.addAll(cartDataResponse.cartData!.deliveryAddress!);
        deliveryType.addAll(cartDataResponse.cartData!.deliveryType!);
        tips.addAll(cartDataResponse.cartData!.tips!);
        grandTotal.value = double.parse('${cartDataResponse.cartData!.billDetails!.grandTotal}');
        isLoading.value = false;

        if (cartProductItems.isNotEmpty) {
          var cartData = HomePageResponse.CartDatas()
            ..total_amount = grandTotal.value
            ..total_qty = cartProductItems.length
            ..total_savings = totalSaving.value;

          Get.find<HomeContentController>().updateCartData(
            cartData: cartData,
            isAdded: false,
          );
        } else {
          Get.find<HomeContentController>().updateCartData(
            cartData: null,
            isAdded: false,
          );
        }
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void changeDeliveryMethod(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingExtraLarge),
            child: Container(
              height: Get.height * .5,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Change Delivery Method',
                      style: subtitle1,
                    ),
                  ),
                  Column(
                    children: List.generate(deliveryMethods.length, (index) {
                      var item = deliveryMethods[index];
                      return InkWell(
                        customBorder: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onTap: () {
                          selectedDeliveryMethod.value = item;
                          Get.back();
                        },
                        child: ListTile(
                          leading: Icon(Icons.bike_scooter),
                          title: Text('${item.name}'),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void changeDeliveryAddress(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingExtraLarge),
            child: Container(
              height: Get.height * .5,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Change Delivery Address',
                      style: subtitle1,
                    ),
                  ),
                  Column(
                    children: List.generate(deliveryAddress.length, (index) {
                      var item = deliveryAddress[index];
                      return InkWell(
                        customBorder: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onTap: () {
                          selectedDeliveryAddress.value = item;
                          Get.back();
                        },
                        child: ListTile(
                          leading: Icon(Icons.location_pin),
                          title: Text('${item.placeText}'),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void deleteFromCartPromptDialog(int productId, BuildContext context) {
    Widget cancelButton = TextButton(
      child: const Text("Cancel"),
      onPressed: () {
        Get.back();
      },
    );
    Widget continueButton = TextButton(
      child: const Text("Confirm"),
      onPressed: () {
        Get.back();
        deleteItemFromCart(productId);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Delete?"),
      content: const Text("Would you like to delete item from cart?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void changeItemFromCart(product_id, qty) async {
    var data = {
      'productId': product_id,
      'quantity': qty,
    };
    var result = await XHttp.request(Endpoints.c_ChangeCartItem, method: XHttp.POST, data: data);
    var addToCartResponse = CartChangeItemResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      getCartDetails();
      // SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void deleteItemFromCart(product_id) async {
    var data = {
      'productId': product_id,
      'status': 'remove',
      'quantity': 0,
      'varient_id': '',
      'choice_of_crust_id': '',
      'topping_id': '',
    };
    var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
    var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      getCartDetails();
      SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void applyCoupon() async {
    var data = {
      'CouponCode': '${couponController.value.text}',
      'amount': cartData?.billDetails?.grandTotal,
    };
    var result = await XHttp.request(Endpoints.r_get_coupon_details, method: XHttp.POST, data: data);
    var addToCartResponse = CartCouponResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      if (addToCartResponse.data!.couponDetails!.status == "success") {
        totalSaving.value = addToCartResponse.data!.couponDetails!.redeemRate!;
        grandTotal.value = addToCartResponse.data!.couponDetails!.finalAmount!;
        isCouponApplied.value = true;
        SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.couponDetails?.message}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "${addToCartResponse.data?.couponDetails?.message}").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void clearCoupon() {
    couponController.clear();
    isCouponApplied.value = false;
    totalSaving.value = 0.0;
    grandTotal.value = cartData?.billDetails?.grandTotal;
  }

  void confirmOrder() async {
    log('${selectedDeliveryType.value?.name} - ${selectedDeliveryType.value?.id}');
    if (selectedDeliveryType.value?.name == "SCHEDULED DELIVERY") {
      if (formattedShceduledDate.isEmpty || formattedShceduledTime.isEmpty) {
        SnackBarFailure(titleText: "Oops..", messageText: "Invalid scheduled date and time").show();
        return;
      }
    }
    var selProductList = [];
    var shopId = 0;
    for (var element in cartProductItems) {
      var prod = {'id': element.productId, 'qty': element.count};
      shopId = element.shopId!;
      selProductList.add(prod);
    }

    var data = {
      'shop_id': shopId,
      'ProductList': jsonEncode(selProductList),
      'Delivery_address_id': selectedDeliveryAddress.value?.id,
      'Delivery_method_id': selectedDeliveryMethod.value?.id,
      'Delivery_type_id': selectedDeliveryType.value?.id,
      'Tip': selectedTips.value == null ? 0 : selectedTips.value?.amount,
      'Scheduled_date': selectedDeliveryType.value?.name == "SCHEDULED DELIVERY" ? formattedShceduledDate : '',
      'Scheduled_time': selectedDeliveryType.value?.name == "SCHEDULED DELIVERY" ? formattedShceduledTime : '',
    };
    var result = await XHttp.request(Endpoints.c_confirm_order, method: XHttp.POST, data: data);

    var confirmOrderRespon = ConfirmOrderResponse.fromJson(jsonDecode(result.data));
    if (confirmOrderRespon.statusCode == 200) {
      confirmOrderResponse = confirmOrderRespon;
      Get.toNamed(Routes.CONFIRM_ORDER);
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Unable to create Order. Try again.").show();
      isLoading.value = false;
    }
  }
}
