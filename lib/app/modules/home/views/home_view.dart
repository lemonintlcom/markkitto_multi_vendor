import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/app_text.dart';
import 'package:markkito_customer/app/modules/home/controllers/cart_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/generated/assets.dart';

import '../../custom_widgets/cart_overview_home.dart';
import '../controllers/home_controller.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final controller = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {

    bool keyboardIsOpened = MediaQuery.of(context).viewInsets.bottom != 0.0;
    return Scaffold(
      body: SafeArea(
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Column(
              children: [
                Expanded(
                  /*child: Obx(() {
                    return FadeIndexedStack(
                      children: [
                        MarketsView(),
                        StoresView(),
                        HomeContentView(),
                        CartView(),
                        MoreView(),
                      ],
                      index: controller.selectedIndex.value,
                    );
                  }),*/
                  child: PageStorage(
                    bucket: controller.pageStorageBucket,
                    child: Obx(() {
                      return controller.screenList[controller.selectedIndex.value];
                    }),
                  ),
                  /* child: Obx(() {
                    return controller.screenList[controller.selectedIndex.value];
                  }),*/
                ),
                Obx(() {
                  return Visibility(
                    visible: controller.cartWindowVisibilty.value,
                    child: CartOverviewHome(),
                  );
                }),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: keyboardIsOpened
          ? Container()
          : FloatingActionButton(
              elevation: 0,
              shape: const StadiumBorder(
                side: BorderSide(color: Colors.white, width: 1),
              ),
              backgroundColor: AppColors.primary_color,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(100),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.primary_color.withOpacity(0.3),
                      spreadRadius: 12,
                      blurRadius: 7,
                      offset: Offset(0, 8),
                    ),
                  ],
                ),
                child: SvgPicture.asset(
                  Assets.svgHome,
                  height: 24,
                ),
              ),
              onPressed: () {
                controller.cartWindowVisibilty.value = true;
                controller.selectIndex(2);
              },
            ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.only(left: 12.0, right: 12.0, bottom: 4, top: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: BottomAppBarItem(Assets.svgMarkets, 'Markets', 0, () {
                controller.selectIndex(0);
                controller.cartWindowVisibilty.value = false;
              })),
              Expanded(
                  child: BottomAppBarItem(Assets.svgStores, 'Stores', 1, () {
                controller.selectIndex(1);
                controller.cartWindowVisibilty.value = false;
              })),
              Expanded(child: BottomAppBarItem(null, 'Home', 2, null)),
              Expanded(
                  child: BottomAppBarItem(Assets.svgCart, 'Cart', 3, () {
                controller.selectIndex(3);
                controller.cartWindowVisibilty.value = false;
                Future.delayed(const Duration(milliseconds: 200), () {
                  Get.find<CartController>().getCartDetails();
                });

              })),
              Expanded(
                  child: BottomAppBarItem(Assets.svgMore, 'More', 4, () {
                controller.selectIndex(4);
                controller.cartWindowVisibilty.value = false;
              })),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  SizedBox BottomAppBarItem(
    String? icon,
    String title,
    int selectedTab,
    VoidCallback? onTap,
  ) {
    return SizedBox(
      height: 55,
      child: InkWell(
        customBorder: const CircleBorder(),
        onTap: onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            icon != null
                ? Obx(() {
                    return SvgPicture.asset(
                      icon,
                      height: 24,
                      color: controller.selectedIndex.value == selectedTab ? AppColors.primary_color : Colors.grey,
                    );
                  })
                : const Icon(
                    Icons.store,
                    color: Colors.transparent,
                  ),
            const SizedBox(
              height: 4,
            ),
            Obx(() {
              return AppText(
                text: title,
                fontSize: 13,
                textColor: controller.selectedIndex.value == selectedTab ? AppColors.primary_color : Colors.grey,
              );
            })
          ],
        ),
      ),
    );
  }
}
