import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/app/modules/custom_widgets/category_item.dart';
import 'package:markkito_customer/app/modules/custom_widgets/market_item_card.dart';
import 'package:markkito_customer/app/modules/custom_widgets/shop_item_card.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../custom_widgets/slider_card_item.dart';

class HomeContentView extends StatelessWidget {
  const HomeContentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(HomeContentController());
    return Padding(
      padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
      child: Obx(() {
        return controller.isLoading.value && controller.homeData == null
            ? SizedBox(
                child: const Center(child: CircularProgressIndicator()),
                height: 100,
                width: Get.width,
              )
            : SingleChildScrollView(
                controller: controller.scrollController,
                key: const PageStorageKey('HomeContentView_SingleChildScrollView'),
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Text(
                            'Markkito',
                            style: headline5.copyWith(color: AppColors.primary_color, fontWeight: FontWeight.bold),
                          ),
                        ),
                        // const AppText(
                        //   text: 'Markkito',
                        //   textColor: AppColors.primary_color,
                        //   fontSize: 28,
                        //   fontWeight: FontWeight.bold,
                        // ),
                        InkWell(
                          onTap: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                Assets.svgLocation,
                                height: 24,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              InkWell(
                                onTap: () {
                                  Get.toNamed(Routes.SELECT_LOCATION_MAP,
                                      arguments: {'fromHome': true, 'toLogin': false});
                                },
                                child: SizedBox(
                                  width: Get.width * .3,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        ' Delivery to',
                                        style: caption,
                                      ),
                                      ValueListenableBuilder(
                                          valueListenable: Hive.box(Constants.configName).listenable(),
                                          builder: (context, Box box, _) {
                                            return Text(
                                              '${box.get(Constants.locationText)}',
                                              style: body2,
                                              maxLines: 1,
                                              softWrap: false,
                                              overflow: TextOverflow.fade,
                                            );
                                          }),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ), //Top bar
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    TextField(
                      readOnly: true,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(2),
                        enabledBorder: const OutlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide: BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primary_color, width: 1),
                        ),
                        hintText: 'Search here',
                        hintStyle: subtitle1,
                        prefixIcon: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: SvgPicture.asset(
                            Assets.svgSearch,
                            height: 10,
                          ),
                        ),
                        suffixIcon: InkWell(
                          onTap: () {
                            Get.toNamed(Routes.SEARCH);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: SvgPicture.asset(
                              Assets.svgBarcode,
                              height: 10,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: paddingExtraLarge,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          Assets.svgStoreSmall,
                          height: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Text(
                          'Latest Offers',
                          style: headline6.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    SizedBox(
                      height: 180,
                      child: Obx(() {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.bannerList.length,
                            physics: const BouncingScrollPhysics(),
                            itemBuilder: (ctx, index) {
                              var bannerItem = controller.bannerList[index];
                              return SliderCartItem(
                                bannerItem: bannerItem,
                              );
                            });
                      }),
                    ),
                    const SizedBox(
                      height: paddingExtraLarge,
                    ),
                    Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade300),
                    const SizedBox(
                      height: paddingExtraLarge,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Categories',
                          style: headline6.copyWith(fontWeight: FontWeight.bold),
                        ),
                        InkWell(
                          onTap: () {},
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'SEE ALL',
                                style: body2,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              SvgPicture.asset(
                                Assets.svgRightRoundedArrow,
                                height: 20,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    SizedBox(
                      height: 130 + 130,
                      child: Obx(() {
                        return GridView.builder(
                            itemCount: controller.categoryList.length,
                            physics: const BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 1.2,
                            ),
                            itemBuilder: (context, index) {
                              var categoryItem = controller.categoryList[index];
                              return Padding(
                                padding: const EdgeInsets.only(right: 18.0, bottom: 0),
                                child: CategoryItem(
                                  categoryItem: categoryItem,
                                ),
                              );
                            });
                      }),
                    ),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade300),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              Assets.svgStoreSmall,
                              height: 20,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Nearest Shops',
                              style: headline6.copyWith(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        InkWell(
                          onTap: () {},
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'SEE ALL',
                                style: body2,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              SvgPicture.asset(
                                Assets.svgRightRoundedArrow,
                                height: 20,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    SizedBox(
                      height: 160,
                      child: Obx(() {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.shopList.length,
                            physics: const BouncingScrollPhysics(),
                            itemBuilder: (ctx, index) {
                              var shop = controller.shopList[index];
                              return ShopItemCard(
                                shop: shop,
                              );
                            });
                      }),
                    ),
                    Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade300),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              Assets.svgStoreSmall,
                              height: 20,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Nearest Markets',
                              style: headline6.copyWith(fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        InkWell(
                          onTap: () {},
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'SEE ALL',
                                style: body2,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              SvgPicture.asset(
                                Assets.svgRightRoundedArrow,
                                height: 20,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    SizedBox(
                      height: 200,
                      child: Obx(() {
                        return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.marketList.length,
                            physics: const BouncingScrollPhysics(),
                            itemBuilder: (ctx, index) {
                              var market = controller.marketList[index];
                              return MarketItemCard(
                                market: market,
                              );
                            });
                      }),
                    ),
                    const SizedBox(
                      height: paddingExtraLarge,
                    ),
                  ],
                ),
              );
      }),
    );
  }
}
