import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/shop_item_card.dart';
import 'package:markkito_customer/app/modules/home/controllers/stores_controller.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../../../constants/dimens.dart';

class StoresView extends StatelessWidget {
  const StoresView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(StoresController());
    return Padding(
      padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
      child: Column(
        children: [
          const SizedBox(
            height: paddingLarge,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                Assets.svgStoreSmall,
                height: 20,
              ),
              const SizedBox(
                width: 8,
              ),
              Text(
                'Nearest Stores',
                style: headline6.copyWith(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          const SizedBox(
            height: paddingLarge,
          ),
          Expanded(
            child: Obx(() {
              return ListView.builder(
                controller: controller.scrollController,
                  scrollDirection: Axis.vertical,
                  itemCount: controller.shopList.length,
                  physics: const BouncingScrollPhysics(),
                  itemBuilder: (ctx, index) {
                    return Padding(
                      key: ObjectKey(controller.shopList[index]),
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: SizedBox(
                        height: 150,
                        child: ShopItemCard(
                          isFullWidth: true,
                          shop: controller.shopList[index],
                        ),
                      ),
                    );
                  });
            }),
          ),
        ],
      ),
    );
  }
}
