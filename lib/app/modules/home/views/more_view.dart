import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/more_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../../constants/dimens.dart';
import '../../../../generated/assets.dart';
import '../../custom_widgets/bottom_sheet_modal.dart';
import 'package:share_plus/share_plus.dart';

class MoreView extends StatelessWidget {
  const MoreView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(MoreController());
    return Padding(
      padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
      child: Column(
        children: [
          const SizedBox(
            height: paddingLarge,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 0, right: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${Storage.box.get(Constants.displayName)}',
                      style: headline5.copyWith(
                          color: Storage.instance.getValue(Constants.colorCode) == null
                              ? AppColors.primary_color
                              : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                InkWell(
                  onTap: () {
                    Get.find<HomeController>().isLoggedIn ? controller.profileUpdate(context) : controller.logout();
                  },
                  child: Row(
                    children: [
                      Text(
                        Get.find<HomeController>().isLoggedIn ? 'Edit Profile' : 'Login',
                        style: body1.copyWith(
                            color: Storage.instance.getValue(Constants.colorCode) == null
                                ? AppColors.primary_color
                                : hexToColor(Storage.instance.getValue(Constants.colorCode))),
                      ),
                      // Icon(
                      //   Icons.play_arrow,
                      //   color: Storage.instance.getValue(Constants.colorCode) == null
                      //       ? AppColors.primary_color
                      //       : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                      // ),
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            height: paddingLarge,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0),
                        child: CircleAvatar(
                          radius: 32,
                          backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                              ? AppColors.primary_color
                              : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                          child: Obx(() {
                            return CircleAvatar(
                                radius: 30,
                                backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                                    ? AppColors.primary_color
                                    : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                backgroundImage: Image.network(
                                  controller.profilePic.value,
                                  errorBuilder: (x, y, z) {
                                    return const Icon(
                                      Icons.person,
                                      size: 48,
                                    );
                                  },
                                ).image
                                // child: Obx(() {
                                //   return Image.network("${controller.user.value?.profilePic}", errorBuilder: (x, y, z) {
                                //     return const Icon(
                                //       Icons.person,
                                //       size: 48,
                                //     );
                                //   });
                                // }),
                                );
                          }),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 0, top: 0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Get.find<HomeController>().isLoggedIn
                                        ? Obx(() {
                                            return Text(
                                              '${controller.user.value?.name}',
                                              style: headline6.copyWith(fontWeight: FontWeight.bold),
                                            );
                                          })
                                        : Text('Hello Guest', style: headline6.copyWith(fontWeight: FontWeight.bold))),
                                Get.find<HomeController>().isLoggedIn
                                    ? Obx(() {
                                        return Text(
                                          '+${Storage.instance.getValue(Constants.countryMobileCode)}-${controller.user.value!.mobileNo}',
                                          style: subtitle1.copyWith(fontWeight: FontWeight.w100),
                                        );
                                      })
                                    : Container(),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                  const Divider(),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                  GridView.count(
                    crossAxisCount: 3,
                    childAspectRatio: 1.2,
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: <Widget>[
                      buildMoreMenuGrid(Icons.list_alt_outlined,
                          // icon: Assets.svgCartOutline,
                          title: 'Orders', onTap: () {
                        Get.find<HomeController>().isLoggedIn
                            ? Get.toNamed(Routes.ORDERS)
                            : BottomSheetModal().showLoginRequired(context);
                      }),
                      buildMoreMenuGrid(Icons.favorite_border_outlined,
                          // icon: Assets.svgOfferOutline,
                          title: 'Wishlist', onTap: () {
                        Get.find<HomeController>().isLoggedIn
                            ? Get.toNamed(Routes.WISHLIST)
                            : BottomSheetModal().showLoginRequired(context);
                      }),
                      buildMoreMenuGrid(Icons.notifications_active_outlined,
                          // icon: Assets.svgOfferOutline,
                          title: 'Notifications', onTap: () {
                        Get.find<HomeController>().isLoggedIn
                            ? Get.toNamed(Routes.NOTIFICATIONS)
                            : BottomSheetModal().showLoginRequired(context);
                      }),
                      buildMoreMenuGrid(Icons.local_offer_outlined,
                          // icon: Assets.svgOfferOutline,
                          title: 'Offers', onTap: () {
                        Get.toNamed(Routes.OFFERS);
                      }),
                      buildMoreMenuGrid(Icons.location_city_outlined,
                          // icon: Assets.svgHomeLocation,
                          title: 'Address', onTap: () {
                        Get.find<HomeController>().isLoggedIn
                            ? Get.toNamed(Routes.ADDRESS_BOOK)
                            : BottomSheetModal().showLoginRequired(context);
                      }),
                      buildMoreMenuGrid(
                        Icons.star_border_outlined,
                        // Assets.svgHomeLocation,
                        title: 'Reviews',
                        onTap: () {
                          Get.find<HomeController>().isLoggedIn
                              ? Get.toNamed(Routes.REVIEW)
                              : BottomSheetModal().showLoginRequired(context);
                        },
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                  const Divider(),
                  Obx(() {
                    return SwitchListTile.adaptive(
                      subtitle: const Text('Turn ON / OFF app notifications'),
                      title: Text(
                        'Notifications',
                        style: subtitle1.copyWith(color: Colors.black, fontSize: 20),
                      ),
                      value: controller.isNotification.value,
                      onChanged: (changed) {
                        controller.isNotification.value = changed;
                      },
                    );
                  }),
                  const Divider(),
                  InkWell(
                    onTap: () {
                      // Get.toNamed(Routes.ABOUT);
                    },
                    child: SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Row(
                          children: [
                            const Icon(Icons.language),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'App Language',
                              style: body1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      controller.launchURL();
                    },
                    child: SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Row(
                          children: [
                            const Icon(Icons.feedback_outlined),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Feedback',
                              style: body1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      controller.launchCaller(
                          '${Storage.instance.getValue(Constants.countryMobileCode)}${Storage.instance.getValue(Constants.contact1)}');
                    },
                    child: SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Row(
                          children: [
                            const Icon(Icons.phone_android_outlined),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Contact us',
                              style: body1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {},
                    child: SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Row(
                          children: [
                            const Icon(Icons.rate_review_outlined),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Rate app',
                              style: body1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      var shareLinkAndroid =
                          'https://play.google.com/store/apps/details?id=${Get.find<HomeController>().packageInfo?.packageName}';
                      var shareLinkIOS =
                          'https://play.google.com/store/apps/details?id=${Get.find<HomeController>().packageInfo?.packageName}';
                      Share.share(
                          'check out ${Storage.instance.getValue(Constants.displayName)} App \n $shareLinkAndroid');
                    },
                    child: SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Row(
                          children: [
                            const Icon(Icons.share_outlined),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'Share App',
                              style: body1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed(Routes.ABOUT);
                    },
                    child: SizedBox(
                      width: Get.width,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                        child: Row(
                          children: [
                            const Icon(Icons.info_outline),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              'About App',
                              style: body1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Get.find<HomeController>().isLoggedIn
                      ? InkWell(
                          onTap: () async {
                            controller.logout();
                          },
                          child: SizedBox(
                            width: Get.width,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 8),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.logout_outlined,
                                    color: Storage.instance.getValue(Constants.colorCode) == null
                                        ? AppColors.primary_color
                                        : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    'Logout',
                                    style: body1.copyWith(
                                      color: Storage.instance.getValue(Constants.colorCode) == null
                                          ? AppColors.primary_color
                                          : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  const SizedBox(
                    height: paddingLarge,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildMoreMenuGrid(IconData icon, {title, VoidCallback? onTap}) {
    return SizedBox(
      height: 100,
      width: 100,
      child: InkWell(
        onTap: onTap,
        child: Card(
          child: Stack(
            children: [
              Positioned(
                left: 0,
                top: 0,
                child: Opacity(
                  opacity: .2,
                  child: Icon(
                    icon,
                    size: 50,
                    color: Storage.instance.getValue(Constants.colorCode) == null
                        ? AppColors.primary_color.withOpacity(.4)
                        : hexToColor(Storage.instance.getValue(Constants.colorCode)).withOpacity(.4),
                  ),
                ),
              ),
              Positioned(
                bottom: 4,
                left: 4,
                child: Text(
                  '$title',
                  style: headline6.copyWith(
                      color: Colors.black.withOpacity(.7), fontSize: 16, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  InkWell buildMoreMenuColumn(IconData icon, {title, VoidCallback? onTap}) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0, bottom: 8),
        child: Row(
          children: [
            Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                color: Storage.instance.getValue(Constants.colorCode) == null
                    ? AppColors.primary_color.withOpacity(.3)
                    : hexToColor(Storage.instance.getValue(Constants.colorCode)).withOpacity(.3),
                borderRadius: const BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Icon(
                  icon,
                ),
              ),
            ),
            const SizedBox(
              width: paddingLarge,
            ),
            Text(
              title,
              style: subtitle1.copyWith(color: Colors.black),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMoreMenuRow({String? icon, String? title, VoidCallback? onTap}) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Column(
            children: [
              SvgPicture.asset(
                icon!,
                height: 22,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  title!,
                  style: subtitle2,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
