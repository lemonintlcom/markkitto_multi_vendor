import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/home/controllers/cart_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/functions.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../../../../constants/dimens.dart';
import '../../../../generated/assets.dart';
import '../../custom_widgets/cart_item.dart';

class CartView extends StatelessWidget {
  const CartView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(CartController());
    return Obx(() {
      return controller.isLoading.value
          ? SizedBox(
              child: const Center(child: CircularProgressIndicator()),
              height: 100,
              width: Get.width,
            )
          : controller.cartProductItems.isEmpty
              ? SizedBox(
                  child: const Center(child: Text('Cart Empty')),
                  height: 100,
                  width: Get.width,
                )
              : Column(
                  children: [
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            Assets.svgStoreSmall,
                            height: 20,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Cart',
                            style: headline6.copyWith(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: paddingLarge,
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        key: const PageStorageKey('CartView_SingleChildScrollView'),
                        physics: const BouncingScrollPhysics(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Items',
                                    style: body3.copyWith(fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    height: paddingMedium,
                                  ),
                                  Obx(() {
                                    return Column(
                                      children: List.generate(controller.cartProductItems.length, (index) {
                                        var cartProductItem = controller.cartProductItems[index];
                                        return CartItem(
                                          cartProductItems: cartProductItem,
                                        );
                                      }),
                                    );
                                  }),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  SizedBox(
                                    width: 30,
                                    child: SvgPicture.asset(Assets.svgBikeIcon),
                                  ),
                                  const SizedBox(
                                    width: paddingLarge,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Delivery Method',
                                          style: captionLite.copyWith(color: Colors.grey),
                                        ),
                                        Obx(() {
                                          return Text(
                                            '${controller.selectedDeliveryMethod.value?.name}',
                                            style: body1.copyWith(color: Colors.black),
                                          );
                                        }),
                                      ],
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      controller.changeDeliveryMethod(context);
                                    },
                                    child: Text(
                                      'CHANGE',
                                      style: body2.copyWith(color: Colors.red),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: 30,
                                    child: SvgPicture.asset(Assets.svgLocationInnerGreen),
                                  ),
                                  const SizedBox(
                                    width: paddingLarge,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Delivery Address',
                                          style: captionLite.copyWith(color: Colors.grey),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(top: 8.0, right: 8),
                                          child: Obx(() {
                                            return Text(
                                              '${controller.selectedDeliveryAddress.value?.placeText}',
                                              style: subtitleLite.copyWith(fontSize: 14),
                                            );
                                          }),
                                        ),
                                      ],
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      controller.changeDeliveryAddress(context);
                                    },
                                    child: Text(
                                      'CHANGE',
                                      style: body2.copyWith(color: Colors.red),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: paddingExtraLarge,
                            ),
                            Container(
                              color: Colors.grey.shade200,
                              width: Get.width,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: paddingLarge, right: paddingLarge, top: paddingLarge, bottom: paddingLarge),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Choose Delivery Type',
                                      style: captionLite.copyWith(color: Colors.grey),
                                    ),
                                    const SizedBox(
                                      height: paddingMedium,
                                    ),
                                    Obx(() {
                                      return Wrap(
                                        spacing: 8,
                                        children: List.generate(controller.deliveryType.length, (index) {
                                          return ChoiceChip(
                                            labelPadding: const EdgeInsets.all(2.0),
                                            label: Text(
                                              controller.deliveryType[index].name!,
                                              style: captionLite.copyWith(
                                                  color: controller.selectedDeliveryType.value?.id ==
                                                          controller.deliveryType[index].id
                                                      ? AppColors.white
                                                      : AppColors.black1),
                                            ),
                                            selected:
                                                controller.selectedDeliveryType.value == controller.deliveryType[index],
                                            selectedColor: AppColors.primary_color,
                                            pressElevation: 0,
                                            backgroundColor: AppColors.white,
                                            onSelected: (value) {
                                              controller.selectedDeliveryType.value = value
                                                  ? controller.deliveryType[index]
                                                  : controller.selectedDeliveryType.value;
                                            },
                                            // backgroundColor: color,
                                            elevation: 1,
                                            padding: const EdgeInsets.symmetric(horizontal: paddingLarge),
                                          );
                                        }),
                                      );
                                    }),
                                    const SizedBox(
                                      height: paddingMedium,
                                    ),
                                    Container(
                                      child: controller.selectedDeliveryType.value?.name == "SCHEDULED DELIVERY"
                                          ? Row(
                                              children: [
                                                Expanded(
                                                  child: Column(
                                                    mainAxisSize: MainAxisSize.min,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        'Date',
                                                        style: captionLite.copyWith(color: Colors.grey),
                                                      ),
                                                      const SizedBox(
                                                        height: 8,
                                                      ),
                                                      Flexible(
                                                        child: InkWell(
                                                          onTap: () {
                                                            controller.chooseDate();
                                                          },
                                                          child: TextField(
                                                            enabled: false,
                                                            style: subtitle2,
                                                            controller: controller.textController,
                                                            decoration: InputDecoration(
                                                              filled: true,
                                                              fillColor: AppColors.white,
                                                              contentPadding: const EdgeInsets.all(8),
                                                              disabledBorder: const OutlineInputBorder(
                                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                // width: 0.0 produces a thin "hairline" border
                                                                borderSide: BorderSide(color: Colors.black, width: 0.2),
                                                              ),
                                                              hintText: 'Select Date',
                                                              hintStyle: subtitle1,
                                                              suffixIcon: Padding(
                                                                padding: const EdgeInsets.all(12.0),
                                                                child: Icon(
                                                                  Icons.calendar_today,
                                                                  color: Colors.grey.shade300,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: paddingMedium,
                                                ),
                                                Expanded(
                                                  child: Column(
                                                    mainAxisSize: MainAxisSize.min,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        'Time',
                                                        style: captionLite.copyWith(color: Colors.grey),
                                                      ),
                                                      const SizedBox(
                                                        height: 8,
                                                      ),
                                                      Flexible(
                                                        child: InkWell(
                                                          onTap: () {
                                                            controller.chooseTime();
                                                          },
                                                          child: TextField(
                                                            enabled: false,
                                                            style: subtitle2,
                                                            controller: controller.textTimeController,
                                                            decoration: InputDecoration(
                                                              filled: true,
                                                              fillColor: AppColors.white,
                                                              contentPadding: const EdgeInsets.all(8),
                                                              disabledBorder: const OutlineInputBorder(
                                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                                // width: 0.0 produces a thin "hairline" border
                                                                borderSide: BorderSide(color: Colors.black, width: 0.2),
                                                              ),
                                                              hintText: 'Select Date',
                                                              hintStyle: subtitle1,
                                                              suffixIcon: Padding(
                                                                padding: const EdgeInsets.all(12.0),
                                                                child: Icon(
                                                                  Icons.schedule,
                                                                  color: Colors.grey.shade300,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            )
                                          : Container(),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Bill Details',
                                    style: body1,
                                  ),
                                  const SizedBox(
                                    height: paddingSmall,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Item Total',
                                        style: captionLite,
                                      ),
                                      Text(
                                        '${Storage.box.get(Constants.currency)} ${controller.cartData?.billDetails?.itemTotal.toStringAsFixed(2)}',
                                        style: subtitle2.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: paddingExtraSmall,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Delivery Fee',
                                        style: captionLite.copyWith(
                                          color: Colors.lime.shade600,
                                          decoration: TextDecoration.underline,
                                          decorationStyle: TextDecorationStyle.dashed,
                                        ),
                                      ),
                                      Text(
                                        '${Storage.box.get(Constants.currency)} ${controller.cartData?.billDetails?.deliveryFee.toStringAsFixed(2)}',
                                        style: subtitle2.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: paddingSmall,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Tax',
                                        style: captionLite,
                                      ),
                                      Text(
                                        '${Storage.box.get(Constants.currency)} ${controller.cartData?.billDetails?.tax.toStringAsFixed(2)}',
                                        style: subtitle2.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: paddingSmall,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Discount',
                                        style: captionLite,
                                      ),
                                      Text(
                                        '${Storage.box.get(Constants.currency)} ${controller.totalSaving.toStringAsFixed(2)}',
                                        style: subtitle2.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: paddingSmall,
                                  ),
                                  const Divider(),
                                  const SizedBox(
                                    height: paddingSmall,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Grand Total',
                                        style: body1.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                      Obx(() {
                                        return Text(
                                          '${Storage.box.get(Constants.currency)} ${controller.grandTotal.value.toStringAsFixed(2)}',
                                          style: subtitle2.copyWith(fontWeight: FontWeight.bold),
                                        );
                                      }),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: paddingLarge,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: AppColors.card_offer_color.withOpacity(.1),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(8),
                                  ),
                                  border: Border.all(color: AppColors.card_offer_color),
                                ),
                                padding: const EdgeInsets.only(
                                    left: paddingLarge, right: paddingLarge, top: paddingLarge, bottom: paddingLarge),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Your total saving',
                                      style: subtitleLite.copyWith(color: AppColors.card_offer_color),
                                    ),
                                    Obx(() {
                                      return Text(
                                        '${Storage.instance.getValue(Constants.currency)} ${controller.totalSaving.value.toStringAsFixed(2)}',
                                        style: subtitleLite.copyWith(
                                            fontWeight: FontWeight.bold, color: AppColors.card_offer_color),
                                      );
                                    }),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: paddingExtraLarge,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: TextField(
                                controller: controller.couponController,
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(2),
                                  enabledBorder: const OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide: BorderSide(color: Colors.grey, width: 0.0),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(color: AppColors.primary_color, width: 1),
                                  ),
                                  hintText: 'Enter Coupon Here',
                                  hintStyle: subtitle1,
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: SvgPicture.asset(
                                      Assets.svgOfferOutline,
                                      height: 10,
                                    ),
                                  ),
                                  suffixIcon: TextButton(
                                    onPressed: () {
                                      if (controller.couponController.value.text.isEmpty) {
                                        SnackBarFailure(titleText: 'Oops..', messageText: 'Invalid coupon code');
                                      } else {
                                        if (controller.isCouponApplied.value) {
                                          controller.clearCoupon();
                                        } else {
                                          controller.applyCoupon();
                                        }
                                      }
                                    },
                                    child: Obx(() {
                                      return Text(
                                        controller.isCouponApplied.value ? 'Clear' : 'Apply',
                                        style: subtitleLite.copyWith(
                                            color: AppColors.primary_color, fontWeight: FontWeight.bold),
                                      );
                                    }),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: paddingExtraLarge,
                            ),
                            Container(
                              color: Colors.grey.shade200,
                              width: Get.width,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: paddingLarge, right: paddingLarge, top: paddingLarge, bottom: paddingLarge),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        SvgPicture.asset(Assets.svgTip),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          'Tip your order',
                                          style: captionLite.copyWith(color: Colors.grey),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: paddingSmall,
                                    ),
                                    Obx(() {
                                      return Wrap(
                                        spacing: 8,
                                        children: List.generate(controller.tips.length, (index) {
                                          return ChoiceChip(
                                            labelPadding: const EdgeInsets.all(2.0),
                                            label: Text(
                                              '${Storage.box.get(Constants.currency)} ${controller.selectedTips.value?.amount}',
                                              style: captionLite.copyWith(
                                                  color: controller.selectedTips.value?.amount ==
                                                          controller.tips[index].amount
                                                      ? AppColors.white
                                                      : AppColors.black1),
                                            ),
                                            selected: controller.selectedTips.value == controller.tips[index],
                                            selectedColor: AppColors.primary_color,
                                            pressElevation: 0,
                                            backgroundColor: AppColors.white,
                                            onSelected: (value) {
                                              controller.selectedTips.value =
                                                  value ? controller.tips[index] : controller.selectedTips.value;
                                            },
                                            // backgroundColor: color,
                                            elevation: 1,
                                            padding: const EdgeInsets.symmetric(horizontal: paddingLarge),
                                          );
                                        }),
                                      );
                                    }),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: paddingExtraLarge,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.shade200,
                              blurRadius: 10.0,
                              spreadRadius: 10,
                              offset: Offset(0.0, 0.75)),
                        ],
                        color: AppColors.white,
                      ),
                      padding: const EdgeInsets.only(
                          top: paddingMedium, bottom: paddingLarge, left: paddingExtraLarge, right: paddingExtraLarge),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Obx(() {
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${controller.cartProductItems.length} Items',
                                    style: captionLite.copyWith(
                                      color: Colors.grey.shade500,
                                    ),
                                  ),
                                  Obx(() {
                                    return Text(
                                      '${Storage.box.get(Constants.currency)} ${controller.numberFormat.format(controller.grandTotal.value)}',
                                      style: headline6.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                    );
                                  }),
                                ],
                              );
                            }),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.primary_color,
                              onPrimary: Colors.white,
                              shadowColor: AppColors.primary_color,
                              elevation: 4,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32.0)),
                              minimumSize: const Size(100, 40), //////// HERE
                            ),
                            onPressed: () {
                              controller.confirmOrder();
                              // Get.toNamed(Routes.CONFIRM_ORDER,arguments: {'shopId':controller.cartProductItems[0].shopId});
                            },
                            child: Text(
                              'Confirm',
                              style: subtitle1,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                );
    });
  }
}
