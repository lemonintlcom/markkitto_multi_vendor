import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/market_item_card.dart';
import 'package:markkito_customer/app/modules/home/controllers/markets_controller.dart';

import '../../../../constants/dimens.dart';
import '../../../../generated/assets.dart';
import '../../../../themes/custom_theme.dart';
import '../../custom_widgets/super_saving_deals_card.dart';

class MarketsView extends StatelessWidget {
  const MarketsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(MarketsController());
    return Padding(
      padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
      child: CustomScrollView(
        controller: controller.scrollController,
        physics: const BouncingScrollPhysics(),
        slivers: [
          const SliverPadding(
            padding: EdgeInsets.only(left: paddingLarge, right: paddingLarge),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: paddingLarge,
            ),
          ),
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  Assets.svgStoreSmall,
                  height: 20,
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  'Nearest Markets',
                  style: headline6.copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: paddingLarge,
            ),
          ),
          Obx(() {
            return SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Padding(
                  key: ObjectKey(controller.marketList[index]),
                  padding: const EdgeInsets.only(
                    bottom: paddingLarge,
                  ),
                  child: MarketItemCard(
                    isFullWidth: true,
                    market: controller.marketList[index],
                  ),
                );
              }, childCount: controller.marketList.length),
            );
          }),
          Obx(() {
            return SliverToBoxAdapter(
              child: controller.marketList.isNotEmpty ? const SuperSavingDealCard() : Container(),
            );
          }),
          const SliverToBoxAdapter(
            child: SizedBox(
              height: paddingLarge,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              return const Padding(
                padding: EdgeInsets.only(bottom: paddingLarge),
                child: MarketItemCard(
                  isFullWidth: true,
                ),
              );
            }, childCount: 0),
          ),
        ],
      ),
    );
  }
}
