import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/home/controllers/cart_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/markets_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/more_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/stores_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MoreController>(
      () => MoreController(),
    );
    Get.lazyPut<StoresController>(
      () => StoresController(),
    );
    Get.lazyPut<MarketsController>(
      () => MarketsController(),
    );
    Get.lazyPut<HomeContentController>(
      () => HomeContentController(),
    );
    Get.lazyPut<CartController>(
      () => CartController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
