import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:timeline_tile/timeline_tile.dart';
import '../controllers/order_details_controller.dart';

class OrderDetailsView extends GetView<OrderDetailsController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Get.arguments['fromCart'] ? Get.find<HomeController>().willPopCallbackCartRefresh() : Get.back();
        return true;
      },
      child: SafeArea(
        child: Obx(() {
          return Scaffold(
              appBar: CustomAppBar(
                titleText: 'Order Details ${controller.count}',
              ),
              body: controller.isLoading.value
                  ? Container()
                  : SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              children: [
                                Image.asset(
                                  Assets.imagesOrderSuccess,
                                  height: 100,
                                  width: 100,
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${controller.orderDetails?.deliveryStatus}',
                                      style: headline5.copyWith(fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      '${controller.orderDetails?.orderTime}',
                                      style: captionLite,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              children: [
                                Image.network(
                                  "${controller.orderDetails?.shopDetails?.shopThumb}",
                                  height: 70,
                                  width: 70,
                                ),
                                const SizedBox(
                                  width: 14,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${controller.orderDetails?.shopDetails?.shopName}",
                                      style: subtitle1.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "#${controller.orderDetails?.orderId}",
                                      style: subtitle2.copyWith(color: Colors.grey),
                                    ),
                                    Text(
                                      "${Storage.instance.getValue(Constants.currency)} ${"${controller.orderDetails?.totalPrice}"}",
                                      style: subtitle2.copyWith(color: Colors.black),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Image.asset(
                                      Assets.imagesMoney,
                                      height: 40,
                                      width: 40,
                                    ),
                                    const SizedBox(
                                      width: 14,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Payment Method",
                                          style: captionLite.copyWith(color: Colors.grey),
                                        ),
                                        Text(
                                          "Cash",
                                          style: subtitle1.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Image.asset(
                                      Assets.imagesScooter,
                                      height: 40,
                                      width: 40,
                                    ),
                                    const SizedBox(
                                      width: 14,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Delivery Method",
                                          style: captionLite.copyWith(
                                            color: Colors.grey,
                                          ),
                                        ),
                                        Text(
                                          "Fast",
                                          style: subtitle1.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: 30,
                                  child: SvgPicture.asset(Assets.svgLocationInnerGreen),
                                ),
                                const SizedBox(
                                  width: paddingLarge,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Delivery Address',
                                        style: subtitle1.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(top: 6.0, right: 6),
                                        child: Text(
                                          '${controller.orderDetails?.addressData?.streetDetails}',
                                          style: subtitleLite.copyWith(fontSize: 14),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade200),
                          controller.orderDetails!.orderTracking!.isNotEmpty
                              ? Column(
                                  children: [
                                    ListView.builder(
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) {
                                        var item = controller.orderDetails?.orderTracking?[index];
                                        return TimelineTile(
                                          alignment: TimelineAlign.manual,
                                          lineXY: 0.1,
                                          isFirst: index == 0 ? true : false,
                                          indicatorStyle: const IndicatorStyle(
                                            width: 20,
                                            color: AppColors.primary_color,
                                            padding: EdgeInsets.all(6),
                                          ),
                                          endChild: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Text(
                                                '${item?.orderStatusName}',
                                                style: captionLite.copyWith(
                                                    color: Colors.black, fontWeight: FontWeight.bold),
                                              ),
                                              Text(
                                                '${item?.dateTime}',
                                                style: caption,
                                              ),
                                            ],
                                          ),
                                          beforeLineStyle: const LineStyle(
                                            color: AppColors.primary_color,
                                          ),
                                        );
                                      },
                                      itemCount: controller.orderDetails!.orderTracking!.length,
                                    ),
                                    const SizedBox(
                                      height: paddingExtraLarge,
                                    ),
                                    Container(
                                        width: double.infinity, height: paddingSmall, color: Colors.grey.shade200),
                                    const SizedBox(
                                      height: paddingSmall,
                                    ),
                                  ],
                                )
                              : Container(),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Order Summary',
                                  style: subtitle1.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                ),
                                TextButton(onPressed: () {}, child: const Text('VIEW RECIEPT'))
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Column(
                              children: List.generate(controller.orderDetails!.items!.length, (index) {
                                var item = controller.orderDetails?.items?[index];
                                return ListTile(
                                  leading: Image.network(
                                    '${item?.productThumb}',
                                    height: 40,
                                    width: 40,
                                  ),
                                  title: Text(
                                    '${item?.itemName}',
                                    softWrap: false,
                                    overflow: TextOverflow.fade,
                                    maxLines: 1,
                                    style: caption.copyWith(color: Colors.black, fontWeight: FontWeight.bold),
                                  ),
                                );
                              }),
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Total',
                                  style: subtitle1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                    "${Storage.instance.getValue(Constants.currency)} ${"${controller.orderDetails?.totalPrice}"}",
                                    style: subtitle1.copyWith(fontWeight: FontWeight.bold))
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          /*Padding(
                            padding: const EdgeInsets.only(
                              left: paddingExtraLarge,
                              right: paddingExtraLarge,
                            ),
                            child: Row(
                              children: [
                                Expanded(child: PrimaryButton(text: 'Reorder', onTap: () {})),
                              ],
                            ),
                          ),*/
                        ],
                      ),
                    ));
        }),
      ),
    );
  }
}
