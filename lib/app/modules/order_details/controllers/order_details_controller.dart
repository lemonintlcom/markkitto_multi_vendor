import 'dart:convert';

import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/OrderDetailsReponse.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class OrderDetailsController extends GetxController {
  final count = '';
  var isLoading = true.obs;
  OrderDetails? orderDetails;
  @override
  void onInit() {
    super.onInit();
    getOrderDetails();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void getOrderDetails() async {
    var data = {
      'OrderId': Get.arguments['orderId'],
    };
    var result = await XHttp.request(Endpoints.r_order_detailsv2, method: XHttp.POST, data: data);

    var confirmOrderResponse = OrderDetailsReponse.fromJson(jsonDecode(result.data));
    if (confirmOrderResponse.statusCode == 200) {
      isLoading.value = false;
      orderDetails = confirmOrderResponse.orderDetails;
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Unable to create Order. Try again.").show();
      isLoading.value = false;
    }
  }
}
