import 'dart:convert';

import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/BannersResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class OffersController extends GetxController {
  var isLoading = true.obs;

  var banners = <BannerData>[];

  @override
  void onInit() {
    super.onInit();
    getOffers();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getOffers() async {
    var data = {
      'type': "product", //type : market, shop, product
      'latitude': Storage.instance.getValue(Constants.latitude),
      'longitude': Storage.instance.getValue(Constants.longitude),
    };
    var result = await XHttp.request(Endpoints.r_bannerDetails, method: XHttp.POST, data: data);
    // var result = await XHttp.request(Endpoints.r_single_banners, method: XHttp.POST, data: data);
    if (result.code == 200) {
      isLoading.value = false;
      var bannerResponse = BannersResponse.fromJson(jsonDecode(result.data));
      banners.addAll(bannerResponse.banners!);
    } else {
      isLoading.value = false;
      SnackBarFailure(titleText: "Oops..", messageText: "Unable to load offers..Try again later").show();
    }
  }
}
