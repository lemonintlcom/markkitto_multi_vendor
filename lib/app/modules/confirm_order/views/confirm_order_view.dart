import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/home/controllers/cart_controller.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../home/controllers/home_controller.dart';
import '../controllers/confirm_order_controller.dart';

class ConfirmOrderView extends GetView<ConfirmOrderController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Get.find<HomeController>().willPopCallbackCartRefresh(),
      child: SafeArea(
        child: Scaffold(
          appBar: CustomAppBar(
            titleText: 'Confirm Order ${controller.count.value}',
          ),
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Obx(() {
              return controller.isProcessingPayment.value
                  ? Text(
                      'Processing Payment ...',
                      style: subtitle1.copyWith(color: Colors.black),
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: paddingLarge,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: 30,
                                child: SvgPicture.asset(Assets.svgLocationInnerGreen),
                              ),
                              const SizedBox(
                                width: paddingLarge,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Delivery Address',
                                      style: subtitle1.copyWith(color: Colors.black),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0, right: 8),
                                      child: Obx(() {
                                        return Text(
                                          '${Get.find<CartController>().selectedDeliveryAddress.value?.placeText}',
                                          style: subtitleLite.copyWith(fontSize: 14),
                                        );
                                      }),
                                    ),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Get.find<CartController>().changeDeliveryAddress(context);
                                },
                                child: Text(
                                  'CHANGE',
                                  style: body2.copyWith(color: Colors.red),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: paddingLarge,
                        ),
                        Container(width: double.infinity, height: 4, color: Colors.grey.shade300),
                        const SizedBox(
                          height: paddingLarge,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: paddingLarge,
                            right: paddingLarge,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Choose your payment methods',
                                style: subtitle1.copyWith(color: Colors.black),
                              ),
                              const SizedBox(
                                height: paddingLarge,
                              ),
                              ListView.separated(
                                itemCount: controller.confirmOrderResponse!.data!.paymentMethods!.length,
                                shrinkWrap: true,
                                separatorBuilder: (BuildContext context, int index) => Divider(),
                                itemBuilder: (BuildContext context, int index) {
                                  var upiApp = controller.confirmOrderResponse!.data!.paymentMethods![index];
                                  return ListTile(
                                    onTap: () {
                                      log('${upiApp.id}, ${upiApp.paymentName}');
                                      if (upiApp.id == 2) {
                                        // cod
                                        controller.placeOrder(upiApp);
                                      } else if (upiApp.id == 3) {
                                        // card or razorpay
                                        controller.generateRazorPayOrder(upiApp);
                                      } else if (upiApp.id == 4) {
                                        controller.listUpiAppsDialog(context);
                                      }
                                      // log("ontap ${upiApp.upiApplication.appName}");
                                      // controller.doUpiTransation(upiApp);
                                    },
                                    // leading: upiApp.iconImage(28),
                                    leading: const Icon(Icons.payment),
                                    title: Text(
                                      '${upiApp.paymentName}',
                                      style: subtitle1,
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
            }),
          ),
        ),
      ),
    );
  }
}
