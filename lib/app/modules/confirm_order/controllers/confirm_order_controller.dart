import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/ConfirmOrderResponse.dart';
import 'package:markkito_customer/app/data/models/PlaceOrderResponse.dart';
import 'package:markkito_customer/app/data/models/RazorPayGeneratePaymentResponse.dart';
import 'package:markkito_customer/app/data/models/UpdatePaymentRefResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/cart_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:upi_pay/upi_pay.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

import '../../home/controllers/home_controller.dart';

class ConfirmOrderController extends GetxController {
  //TODO: Implement ConfirmOrderController

  final count = 0.obs;

  var upiAppList = <ApplicationMeta>[].obs;

  var isLoading = true.obs;
  var isProcessingPayment = false.obs;

  var confirmOrderResponse = Get.find<CartController>().confirmOrderResponse;

  PaymentDetails? generatePaymentDetails;

  int? currentPaymentId;

  late Razorpay _razorpay;

  @override
  void onInit() {
    super.onInit();
    // log(jsonDecode(Get.arguments['data']));
    getUpiAppList();
    log("${confirmOrderResponse?.statusCode}");

    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    _razorpay.clear();
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(msg: "SUCCESS: " + response.paymentId!, timeInSecForIosWeb: 2);
    updatePaymentReference(response.paymentId!);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    isProcessingPayment.value = false;
    // Fluttertoast.showToast(
    //     msg: "ERROR: " + response.code.toString() + " - " + response.message!, timeInSecForIosWeb: 2);
    SnackBarFailure(titleText: "Oops..", messageText: "Payment Failed. Try again - ${response.message!}").show();
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(msg: "EXTERNAL_WALLET: " + response.walletName!, timeInSecForIosWeb: 2);
  }

  void getUpiAppList() async {
    final List<ApplicationMeta> appMetaList = await UpiPay.getInstalledUpiApplications(
        statusType: UpiApplicationDiscoveryAppStatusType.all,
        paymentType: UpiApplicationDiscoveryAppPaymentType.nonMerchant);
    Logger().e(appMetaList);
    upiAppList.addAll(appMetaList);
    for (var element in appMetaList) {
      log(element.packageName);
    }
  }

  void doUpiTransation(ApplicationMeta upiApp) async {
    final UpiTransactionResponse response = await UpiPay.initiateTransaction(
      amount: '10',
      app: upiApp.upiApplication,
      receiverName: 'Nowfal Salahudeen',
      receiverUpiAddress: 'nowfalsalahudeen@okicici',
      transactionRef: 'UPITXREF0001',
      transactionNote: 'A UPI Transaction',
    );
    print(response.status);
  }

  void placeOrder(PaymentMethods payMethod) async {
    log(payMethod.id!.toString());
    currentPaymentId = payMethod.id!;
    var data = {
      'orderID': '${confirmOrderResponse?.data?.orderId}',
      'PaymentmethodID': payMethod.id,
    };
    var result = await XHttp.request(Endpoints.c_place_order, method: XHttp.POST, data: data);

    var placeOrderResponse = PlaceOrderResponse.fromJson(jsonDecode(result.data));
    if (placeOrderResponse.statusCode == 200) {
      SnackBarSuccess(titleText: "Success", messageText: "${placeOrderResponse.data?.status}").show();
      Get.offAndToNamed(Routes.ORDER_DETAILS,
          arguments: {'orderId': placeOrderResponse.data?.orderDetails?.orderID, 'fromCart': true});
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void generateRazorPayOrder(PaymentMethods payMethod) async {
    currentPaymentId = payMethod.id!;
    log(payMethod.id!.toString());
    var data = {
      'orderID': '${confirmOrderResponse?.data?.orderId}',
    };
    var result = await XHttp.request(Endpoints.c_generate_payment_details, method: XHttp.POST, data: data);

    var placeOrderResponse = RazorPayGeneratePaymentResponse.fromJson(jsonDecode(result.data));
    if (placeOrderResponse.statusCode == 200) {
      // SnackBarSuccess(titleText: "Success", messageText: "${placeOrderResponse.data?.status}").show();
      if (placeOrderResponse.data!.paymentDetails != null) {
        isProcessingPayment.value = true;
        generatePaymentDetails = placeOrderResponse.data!.paymentDetails!;
        openCheckout(placeOrderResponse.data!.paymentDetails!);
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "${placeOrderResponse.data?.status}").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void openCheckout(PaymentDetails paymentDetails) async {
    var options = {
      'key': paymentDetails.razorpayKey,
      'amount': paymentDetails.amount,
      'name': paymentDetails.customerName,
      'description': paymentDetails.paymentGateOrderId,
      'prefill': {'contact': paymentDetails.customerMob, 'email': Get.find<HomeController>().user?.email},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      e.printError();
    }
  }

  void updatePaymentReference(String paymentRef) async {
    var data = {
      'PaymentmethodID': currentPaymentId,
      'orderID': generatePaymentDetails?.paymentGateOrderId,
      'paymentAmount': generatePaymentDetails?.amount,
      'Paymentref': paymentRef,
    };
    var result = await XHttp.request(Endpoints.update_order_payment_ref, method: XHttp.POST, data: data);

    var placeOrderResponse = UpdatePaymentRefResponse.fromJson(jsonDecode(result.data));
    if (placeOrderResponse.statusCode == 200) {
      if (placeOrderResponse.data!.orderDetails != null) {
        SnackBarSuccess(titleText: "Success", messageText: "${placeOrderResponse.data?.status}").show();
        Get.offAndToNamed(Routes.ORDER_DETAILS,
            arguments: {'orderId': placeOrderResponse.data!.orderDetails?.orderID, 'fromCart': true});
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "${placeOrderResponse.data?.status}").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void listUpiAppsDialog(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingExtraLarge),
            child: Container(
              height: Get.height * .5,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'UPI Apps',
                      style: subtitle1,
                    ),
                  ),
                  const SizedBox(height: 24),
                  Column(
                    children: List.generate(upiAppList.length, (index) {
                      var item = upiAppList[index];
                      return InkWell(
                        customBorder: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onTap: () {
                          // selectedDeliveryAddress.value = item;
                          doUpiTransation(item);
                          Get.back();
                        },
                        child: ListTile(
                          leading: item.iconImage(24),
                          title: Text('${item.upiApplication.appName}'),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
