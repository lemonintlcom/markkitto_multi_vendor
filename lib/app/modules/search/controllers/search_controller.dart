import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/CartChangeItemResponse.dart';
import 'package:markkito_customer/app/data/models/CartStatusResponse.dart';
import 'package:markkito_customer/app/data/models/SearchResponse.dart';
import 'package:markkito_customer/app/data/models/HomePageResponse.dart' as HomePageResponse;
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/app/modules/store_details/controllers/store_details_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class SearchController extends GetxController {
  var searchController = TextEditingController();
  var isLoading = false.obs;

  var productTypeValue = <int, String>{}.obs;
  var selectedProductTypeObject = <int, PriceList>{}.obs;
  var productPriceValue = <int, String>{}.obs;

  var products = <Products?>[].obs;

  var isLoggedIn = Storage.box.get(Constants.isUserLoggedIn, defaultValue: false);

  var shopFavorite = 0.obs;

  var searchValue = ''.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  onSearchApi(String value) async {
    isLoading.value = true;

    var data = {'shop_id': Storage.instance.getValue(Constants.storeId), 'searchContent': value};
    var result = await XHttp.request(Endpoints.r_search_individual, method: XHttp.POST, data: data);
    var searchDataResponse = SearchResponse.fromJson(jsonDecode(result.data));
    if (searchDataResponse.statusCode == 200) {
      products.clear();
      if (searchDataResponse.data!.products!.isNotEmpty) {
        // products.addAll(shopDataResponse.shopData!.products!);
        for (var product in searchDataResponse.data!.products!) {
          // var i = 0;
          // set selected product varient id from the product list
          if (product.priceList!.isNotEmpty) {
            product.selectedPriceListId = product.priceList![0].generatelistdetailsId;
            // selectedProductTypeObject[i] = product.priceList![0];
            product.cartTempCount = product.priceList![0].cartQty!;
          } else {
            product.selectedPriceListId = product.id;
            product.cartTempCount = 0;
          }
          products.add(product);
          // i++;
        }
        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateProductPrice(Products product) {
    products[products.indexWhere((element) => element?.id == product.id)] = product;
  }

  doProductFavorite(Products product) {
    //product.selectedPriceListId! = generatedProductListId
    Get.find<HomeController>().doProductFavorite(product.selectedPriceListId!, (product.favouirte == 1)).then((value) {
      if (value) {
        product.favouirte = product.favouirte == 1 ? 0 : 1;
        updateProductPrice(product);
      } //complete productTileUpdate
    });
  }

  void addToCart({status, product_id, quantity, productData}) async {
    var data = {
      'productId': product_id,
      'status': status,
      'quantity': quantity,
      'varient_id': '',
      'choice_of_crust_id': '',
      'topping_id': '',
    };
    var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
    var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      (productData as Products).isIncart = 1;
//find active productList index from the current product object
      var productIndex = products.indexWhere((element) => element?.id == productData.id);
      //find current selected product index from the product list
      var productListIndex = productData.priceList!
          .indexWhere((element) => element.generatelistdetailsId == productData.selectedPriceListId);
      // update productList cartQty
      productData.priceList![productListIndex].cartQty = productData.priceList![productListIndex].cartQty! + 1;
      // update localTemp Varieable for temp cart count
      productData.cartTempCount = productData.priceList![productListIndex].cartQty!;
      products[productIndex] = productData;
      var cartData = HomePageResponse.CartDatas()
        ..total_amount = addToCartResponse.data?.cartDatas?.totalAmount
        ..total_qty = addToCartResponse.data?.cartDatas?.totalQty
        ..total_savings = addToCartResponse.data?.cartDatas?.totalSavings;

      Get.find<HomeContentController>().updateCartData(
        cartData: cartData,
        isAdded: false,
      );
      //update products data across all screens for update view
      Get.find<HomeController>().updateProductsAcrossAllScreens(productData);
      SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.message}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateCartCount(Products product, String status) async {
    //find active productList index from the current product object
    var productListIndex =
        product.priceList!.indexWhere((element) => element.generatelistdetailsId == product.selectedPriceListId);
    //find current selected product index from the product list
    var productIndex = products.indexWhere((element) => element?.id == product.id);
    if (status != 'remove') {
      // update cart qty
      // var productIndex = products.indexWhere((element) => element?.id == product.id);

      var data = {
        'productId': product.selectedPriceListId,
        'quantity': status == 'decrement'
            ? product.priceList![productListIndex].cartQty! - 1
            : product.priceList![productListIndex].cartQty! + 1,
      };
      var result = await XHttp.request(Endpoints.c_ChangeCartItem, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // update product to the current product list
        product.priceList![productListIndex].cartQty = status == 'decrement'
            ? product.priceList![productListIndex].cartQty! - 1
            : product.priceList![productListIndex].cartQty! + 1;
        product.cartTempCount = product.priceList![productListIndex].cartQty!;
        products[productIndex] = product;
        var cartData = HomePageResponse.CartDatas()
          ..total_amount = addToCartResponse.data?.cartDatas?.totalAmount
          ..total_qty = addToCartResponse.data?.cartDatas?.totalQty
          ..total_savings = addToCartResponse.data?.cartDatas?.totalSavings;

        Get.find<HomeContentController>().updateCartData(
          cartData: cartData,
          isAdded: false,
        );
        //update products data across all screens for update view
        Get.find<HomeController>().updateProductsAcrossAllScreens(product);
        // SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    } else {
      //remove item from cart if qty selected zero or less than 1

      var data = {
        'productId': product.selectedPriceListId,
        'status': 'remove',
        'quantity': 0,
        'varient_id': '',
        'choice_of_crust_id': '',
        'topping_id': '',
      };
      var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // reset temprory update product data
        product.isIncart = 0;
        product.priceList![productListIndex].cartQty = 0;
        product.cartTempCount = 0;
        products[productIndex] = product;
        var cartData = HomePageResponse.CartDatas()
          ..total_amount = addToCartResponse.data?.cartDatas?.totalAmount
          ..total_qty = addToCartResponse.data?.cartDatas?.totalQty
          ..total_savings = addToCartResponse.data?.cartDatas?.totalSavings;

        Get.find<HomeContentController>().updateCartData(
          cartData: cartData,
          isAdded: false,
        );
        //update products data across all screens for update view
        Get.find<HomeController>().updateProductsAcrossAllScreens(product);
        SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data?.message}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    }
  }
}
