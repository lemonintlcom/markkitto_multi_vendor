import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/bottom_sheet_modal.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/increment_decrement.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/functions.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/search_controller.dart';

class SearchView extends StatelessWidget {
  const SearchView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(SearchController());
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            const SizedBox(
              height: paddingLarge,
            ),
            Padding(
              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Text(
                        '${Storage.box.get(Constants.displayName)}',
                        style: headline5.copyWith(
                            color: Storage.instance.getValue(Constants.colorCode) == null
                                ? AppColors.primary_color
                                : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: paddingLarge,
            ),
            Hero(
              tag: 'SearchTextField',
              child: Padding(
                padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                child: TextField(
                  autofocus: false,
                  controller: controller.searchController,
                  textInputAction: TextInputAction.search,
                  onChanged: (text) {
                    if (text.isNotEmpty) {
                      controller.searchValue.value = text;
                    } else {
                      controller.searchValue.value = '';
                      controller.products.clear();
                    }
                  },
                  onSubmitted: (value) {
                    controller.onSearchApi(value);
                  },
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(2),
                    enabledBorder: const OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide: BorderSide(color: Colors.grey, width: 0.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Storage.instance.getValue(Constants.colorCode) == null
                              ? AppColors.primary_color
                              : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                          width: 1),
                    ),
                    hintText: 'Search here',
                    hintStyle: subtitle1,
                    prefixIcon: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: SvgPicture.asset(
                        Assets.svgSearch,
                        height: 10,
                      ),
                    ),
                    suffixIcon: InkWell(
                      onTap: () {
                        controller.onSearchApi(controller.searchValue.value);
                      },
                      child: Obx(() {
                        return controller.searchValue.value.isEmpty
                            ? Container(
                                width: 1,
                              )
                            : SizedBox(
                                width: 75,
                                child: Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Center(
                                      child: Text(
                                    'ENTER',
                                    style: body1.copyWith(
                                      color: Storage.instance.getValue(Constants.colorCode) == null
                                          ? AppColors.primary_color
                                          : hexToColor(
                                              Storage.instance.getValue(Constants.colorCode),
                                            ),
                                    ),
                                  )),
                                ),
                              );
                      }),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: paddingLarge,
            ),
            Expanded(
              child: Obx(() {
                return controller.products.isEmpty
                    ? Center(
                        child: Text(
                        'No products found',
                        style: headline3.copyWith(fontSize: 28),
                      ))
                    : GridView.builder(
                        padding: EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                        itemCount: controller.products.length,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: .7),
                        itemBuilder: ((ctx, index) {
                          var product = controller.products[index];
                          return InkWell(
                            key: ValueKey(product?.selectedPriceListId),
                            onTap: () {
                              Get.toNamed(
                                Routes.PRODUCT_DETAILS,
                                arguments: {
                                  'productId': product?.generateProductListId,
                                  'VendorUserTypeId': Storage.instance.getValue(Constants.storeId)
                                },
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey.shade200),
                              ),
                              child: Stack(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8, right: 8, top: 12),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Image.network(
                                          product?.image != null
                                              ? "${product?.image}"
                                              : "https://picsum.photos/200/300",
                                          height: 90,
                                          width: 100,
                                          fit: BoxFit.contain,
                                          errorBuilder: (x, y, z) {
                                            return Image.asset(
                                              Assets.imagesProductPlaceholder,
                                              height: 90,
                                              width: 100,
                                              fit: BoxFit.contain,
                                            );
                                          },
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        SizedBox(
                                          width: double.infinity,
                                          child: Text(
                                            '${product?.catagoryName}',
                                            style: caption.copyWith(color: Colors.grey),
                                          ),
                                        ),
                                        SizedBox(
                                          width: double.infinity,
                                          child: Text(
                                            '${product?.productName} '.useCorrectEllipsis(),
                                            maxLines: 1,
                                            softWrap: false,
                                            overflow: TextOverflow.fade,
                                            style: caption.copyWith(fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        Row(
                                          children: [
                                            RichText(
                                              text: TextSpan(
                                                children: [
                                                  TextSpan(
                                                    text:
                                                        '${product?.currencySymbol}${product?.price.toStringAsFixed(2)}',
                                                    style: body2.copyWith(
                                                        color: Colors.black, fontWeight: FontWeight.bold),
                                                  ),
                                                  TextSpan(
                                                    text: ' /${product?.unit}',
                                                    style: captionSmall.copyWith(
                                                        color: Colors.black, fontWeight: FontWeight.bold),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Container(
                                                padding: const EdgeInsets.only(left: 8, right: 8),
                                                decoration: BoxDecoration(
                                                  borderRadius: const BorderRadius.all(Radius.circular(4)),
                                                  border: Border.all(color: Colors.grey.shade400),
                                                ),
                                                child: PopupMenuButton(
                                                  itemBuilder: (BuildContext context) {
                                                    return product!.priceList!.map((PriceList item) {
                                                      return PopupMenuItem<PriceList>(
                                                        value: item,
                                                        child: Text(
                                                          '${item.price.toStringAsFixed(2)} ${item.priceFor}',
                                                          style: caption.copyWith(color: Colors.black),
                                                        ),
                                                      );
                                                    }).toList();
                                                  },
                                                  onSelected: (PriceList value) {
                                                    controller.productTypeValue[index] =
                                                        '${value.price.toStringAsFixed(2)} ${value.priceFor}';
                                                    product?.price = value.price;
                                                    product?.unit = value.priceFor;
                                                    product?.selectedPriceListId = value.generatelistdetailsId;
                                                    product?.cartTempCount = value.cartQty!;

                                                    // controller.selectedProductTypeObject[index] = value;

                                                    controller.updateProductPrice(product!);
                                                    // controller.productQtyValue.value = '${value.price} ${value.priceFor}';
                                                  },
                                                  child: Row(
                                                    children: [
                                                      Obx(() {
                                                        return Expanded(
                                                          child: Text(
                                                            controller.productTypeValue[index] == null &&
                                                                    product!.priceList!.isNotEmpty
                                                                ? '${product.priceList![0].price.toStringAsFixed(2)} ${product.priceList![0].priceFor}'
                                                                : controller.productTypeValue[index] ?? '',
                                                            style: caption.copyWith(color: Colors.grey.shade600),
                                                          ),
                                                        );
                                                      }),
                                                      product!.priceList!.isNotEmpty && product.priceList!.length != 1
                                                          ? const Icon(Icons.arrow_drop_down)
                                                          : Container(
                                                              height: 24,
                                                            ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        !Get.find<HomeController>().isLoggedIn
                                            ? Container()
                                            : Expanded(
                                                child: Row(
                                                  children: [
                                                    product.isIncart! == 0
                                                        ? Expanded(
                                                            child: MaterialButton(
                                                              color: Storage.instance.getValue(Constants.colorCode) ==
                                                                      null
                                                                  ? AppColors.primary_color
                                                                  : hexToColor(
                                                                      Storage.instance.getValue(Constants.colorCode)),
                                                              textColor: AppColors.white,
                                                              height: 28,
                                                              elevation: 0,
                                                              shape: const RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.all(
                                                                  Radius.circular(6),
                                                                ),
                                                              ),
                                                              onPressed: () {
                                                                Get.find<HomeController>().isLoggedIn
                                                                    ? controller.addToCart(
                                                                        status: 'add',
                                                                        product_id: product.selectedPriceListId,
                                                                        quantity: 1,
                                                                        productData: product)
                                                                    : BottomSheetModal().showLoginRequired(context);
                                                              },
                                                              child: Text(
                                                                'Add to cart',
                                                                style: caption.copyWith(fontWeight: FontWeight.bold),
                                                              ),
                                                            ),
                                                          )
                                                        : Expanded(
                                                            child: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                IncrementDecrement(
                                                                  cartProductItems: null,
                                                                  count: product.cartTempCount,
                                                                  onDecrement: () {
                                                                    if (product.cartTempCount > 1) {
                                                                      // product.cartTempCount = product.cartTempCount - 1;
                                                                      controller.updateCartCount(product, 'decrement');
                                                                    }
                                                                    if (product.cartTempCount == 1) {
                                                                      controller.updateCartCount(product, 'remove');
                                                                    }
                                                                  },
                                                                  onIncrement: () {
                                                                    // product.cartTempCount = product.cartTempCount + 1;
                                                                    controller.updateCartCount(product, 'increment');
                                                                  },
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                  ],
                                                ),
                                              ),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    top: 10,
                                    left: 0,
                                    right: 0,
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            color: Colors.red,
                                            child: Text(
                                              ' Flat ${product.offer}% ',
                                              style: captionSmall.copyWith(color: AppColors.white),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              Get.find<HomeController>().isLoggedIn
                                                  ? controller.doProductFavorite(product)
                                                  : BottomSheetModal().showLoginRequired(context);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(right: 8.0),
                                              child: Image.asset(
                                                Assets.imagesBookmarkOutlineGrey,
                                                height: 16,
                                                color: product.favouirte == 1 ? AppColors.primary_color : Colors.grey,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                      );
              }),
            )
          ],
        ),
      ),
    );
  }
}
