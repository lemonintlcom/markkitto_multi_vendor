import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/constants/constants.dart';

class IntroController extends GetxController {
  final PageController pageController = PageController(initialPage: 0);
  var box = Hive.box(Constants.configName);
  var introTextList = [
    'Order from a wide range of restaurants',
    'Order from a wide range of restaurants',
    'Order from a wide range of markets',
  ];

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
