import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/app_text.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/modules/custom_widgets/secondary_button.dart';
import 'package:markkito_customer/app/modules/splash/controllers/splash_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:onboarding_animation/onboarding_animation.dart';

import '../controllers/intro_controller.dart';

class IntroView extends GetView<IntroController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Column(
        children: [
          SizedBox(
            height: context.height * .75,
            child: OnBoardingAnimation(
              pages: [
                introPage(assetFile: Assets.svgWalkthroughPeopleGlass, title: controller.introTextList[0]),
                introPage(assetFile: Assets.imagesIntroThumb, title: controller.introTextList[1], type: 'image'),
                introPage(assetFile: Assets.svgWalkthroughPeopleGlass, title: controller.introTextList[2]),
              ],
              indicatorDotHeight: 5.0,
              indicatorDotWidth: 35.0,
              indicatorType: IndicatorType.colourTransitionDots,
              indicatorActiveDotColor: AppColors.primary_color,
              indicatorDotRadius: 1,
              indicatorPosition: IndicatorPosition.bottomCenter,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Selected Country : ${Get.find<SplashController>().selectedCountryData.value?.countryName}'),
                TextButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: Text('Change'))
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Expanded(
                    child: SecondaryButton(
                        text: 'Explore',
                        onTap: () {
                          // Get.toNamed(Routes.HOME);
                          Storage.instance.setValue(Constants.isUserLoggedIn, false);
                          if (controller.box.get(Constants.hasUserLocation, defaultValue: false)) {
                            Get.offAllNamed(Routes.HOME);
                          } else {
                            Get.toNamed(Routes.SELECT_LOCATION_MAP, arguments: {'fromHome': false, 'toLogin': false});
                          }
                          // Get.toNamed(Routes.SELECT_LOCATION_MAP);
                        }),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: PrimaryButton(
                        text: 'Login',
                        onTap: () {
                          Get.toNamed(Routes.SELECT_LOCATION_MAP, arguments: {'fromHome': false, 'toLogin': true});
                          // Get.offAndToNamed(Routes.LOGIN);
                        }),
                  ),
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }

  Column introPage({assetFile, title, type = 'svg'}) {
    return Column(
      children: [
        SizedBox(
          height: Get.height * .1,
        ),
        Padding(
          padding: EdgeInsets.only(left: Get.width * .15, right: Get.width * .15),
          child: SizedBox(
            height: Get.height * .5,
            child: type == 'image' ? Image.asset(Assets.imagesIntroThumb) : SvgPicture.asset(assetFile),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: paddingExtraLarge,
            right: paddingExtraLarge,
          ),
          child: AppText(
            text: title,
            fontWeight: FontWeight.bold,
          ),
          /*Text(
            title,
            style:
                Get.textTheme.bodyText1?.copyWith(color: Colors.grey[900], fontWeight: FontWeight.bold, fontSize: 20),
            textAlign: TextAlign.center,
          ),*/
        )
      ],
    );
  }
}
