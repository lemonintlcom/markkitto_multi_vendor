import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/CustomerRegistrationResponse.dart';
import 'package:markkito_customer/app/data/UserData.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_mobile_controller.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_otp_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class LoginRegisterController extends GetxController {
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var box = Hive.box(Constants.configName);

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void validateAndRegisterUser() async {
    if (nameController.value.text.isEmpty) {
      SnackBarFailure(titleText: 'Oops..', messageText: 'Invalid Name').show();
    } else if (emailController.value.text.isEmpty) {
      SnackBarFailure(titleText: 'Oops..', messageText: 'Invalid email').show();
    } else if (!GetUtils.isEmail(emailController.value.text)) {
      SnackBarFailure(titleText: 'Oops..', messageText: 'Invalid email').show();
    } else {
      var data = {
        'Name': nameController.value.text,
        'Contact1': Get.find<LoginMobileController>().phoneNumber,
        'Email': emailController.value.text,
        'is_register': Get.find<LoginMobileController>().customerRegisterStatus,
        'referralId': '',
        'ProfilePic': null,
        'AreaCode': '',
        'Contact2': '',
        'Gender': '',
        'DOB': '',
        'FCMToken': '',
        'BuildingDetails': box.get(Constants.buildingDetails, defaultValue: ''),
        'StreetDetails': box.get(Constants.streetDetails, defaultValue: ''),
        'Locality': box.get(Constants.locality, defaultValue: ''),
        'State': box.get(Constants.state, defaultValue: ''),
        'City': box.get(Constants.city, defaultValue: ''),
        'PinCode': box.get(Constants.pinCode, defaultValue: ''),
        'Landmark': box.get(Constants.landmark, defaultValue: ''),
        'Latitude': box.get(Constants.latitude, defaultValue: ''),
        'Longitude': box.get(Constants.longitude, defaultValue: ''),
      };
      var result = await XHttp.request(Endpoints.customer_registration, method: XHttp.POST, data: data);
      var responseData = CustomerRegistrationResponse.fromJson(jsonDecode(result.data));
      Logger().e(box.get(Constants.isUserLoggedIn, defaultValue: false));
      if (responseData.statusCode == 200) {
        // box.put(isUserLoggedIn, true);
        Storage.instance.setValue(Constants.isUserLoggedIn, true);
        var user = User.fromJson((jsonDecode(Storage.box.get(Constants.userdb))));
        user.name = nameController.value.text;
        user.email = emailController.value.text;
        user.userId = responseData.data?.userId;
        box.put(Constants.userdb, jsonEncode(user.toJson()));
        await Get.find<LoginOtpController>().updateUserLocation(user.userId);
        SnackBarSuccess(titleText: "Success", messageText: "User successfully registered").show();
        Get.offAllNamed(Routes.HOME);
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      }
    }
  }
}
