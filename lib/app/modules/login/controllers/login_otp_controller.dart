import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/UserData.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/FavoriteResponse.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_controller.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

import 'login_mobile_controller.dart';

class LoginOtpController extends GetxController {
  final pinController = TextEditingController();
  final focusNode = FocusNode();
  var box = Hive.box(Constants.configName);
  Timer? _timer;
  var time = 30.obs;

  var mobile_number = Get.find<LoginMobileController>().phoneNumber;

  @override
  void onInit() {
    super.onInit();
  }

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (time.value == 0) {
          timer.cancel();
        } else {
          time.value--;
        }
      },
    );
  }

  @override
  void onReady() {
    super.onReady();
    startTimer();
  }

  @override
  void onClose() {
    pinController.dispose();
    focusNode.dispose();
    _timer?.cancel();
  }

  void validateOtp() async {
    var data = {'Mobile_Number': mobile_number, 'otp': pinController.value.text};
    var result = await XHttp.request(Endpoints.login_verification, method: XHttp.POST, data: data);
    var userData = UserData.fromJson(jsonDecode(result.data));
    if (userData.statusCode == 200) {
      await saveUsrDataToDB(userData.data?.token);
      SnackBarSuccess(titleText: "Success", messageText: "${userData.data?.msg}").show();
      if (Get.find<LoginMobileController>().customerRegisterStatus == 1) {
        Storage.instance.setValue(Constants.isUserLoggedIn, true);
        await updateUserLocation(
          userData.data?.token?.userId,
        ); //api docs says its userTypeid

      } else {
        Storage.instance.setValue(Constants.isUserLoggedIn, true);
        Get.find<LoginController>().pageController.jumpToPage(2);
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "${userData.data?.msg}").show();
    }
  }

  Future<void> saveUsrDataToDB(User? user) async {
    box.put(Constants.userdb, jsonEncode(user?.toJson()));
    box.put(Constants.authToken, user?.auth);
    XHttp.setAuthToken(user?.auth);

    Logger().e((box.get(Constants.userdb)));
  }

  Future<void> updateUserLocation(int? userTypeId) async {
    var data = {
      'address2': box.get(Constants.locality, defaultValue: ''),
      'address1': box.get(Constants.locationText, defaultValue: ''),
      'DeliveryContact1': Get.find<LoginMobileController>().phoneNumber,
      'DeliveryContact2': '',
      'usertypeid': userTypeId,
      'BuildingDetails': box.get(Constants.buildingDetails, defaultValue: ''),
      'StreetDetails': box.get(Constants.streetDetails, defaultValue: ''),
      'Locality': box.get(Constants.locality, defaultValue: ''),
      'State': box.get(Constants.state, defaultValue: ''),
      'City': box.get(Constants.city, defaultValue: ''),
      'PinCode': box.get(Constants.pinCode, defaultValue: ''),
      'Landmark': box.get(Constants.landmark, defaultValue: ''),
      'Latitude': box.get(Constants.latitude, defaultValue: ''),
      'Longitude': box.get(Constants.longitude, defaultValue: ''),
    };
    var result = await XHttp.request(Endpoints.i_user_address, method: XHttp.POST, data: data);
    var responseData = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (responseData.statusCode == 200) {
      if (responseData.data?.status == 'success') {
        Get.offAllNamed(Routes.HOME);
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Unable to save user location. Try again").show();
      }
    }
  }
}
