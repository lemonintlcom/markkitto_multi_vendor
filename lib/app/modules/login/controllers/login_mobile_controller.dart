import 'dart:convert';

import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/app/data/LoginOtp.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class LoginMobileController extends GetxController {
  var box = Hive.box(Constants.configName);

  String? completePhoneNumber;

  String? phoneNumber;

  int? customerRegisterStatus;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void sendOtp() {
    if (box.get(Constants.totalMobileNumberDigits) == phoneNumber!.length && GetUtils.isPhoneNumber(phoneNumber!)) {
      var data = {"Mobile_Number": phoneNumber};
      XHttp.request(Endpoints.customer_login, method: XHttp.POST, data: data).then((response) {
        var data = LoginOtp.fromJson(jsonDecode(response.data));
        if (data.statusCode == 200) {
          customerRegisterStatus = data.data?.customerRegisterStatus;
          SnackBarSuccess(titleText: "Success", messageText: "OTP has been sent successfully.").show();
          Get.find<LoginController>().pageController.jumpToPage(1);
        } else {
          SnackBarFailure(titleText: "Oops..", messageText: "Unable to sent OTP.").show();
        }
      });

      return;
    } else {
      print('sdfsf');
      SnackBarFailure(titleText: "Oops..", messageText: "Invalid phone number.").show();
      return;
    }
  }
}
