import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

import '../../../../constants/colors.dart';
import '../../../../themes/custom_theme.dart';
import '../../custom_widgets/primary_button.dart';
import '../controllers/login_mobile_controller.dart';

class LoginMobileView extends GetView<LoginMobileController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Text(
            'Enter your phone number and we will send and "OTP" to continue',
            style: subtitleLite.copyWith(fontSize: 16, color: Colors.grey),
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(
          height: 60,
        ),
        Text(
          'Mobile No.',
          style: captionLite.copyWith(fontWeight: FontWeight.normal),
        ),
        const SizedBox(
          height: 16,
        ),
        IntlPhoneField(
          obscureText: false,
          disableLengthCheck: false,
          dropdownIconPosition: IconPosition.trailing,
          showDropdownIcon: true,
          flagsButtonPadding: EdgeInsets.all(10),
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              // width: 0.0 produces a thin "hairline" border
              borderSide: BorderSide(color: Colors.grey, width: 0.0),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              borderSide: BorderSide(color: AppColors.primary_color, width: 1),
            ),
            hintText: '01234566771213',
            hintStyle: subtitle1,
          ),
          initialCountryCode: 'IN',
          onChanged: (phone) {
            print(phone.number);
            controller.phoneNumber = phone.number;
            controller.completePhoneNumber = phone.completeNumber;
          },
        ),
        const SizedBox(
          height: 42,
        ),
        Row(
          children: [
            Expanded(
              child: PrimaryButton(
                text: 'Send OTP',
                onTap: () {
                  controller.sendOtp();
                },
              ),
            ),
          ],
        ),
        const Expanded(child: SizedBox()),
        Text(
          'By continue, you agree to our Terms of Service, Privacy Policy, Content Policy',
          textAlign: TextAlign.center,
          style: subtitle2.copyWith(color: Colors.grey),
        )
      ],
    );
  }
}
