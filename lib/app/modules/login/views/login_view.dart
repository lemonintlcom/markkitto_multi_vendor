import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../../../constants/colors.dart';
import '../../../../constants/dimens.dart';
import '../../custom_widgets/custom_app_bar.dart';
import '../controllers/login_controller.dart';
import 'login_mobile_view.dart';
import 'login_otp_view.dart';
import 'login_register_view.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
          backgroundColor: AppColors.white,
          appBar: const CustomAppBar(
            titleText: '',
            automaticallyImplyLeading: false,
          ),
          body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.dark
                .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
            child: Padding(
              padding: const EdgeInsets.all(paddingLarge),
              child: PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: controller.pageController,
                children: [
                  LoginMobileView(),
                  LoginOtpView(),
                  LoginRegisterView(),
                ],
              ),
            ),
          )),
    ));
  }
}
