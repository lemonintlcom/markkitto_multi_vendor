import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_controller.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_mobile_controller.dart';
import 'package:markkito_customer/app/modules/login/controllers/login_otp_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:pinput/pinput.dart';

import '../../../../themes/custom_theme.dart';

class LoginOtpView extends GetView<LoginOtpController> {
  @override
  Widget build(BuildContext context) {
    var keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            InkWell(
              onTap: () {
                Get.find<LoginController>().pageController.jumpToPage(0);
              },
              child: Container(
                child: GetPlatform.isAndroid ? const Icon(Icons.arrow_back) : const Icon(Icons.arrow_back_ios),
              ),
            ),
            Text(
              'Enter verification code',
              style: headline6,
            ),
          ],
        ),
        const SizedBox(
          height: paddingLarge,
        ),
        Padding(
          padding: const EdgeInsets.only(left: paddingExtraLarge),
          child: Text(
            'We have sent an OTP to ${Get.find<LoginMobileController>().phoneNumber}',
            style: captionLite.copyWith(fontSize: 14),
          ),
        ),
        const SizedBox(
          height: 56,
        ),
        Center(
          child: Pinput(
            controller: controller.pinController,
            focusNode: controller.focusNode,
            defaultPinTheme: defaultPinTheme,

            validator: (s) {
              return s == '1234' ? null : 'Pin is incorrect';
            },
            hapticFeedbackType: HapticFeedbackType.lightImpact,
            // onCompleted: print,
            onCompleted: (value) {
              controller.validateOtp();
            },
            cursor: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 9),
                  width: 22,
                  height: 1,
                  color: AppColors.primary_color,
                ),
              ],
            ),
            focusedPinTheme: defaultPinTheme.copyWith(
              decoration: defaultPinTheme.decoration!.copyWith(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: AppColors.primary_color),
              ),
            ),
            submittedPinTheme: defaultPinTheme.copyWith(
              decoration: defaultPinTheme.decoration!.copyWith(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(19),
                border: Border.all(color: AppColors.primary_color),
              ),
            ),
            errorPinTheme: defaultPinTheme.copyBorderWith(
              border: Border.all(color: Colors.redAccent),
            ),
          ),
        ),
        const Expanded(child: SizedBox()),
        SizedBox(
          width: Get.width,
          child: Obx(() {
            return Text(
              '${controller.time.value}',
              textAlign: TextAlign.center,
            );
          }),
        ),
        Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Didn\'t receive the code? ',
                style: subtitleLite.copyWith(color: AppColors.black1, fontSize: 14),
              ),
              Text(
                'Resend now',
                style: subtitleLite.copyWith(color: Colors.grey, fontSize: 14),
              ),
            ],
          ),
        ),
        SizedBox(
          height: keyboardHeight,
        )
      ],
    );
  }
}
