import 'dart:convert';

import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/CartChangeItemResponse.dart';
import 'package:markkito_customer/app/data/models/CartStatusResponse.dart';
import 'package:markkito_customer/app/data/models/FavoriteResponse.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class StoreDetailsController extends GetxController {
  List catTypes = [
    'All',
    'Offers',
    'Vegetables',
    'Fruits',
    'Bakery',
    'Ice Creams',
  ].obs;

  var defaultCatTypeIndex = 0.obs;
  var productQtyList = <String>[
    '1 Pack',
    '2 Pack',
    '3 Pack',
    '4 Pack',
  ];

  var productTypeValue = <int, String>{}.obs;
  var selectedProductTypeObject = <int, PriceList>{}.obs;
  var productPriceValue = <int, String>{}.obs;

  var isLoading = true.obs;

  // final Rx<ShopData?> shopData = Rx<ShopData?>(null);
  ShopData? shopData;
  var catogories = <Categories>[].obs;
  var products = <Products?>[].obs;
  var pageNo = 0;

  var isLoggedIn = Storage.box.get(Constants.isUserLoggedIn, defaultValue: false);

  var shopFavorite = 0.obs;

  @override
  void onInit() {
    super.onInit();
    getShopDetails();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getShopDetails() async {
    var data = {
      'UserTypeId': Get.arguments['shop_id'],
    };
    var result = await XHttp.request(Endpoints.r_ShopDetails_v2, method: XHttp.POST, data: data);
    var shopDataResponse = ShopDetailsResponse.fromJson(jsonDecode(result.data));
    if (shopDataResponse.statusCode == 200) {
      if (shopDataResponse.shopData != null) {
        shopData = shopDataResponse.shopData;
        shopFavorite.value = shopData!.favorite!;
        catogories.addAll(shopDataResponse.shopData!.categories!);
        catogories.insert(0, Categories(categoryName: 'All'));
        // products.addAll(shopDataResponse.shopData!.products!);
        for (var product in shopDataResponse.shopData!.products!) {
          // var i = 0;
          // set selected product varient id from the product list
          if (product.priceList!.isNotEmpty) {
            product.selectedPriceListId = product.priceList![0].generatelistdetailsId;
            // selectedProductTypeObject[i] = product.priceList![0];
            product.cartTempCount = product.priceList![0].cartQty!;
          } else {
            product.selectedPriceListId = product.id;
            product.cartTempCount = 0;
          }
          products.add(product);
          // i++;
        }
        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateProductsByCat(Categories cat) {
    products.clear();
    productTypeValue.clear();
    if (cat.categoryName == "All") {
      products.addAll(shopData!.products!);
    } else {
      products.value = shopData!.products!
          .where((element) => element.catagoryName?.toLowerCase() == cat.categoryName?.toLowerCase())
          .toList();
    }
  }

  void updateProductPrice(Products product) {
    products[products.indexWhere((element) => element?.id == product.id)] = product;
  }

  doShopFavorite() {
    Get.find<HomeController>().doShopFavorite(shopData!.shopId!).then((value) {
      shopFavorite.value = value ? 1 : 0;
    });
  }

  void addToCart({status, product_id, quantity, productData}) async {
    var data = {
      'productId': product_id,
      'status': status,
      'quantity': quantity,
      'varient_id': '',
      'choice_of_crust_id': '',
      'topping_id': '',
    };
    var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
    var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
    if (addToCartResponse.statusCode == 200) {
      (productData as Products).isIncart = 1;
//find active productList index from the current product object
      var productIndex = products.indexWhere((element) => element?.id == productData.id);
      //find current selected product index from the product list
      var productListIndex = productData.priceList!
          .indexWhere((element) => element.generatelistdetailsId == productData.selectedPriceListId);
      // update productList cartQty
      productData.priceList![productListIndex].cartQty = productData.priceList![productListIndex].cartQty! + 1;
      // update localTemp Varieable for temp cart count
      productData.cartTempCount = productData.priceList![productListIndex].cartQty!;
      products[productIndex] = productData;
      SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateCartCount(Products product, String status) async {
    //find active productList index from the current product object
    var productListIndex =
    product.priceList!.indexWhere((element) => element.generatelistdetailsId == product.selectedPriceListId);
    //find current selected product index from the product list
    var productIndex = products.indexWhere((element) => element?.id == product.id);
    if (status != 'remove') {  // update cart qty
      // var productIndex = products.indexWhere((element) => element?.id == product.id);

      var data = {
        'productId': product.selectedPriceListId,
        'quantity': status == 'decrement'
            ? product.priceList![productListIndex].cartQty! - 1
            : product.priceList![productListIndex].cartQty! + 1,
      };
      var result = await XHttp.request(Endpoints.c_ChangeCartItem, method: XHttp.POST, data: data);
      var addToCartResponse = CartChangeItemResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // update product to the current product list
        product.priceList![productListIndex].cartQty = status == 'decrement'
            ? product.priceList![productListIndex].cartQty! - 1
            : product.priceList![productListIndex].cartQty! + 1;
        product.cartTempCount = product.priceList![productListIndex].cartQty!;
        products[productIndex] = product;
        // SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }
    } else { //remove item from cart if qty selected zero or less than 1

      var data = {
        'productId': product.selectedPriceListId,
        'status': 'remove',
        'quantity': 0,
        'varient_id': '',
        'choice_of_crust_id': '',
        'topping_id': '',
      };
      var result = await XHttp.request(Endpoints.c_ProductToCart, method: XHttp.POST, data: data);
      var addToCartResponse = CartStatusResponse.fromJson(jsonDecode(result.data));
      if (addToCartResponse.statusCode == 200) {
        // reset temprory update product data
        product.isIncart = 0;
        product.priceList![productListIndex].cartQty = 0;
        product.cartTempCount = 0;
        products[productIndex] = product;
        SnackBarSuccess(titleText: "Success", messageText: "${addToCartResponse.data}").show();
      } else {
        SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
        isLoading.value = false;
      }


    }
  }
}
