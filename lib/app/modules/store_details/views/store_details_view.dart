import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:markkito_customer/app/data/models/ShopDetailsResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/bottom_sheet_modal.dart';
import 'package:markkito_customer/app/modules/custom_widgets/increment_decrement.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_content_controller.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/utils/functions.dart';

import '../../../../constants/colors.dart';
import '../../../../constants/dimens.dart';
import '../../../../generated/assets.dart';
import '../../../../themes/custom_theme.dart';
import '../../../routes/app_pages.dart';
import '../controllers/store_details_controller.dart';

class StoreDetailsView extends GetView<StoreDetailsController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        floatingActionButton: Visibility(
          visible: Get.find<HomeContentController>().cartDatas.value != null,
          child: FloatingActionButton(
            backgroundColor: AppColors.primary_color,
            child: const Icon(Icons.shopping_cart),
            onPressed: () {
              Get.find<HomeController>().navigateToCart();
            },
          ),
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Obx(() {
              return Container(
                child: controller.isLoading.value
                    ? Container()
                    : CustomScrollView(
                        physics: const BouncingScrollPhysics(),
                        slivers: [
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingLarge,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    children: [
                                      InkWell(
                                        child: Icon(GetPlatform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios),
                                        onTap: () {
                                          Get.back();
                                        },
                                      ),
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        'Markkito',
                                        style: headline5.copyWith(
                                            color: AppColors.primary_color, fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                    onTap: () {},
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        SvgPicture.asset(
                                          Assets.svgLocation,
                                          height: 24,
                                        ),
                                        const SizedBox(
                                          width: 6,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Get.toNamed(Routes.SELECT_LOCATION_MAP,
                                                arguments: {'fromHome': true, 'toLogin': false});
                                          },
                                          child: SizedBox(
                                            width: Get.width * .3,
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  ' Delivery to',
                                                  style: caption,
                                                ),
                                                ValueListenableBuilder(
                                                    valueListenable: Hive.box(Constants.configName).listenable(),
                                                    builder: (context, Box box, _) {
                                                      return Text(
                                                        '${box.get(Constants.locationText)}',
                                                        style: body2,
                                                        maxLines: 1,
                                                        softWrap: false,
                                                        overflow: TextOverflow.fade,
                                                      );
                                                    }),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ), //Top bar
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingLarge,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                              child: TextField(
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(2),
                                  enabledBorder: const OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide: BorderSide(color: Colors.grey, width: 0.0),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(color: AppColors.primary_color, width: 1),
                                  ),
                                  hintText: 'Search here',
                                  hintStyle: subtitle1,
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: SvgPicture.asset(
                                      Assets.svgSearch,
                                      height: 10,
                                    ),
                                  ),
                                  suffixIcon: InkWell(
                                    onTap: () {},
                                    child: Padding(
                                      padding: const EdgeInsets.all(12.0),
                                      child: SvgPicture.asset(
                                        Assets.svgBarcode,
                                        height: 10,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingExtraLarge,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Stack(
                              children: [
                                Container(
                                  height: Get.height * .28,
                                  decoration: BoxDecoration(
                                    color: AppColors.black1,
                                    image: DecorationImage(
                                      fit: BoxFit.cover,
                                      colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
                                      image: NetworkImage(
                                        controller.shopData?.profilePic != null
                                            ? "${controller.shopData?.profilePic}"
                                            : "https://picsum.photos/200/300",
                                      ),
                                    ),
                                  ),
                                ),
                                // Image.asset(
                                //   Assets.imagesHotelParagonRestaurant,
                                //   fit: BoxFit.fill,
                                // ),
                                Positioned(
                                  bottom: 10,
                                  left: 10,
                                  right: 10,
                                  child: Container(
                                    height: Get.width * .2,
                                    width: double.infinity,
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(10),
                                          child: Image.network(
                                            controller.shopData?.profilePic != null
                                                ? "${controller.shopData?.profilePic}"
                                                : "https://picsum.photos/200/300",
                                            width: 70,
                                            fit: BoxFit.cover,
                                            height: 70,
                                          ),
                                        ),
                                        const SizedBox(
                                          width: paddingMedium,
                                        ),
                                        Flexible(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                '${controller.shopData?.shopName}',
                                                style: subtitleLite.copyWith(
                                                    fontWeight: FontWeight.bold, color: AppColors.white, fontSize: 20),
                                              ),
                                              Text(
                                                '${controller.shopData?.type}',
                                                style: captionLite.copyWith(
                                                  color: AppColors.white,
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      '${controller.shopData?.shopLocation?.place} '
                                                          .useCorrectEllipsis(),
                                                      maxLines: 1,
                                                      softWrap: false,
                                                      overflow: TextOverflow.fade,
                                                      style: caption.copyWith(
                                                          fontWeight: FontWeight.bold, color: AppColors.white),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 20,
                                                  ),
                                                  Text(
                                                    controller.shopData?.openingStatus! == 1
                                                        ? 'Open Now'
                                                        : 'Closed Now',
                                                    style: captionLite.copyWith(
                                                        color: controller.shopData?.openingStatus! == 1
                                                            ? Colors.green
                                                            : Colors.red,
                                                        fontWeight: FontWeight.bold),
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  right: paddingExtraLarge,
                                  top: paddingExtraLarge,
                                  child: InkWell(
                                    onTap: () {
                                      Get.find<HomeController>().isLoggedIn
                                          ? controller.doShopFavorite()
                                          : BottomSheetModal().showLoginRequired(context);
                                    },
                                    child: Container(
                                      height: 50,
                                      width: 50,
                                      decoration: const BoxDecoration(color: AppColors.white, shape: BoxShape.circle),
                                      child: Obx(() {
                                        return Image.asset(
                                          Assets.imagesBookmarkOutlineGrey,
                                          color: controller.shopFavorite.value == 1
                                              ? AppColors.primary_color
                                              : Colors.grey,
                                        );
                                      }),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingSmall,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  buildActionMenuItem(
                                      icon: Assets.svgStoreReview,
                                      title: 'Review',
                                      onTap: () {
                                        Get.toNamed(Routes.REVIEW, arguments: {'type': 'store'});
                                      }),
                                  buildActionMenuItem(icon: Assets.svgStoreOffers, title: 'Offers'),
                                  buildActionMenuItem(icon: Assets.svgStoreContact, title: 'Contact'),
                                  buildActionMenuItem(icon: Assets.svgStoreDirection, title: 'Direction'),
                                  buildActionMenuItem(icon: Assets.svgStoreShare, title: 'Share'),
                                ],
                              ),
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingSmall,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Container(
                                width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingLarge,
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: SingleChildScrollView(
                              physics: const BouncingScrollPhysics(),
                              padding: const EdgeInsets.only(left: paddingLarge),
                              scrollDirection: Axis.horizontal,
                              child: Obx(() {
                                return Row(
                                  children: List.generate(controller.catogories.length, (index) {
                                    var cat = controller.catogories[index];
                                    return Padding(
                                      padding: const EdgeInsets.only(right: paddingSmall),
                                      child: ChoiceChip(
                                        labelPadding: const EdgeInsets.all(2.0),
                                        label: Text(
                                          "${cat.categoryName}",
                                          style: captionLite.copyWith(
                                              color: controller.defaultCatTypeIndex.value == index
                                                  ? AppColors.white
                                                  : AppColors.black1),
                                        ),
                                        selected: controller.defaultCatTypeIndex.value == index,
                                        selectedColor: Colors.black,
                                        pressElevation: 0,
                                        backgroundColor: AppColors.white,
                                        onSelected: (value) {
                                          controller.defaultCatTypeIndex.value =
                                              value ? index : controller.defaultCatTypeIndex.value;
                                          controller.updateProductsByCat(cat);
                                        },
                                        // backgroundColor: color,
                                        elevation: 1,
                                        padding: const EdgeInsets.symmetric(horizontal: paddingLarge),
                                      ),
                                    );
                                  }),
                                );
                              }),
                            ),
                          ),
                          const SliverToBoxAdapter(
                            child: SizedBox(
                              height: paddingLarge,
                            ),
                          ),
                          SliverPadding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            sliver: Obx(() {
                              return SliverGrid(
                                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2, childAspectRatio: .7),
                                delegate: SliverChildBuilderDelegate((ctx, index) {
                                  var product = controller.products[index];
                                  return InkWell(
                                    onTap: () {
                                      Get.toNamed(Routes.PRODUCT_DETAILS, arguments: {
                                        'productId': product?.generateProductListId,
                                        'VendorUserTypeId': controller.shopData?.shopId
                                      });
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey.shade200),
                                      ),
                                      child: Stack(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(left: 8, right: 8, top: 12),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Image.network(
                                                  product?.image != null
                                                      ? "${product?.image}"
                                                      : "https://picsum.photos/200/300",
                                                  height: 90,
                                                  width: 100,
                                                  fit: BoxFit.contain,
                                                ),
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                SizedBox(
                                                  width: double.infinity,
                                                  child: Text(
                                                    '${product?.catagoryName}',
                                                    style: caption.copyWith(color: Colors.grey),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: double.infinity,
                                                  child: Text(
                                                    '${product?.productName} '.useCorrectEllipsis(),
                                                    maxLines: 1,
                                                    softWrap: false,
                                                    overflow: TextOverflow.fade,
                                                    style: caption.copyWith(fontWeight: FontWeight.bold),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 8,
                                                ),
                                                Row(
                                                  children: [
                                                    RichText(
                                                      text: TextSpan(
                                                        children: [
                                                          TextSpan(
                                                            text:
                                                                '${product?.currencySymbol}${product?.price.toStringAsFixed(2)}',
                                                            style: body2.copyWith(
                                                                color: Colors.black, fontWeight: FontWeight.bold),
                                                          ),
                                                          TextSpan(
                                                            text: ' /${product?.unit}',
                                                            style: captionSmall.copyWith(
                                                                color: Colors.black, fontWeight: FontWeight.bold),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                const SizedBox(
                                                  height: 8,
                                                ),
                                                Row(
                                                  children: [
                                                    Expanded(
                                                      child: Container(
                                                        padding: const EdgeInsets.only(left: 8, right: 8),
                                                        decoration: BoxDecoration(
                                                          borderRadius: const BorderRadius.all(Radius.circular(4)),
                                                          border: Border.all(color: Colors.grey.shade400),
                                                        ),
                                                        child: PopupMenuButton(
                                                          itemBuilder: (BuildContext context) {
                                                            return product!.priceList!.map((PriceList item) {
                                                              return PopupMenuItem<PriceList>(
                                                                value: item,
                                                                child: Text(
                                                                  '${item.price.toStringAsFixed(2)} ${item.priceFor}',
                                                                  style: caption.copyWith(color: Colors.black),
                                                                ),
                                                              );
                                                            }).toList();
                                                          },
                                                          onSelected: (PriceList value) {
                                                            controller.productTypeValue[index] =
                                                                '${value.price.toStringAsFixed(2)} ${value.priceFor}';
                                                            product?.price = value.price;
                                                            product?.unit = value.priceFor;
                                                            product?.selectedPriceListId = value.generatelistdetailsId;
                                                            product?.cartTempCount = value.cartQty!;

                                                            // controller.selectedProductTypeObject[index] = value;

                                                            controller.updateProductPrice(product!);
                                                            // controller.productQtyValue.value = '${value.price} ${value.priceFor}';
                                                          },
                                                          child: Row(
                                                            children: [
                                                              Obx(() {
                                                                return Expanded(
                                                                  child: Text(
                                                                    controller.productTypeValue[index] == null &&
                                                                            product!.priceList!.isNotEmpty
                                                                        ? '${product.priceList![0].price.toStringAsFixed(2)} ${product.priceList![0].priceFor}'
                                                                        : controller.productTypeValue[index] ?? '',
                                                                    style:
                                                                        caption.copyWith(color: Colors.grey.shade600),
                                                                  ),
                                                                );
                                                              }),
                                                              product!.priceList!.isNotEmpty
                                                                  ? const Icon(Icons.arrow_drop_down)
                                                                  : Container(),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Expanded(
                                                  child: Row(
                                                    children: [
                                                      product.isIncart! == 0
                                                          ? Expanded(
                                                              child: MaterialButton(
                                                                color: AppColors.primary_color,
                                                                textColor: AppColors.white,
                                                                height: 28,
                                                                elevation: 0,
                                                                shape: const RoundedRectangleBorder(
                                                                  borderRadius: BorderRadius.all(
                                                                    Radius.circular(6),
                                                                  ),
                                                                ),
                                                                onPressed: () {
                                                                  controller.addToCart(
                                                                      status: 'add',
                                                                      product_id: product.selectedPriceListId,
                                                                      quantity: 1,
                                                                      productData: product);
                                                                },
                                                                child: Text(
                                                                  'Add to cart',
                                                                  style: caption.copyWith(fontWeight: FontWeight.bold),
                                                                ),
                                                              ),
                                                            )
                                                          : Expanded(
                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: [
                                                                  IncrementDecrement(
                                                                    cartProductItems: null,
                                                                    count: product.cartTempCount,
                                                                    onDecrement: () {
                                                                      if (product.cartTempCount > 1) {
                                                                        // product.cartTempCount = product.cartTempCount - 1;
                                                                        controller.updateCartCount(
                                                                            product, 'decrement');
                                                                      }
                                                                      if (product.cartTempCount == 1) {
                                                                        controller.updateCartCount(product, 'remove');
                                                                      }
                                                                    },
                                                                    onIncrement: () {
                                                                      // product.cartTempCount = product.cartTempCount + 1;
                                                                      controller.updateCartCount(product, 'increment');
                                                                    },
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                      // MaterialButton(
                                                      //   minWidth: 8,
                                                      //   elevation: 0,
                                                      //   shape: const RoundedRectangleBorder(
                                                      //     borderRadius: BorderRadius.all(
                                                      //       Radius.circular(6),
                                                      //     ),
                                                      //   ),
                                                      //   onPressed: () {
                                                      //     if (product.cartTempCount > 0) {
                                                      //       controller.addToCart(
                                                      //           status: 'add',
                                                      //           product_id: product.selectedPriceListId,
                                                      //           quantity: product.cartTempCount);
                                                      //     } else {
                                                      //       SnackBarFailure(
                                                      //           titleText: "Oops..",
                                                      //           messageText: "Please change quantity");
                                                      //     }
                                                      //   },
                                                      //   child: const Icon(
                                                      //     Icons.add_shopping_cart,
                                                      //     size: 18,
                                                      //   ),
                                                      // ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            top: 10,
                                            left: 0,
                                            right: 0,
                                            child: SizedBox(
                                              width: double.infinity,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Container(
                                                    color: Colors.red,
                                                    child: Text(
                                                      'Flat 40%',
                                                      style: captionSmall.copyWith(color: AppColors.white),
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      Get.find<HomeController>().isLoggedIn
                                                          ? Get.snackbar("wishlist", "to be implemented")
                                                          : BottomSheetModal().showLoginRequired(context);
                                                    },
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(right: 8.0),
                                                      child: Image.asset(
                                                        Assets.imagesBookmarkOutlineGrey,
                                                        height: 16,
                                                        color: product.favouirte == 1
                                                            ? AppColors.primary_color
                                                            : Colors.grey,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }, childCount: controller.products.length),
                              );
                            }),
                          )
                        ],
                      ),
              );
            }),
          ),
        ),
      ),
    );
  }

  Widget buildActionMenuItem({icon, title, onTap}) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SvgPicture.asset(icon),
            SizedBox(
              height: paddingExtraSmall,
            ),
            Text(
              title,
              style: caption,
            )
          ],
        ),
      ),
    );
  }
}
