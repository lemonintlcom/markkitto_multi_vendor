import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/ProductDetailResponse.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

class ProductDetailsController extends GetxController {
  List<String> images = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTIZccfNPnqalhrWev-Xo7uBhkor57_rKbkw&usqp=CAU",
    "https://wallpaperaccess.com/full/2637581.jpg",
    "https://picsum.photos/200/300"
  ];
  late PageController pageController;

  List productSizes = [
    'S',
    'M',
    'L',
    'XL',
  ];

  List productColors = [
    Colors.black,
    Colors.yellow,
    Colors.blueAccent,
    Colors.red,
  ];

  var productSizesSelected = 0.obs;
  var productColorsSelected = 0.obs;

  var isLoading = true.obs;
  ProductDetail? productDetail;
  var choiceOfCrust = <ChoiceOfCrust>[].obs;
  var toping = <Topping>[].obs;

  @override
  void onInit() {
    super.onInit();
    pageController = PageController(viewportFraction: 1);
    getOrdersList();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getOrdersList() async {
    var data = {
      'VendorUserTypeId': Get.arguments['VendorUserTypeId'],
      'productId': Get.arguments['productId'],
    };
    // var data = {
    //   'VendorUserTypeId': 25,
    //   'productId': 897,
    // };
    var result = await XHttp.request(Endpoints.r_ProductDetailsv2, method: XHttp.POST, data: data);
    var productDetailsResponse = ProductDetailResponse.fromJson(jsonDecode(result.data));
    if (productDetailsResponse.statusCode == 200) {
      if (productDetailsResponse.productDetail != null) {
        productDetail = productDetailsResponse.productDetail;
        choiceOfCrust.addAll(productDetailsResponse.productDetail!.choiceOfCrust!);
        toping.addAll(productDetailsResponse.productDetail!.topping!);
        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateChoiceOfCrust(ChoiceOfCrust choice) {
    choiceOfCrust[choiceOfCrust.indexWhere((element) => element.id == choice.id)] = choice;
  }

  void updateTopping(Topping topp) {
    toping[toping.indexWhere((element) => element.id == topp.id)] = topp;
  }
}
