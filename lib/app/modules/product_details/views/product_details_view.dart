import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/LabeledCheckbox.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../../../../constants/colors.dart';
import '../../../../generated/assets.dart';
import '../controllers/product_details_controller.dart';

class ProductDetailsView extends GetView<ProductDetailsController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: const CustomAppBar(
          titleText: '',
        ),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.dark
              .copyWith(statusBarColor: AppColors.transparent, systemNavigationBarColor: AppColors.white),
          child: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(FocusNode());
            },
            child: Obx(() {
              return controller.isLoading.value
                  ? Container()
                  : SingleChildScrollView(
                      physics: const BouncingScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Stack(
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    height: context.height * .3,
                                    child: PageView.builder(
                                        itemCount: controller.productDetail?.thumbList?.length,
                                        pageSnapping: true,
                                        controller: controller.pageController,
                                        onPageChanged: (page) {},
                                        itemBuilder: (context, pagePosition) {
                                          return Container(
                                              width: Get.width,
                                              margin: const EdgeInsets.all(paddingLarge),
                                              child: Image.network(controller.productDetail!.thumbList![pagePosition]));
                                        }),
                                  ),
                                  SmoothPageIndicator(
                                      controller: controller.pageController, // PageController
                                      count: controller.productDetail!.thumbList!.length,
                                      effect: const WormEffect(
                                          activeDotColor: AppColors.primary_color,
                                          paintStyle: PaintingStyle.stroke,
                                          dotHeight: 8,
                                          dotWidth: 8,
                                          radius: 8), // your preferred effect
                                      onDotClicked: (index) {})
                                ],
                              ),
                              Positioned(
                                right: paddingLarge,
                                top: 10,
                                child: InkWell(
                                  onTap: () {},
                                  child: Image.asset(
                                    Assets.imagesBookmarkOutlineGrey,
                                    color:
                                        controller.productDetail?.favorite == 1 ? AppColors.primary_color : Colors.grey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${controller.productDetail?.productType}',
                                  style: body2.copyWith(color: Colors.grey),
                                ),
                                Text(
                                  '${controller.productDetail?.productName}',
                                  maxLines: 1,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: body1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: paddingSmall,
                                ),
                                Text(
                                  '${Storage.instance.getValue(Constants.currency)} ${controller.productDetail?.productPrice}',
                                  maxLines: 1,
                                  overflow: TextOverflow.fade,
                                  softWrap: false,
                                  style: headline5.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: paddingLarge,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: PrimaryButton(
                                        height: 40,
                                        text: 'Add to Cart',
                                        onTap: () {},
                                      ),
                                    ),
                                    controller.productDetail!.offerText!.isNotEmpty
                                        ? Row(
                                            children: [
                                              const SizedBox(
                                                width: paddingExtraLarge,
                                              ),
                                              const Icon(
                                                Icons.info_outline,
                                                color: AppColors.card_offer_color,
                                              ),
                                              const SizedBox(
                                                width: 4,
                                              ),
                                              Text(
                                                '${controller.productDetail?.offerText}',
                                                style: subtitle2.copyWith(color: AppColors.card_offer_color),
                                              ),
                                            ],
                                          )
                                        : Container(),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Select variant',
                                  style: body1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Obx(() {
                                  return Wrap(
                                    spacing: 8,
                                    children: List.generate(controller.productDetail!.varient!.length, (index) {
                                      return ChoiceChip(
                                        labelPadding: const EdgeInsets.all(2.0),
                                        label: Text(
                                          controller.productDetail!.varient![index].varientName!,
                                          style: captionLite.copyWith(
                                              color: controller.productSizesSelected.value == index
                                                  ? AppColors.white
                                                  : AppColors.black1),
                                        ),
                                        selected: controller.productSizesSelected.value == index,
                                        selectedColor: AppColors.primary_color,
                                        pressElevation: 0,
                                        backgroundColor: AppColors.white,
                                        onSelected: (value) {
                                          controller.productSizesSelected.value =
                                              value ? index : controller.productSizesSelected.value;
                                        },
                                        // backgroundColor: color,
                                        elevation: 1,
                                        padding: const EdgeInsets.symmetric(horizontal: paddingLarge),
                                      );
                                    }),
                                  );
                                }),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Select variant',
                                  style: body1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Obx(() {
                                  return Wrap(
                                    spacing: 8,
                                    children: List.generate(controller.productDetail!.varient!.length, (index) {
                                      return InkWell(
                                        onTap: () {
                                          controller.productColorsSelected.value = index;
                                        },
                                        child: Container(
                                          height: 38,
                                          width: 38,
                                          decoration: BoxDecoration(
                                              color:
                                                  hexToColor(controller.productDetail!.varient![index].varientColor!),
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: controller.productColorsSelected.value == index
                                                      ? AppColors.primary_color
                                                      : Colors.white,
                                                  width: 4),
                                              boxShadow: [
                                                BoxShadow(blurRadius: 2, spreadRadius: 2, color: Colors.grey.shade200),
                                              ]),
                                        ),
                                      );
                                    }),
                                  );
                                }),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Overview',
                                  style: body1.copyWith(fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  '${controller.productDetail?.productDescription}',
                                  style: caption.copyWith(fontSize: 15, color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          controller.choiceOfCrust.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Choice of Crust',
                                        style: body1.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                      Obx(() {
                                        return ListView.separated(
                                          itemCount: controller.choiceOfCrust.length,
                                          physics: const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (context, index) {
                                            var choice = controller.choiceOfCrust[index];
                                            return LabeledCheckbox(
                                              trailinglabel:
                                                  '${Storage.instance.getValue(Constants.currency)} ${choice.price}',
                                              label: '${choice.name}',
                                              selectedColor: AppColors.primary_color,
                                              value: choice.checked,
                                              onChanged: (value) {
                                                choice.checked = value;
                                                controller.updateChoiceOfCrust(choice);
                                              },
                                            );
                                          },
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(
                                              height: 4,
                                            );
                                          },
                                        );
                                      }),
                                    ],
                                  ),
                                ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          Container(width: double.infinity, height: paddingExtraSmall, color: Colors.grey.shade200),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                          controller.toping.isEmpty
                              ? Container()
                              : Padding(
                                  padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Topping(1)',
                                        style: body1.copyWith(fontWeight: FontWeight.bold),
                                      ),
                                      Obx(() {
                                        return ListView.separated(
                                          itemCount: controller.toping.length,
                                          physics: const NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemBuilder: (context, index) {
                                            var topping = controller.toping[index];
                                            return LabeledCheckbox(
                                              trailinglabel:
                                                  '${Storage.instance.getValue(Constants.currency)} ${topping.price}',
                                              label: '${topping.name}',
                                              selectedColor: AppColors.primary_color,
                                              value: topping.checked,
                                              onChanged: (value) {
                                                topping.checked = value;
                                                controller.updateTopping(topping);
                                              },
                                            );
                                          },
                                          separatorBuilder: (context, index) {
                                            return const SizedBox(
                                              height: 4,
                                            );
                                          },
                                        );
                                      }),
                                    ],
                                  ),
                                ),
                          const SizedBox(
                            height: paddingLarge,
                          ),
                        ],
                      ),
                    );
            }),
          ),
        ),
      ),
    );
  }
}
