import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/models/MarketShopListResponse.dart';
import 'package:markkito_customer/utils/functions.dart';
import 'package:shape_of_view_null_safe/shape_of_view_null_safe.dart';

import '../../../../constants/colors.dart';
import '../../../../constants/dimens.dart';
import '../../../../generated/assets.dart';
import '../../../../themes/custom_theme.dart';
import '../../../routes/app_pages.dart';

class MarketHeader extends StatelessWidget {
  final MarketData? marketData;

  const MarketHeader({
    Key? key,
    this.marketData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: InkWell(
        onTap: () {
          Get.toNamed(Routes.MARKET_DETAILS);
        },
        child: Container(
          width: Get.width,
          height: 200,
          color: AppColors.card_bg_color,
          child: Stack(
            children: [
              Positioned(
                right: Get.width * .4,
                top: 2,
                child: SvgPicture.asset(Assets.svgGridDot),
              ),
              Positioned(
                right: -10,
                top: 20,
                child: SvgPicture.asset(Assets.svgMarketHomeIcon, color: AppColors.primary_color.withOpacity(.2)),
              ),
              Positioned(
                top: 70,
                left: -10,
                child: Image.asset(
                  Assets.imagesCurvedCurve,
                  fit: BoxFit.fill,
                  height: 100,
                  width: Get.width / 1.8,
                ),
              ),
              Positioned(
                bottom: paddingMedium,
                left: paddingMedium,
                child: SizedBox(
                  width: Get.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: List.generate(marketData!.shopThumbs!.length, (index) {
                          return Align(
                            widthFactor: 0.2,
                            alignment: Alignment.centerLeft,
                            child: ShapeOfView(
                              elevation: 0,
                              shape: CircleShape(borderColor: AppColors.white, borderWidth: 1),
                              child: Image.network(
                                marketData?.shopThumbs?[index] != null
                                    ? "${marketData?.shopThumbs?[index]}"
                                    : "https://picsum.photos/200/300",
                                height: 50,
                                width: 50,
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        }),
                      ),
                      const SizedBox(
                        height: paddingMedium,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: AppColors.white.withOpacity(.5),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 14.0, top: 8, right: 14, bottom: 8),
                          child: Text(
                            '${marketData?.offer} OFF',
                            style: caption.copyWith(
                                color: AppColors.primary_color, fontWeight: FontWeight.bold, fontSize: 10),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 3.0),
                        child: Text(
                          '${marketData?.marketName}',
                          style: headline6.copyWith(fontWeight: FontWeight.bold, color: AppColors.primary_color),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(right: paddingExtraLarge),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '${marketData?.marketLocation?.place}'.useCorrectEllipsis(),
                              softWrap: false,
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              style: subtitle2.copyWith(
                                color: Colors.grey,
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: AppColors.primary_color),
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0, right: 10, top: 2, bottom: 2),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        const Icon(
                                          Icons.star,
                                          size: 18,
                                          color: AppColors.primary_color,
                                        ),
                                        const SizedBox(
                                          width: 2,
                                        ),
                                        Text(
                                          '${marketData?.rating}',
                                          overflow: TextOverflow.fade,
                                          style: subtitle1.copyWith(
                                              color: AppColors.primary_color, fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey.shade500),
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 2),
                                    child: Row(
                                      children: [
                                        SvgPicture.asset(
                                          Assets.svgStores,
                                          color: AppColors.primary_color,
                                          height: 18,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 4.0),
                                          child: Text(
                                            '${(marketData?.distance)?.toStringAsFixed(1)} km',
                                            overflow: TextOverflow.fade,
                                            style: subtitle1.copyWith(fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
