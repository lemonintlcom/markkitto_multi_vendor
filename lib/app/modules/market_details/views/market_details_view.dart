import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/super_saving_deals_card.dart';
import 'package:markkito_customer/app/modules/market_details/views/market_header.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../../../../generated/assets.dart';
import '../../custom_widgets/custom_app_bar.dart';
import '../../custom_widgets/shop_item_card.dart';
import '../controllers/market_details_controller.dart';

class MarketDetailsView extends GetView<MarketDetailsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        titleText: '',
        titleWidget: Obx(() {
          return Text(controller.marketName.value);
        }),
        actions: const [
          // Padding(
          //   padding: const EdgeInsets.only(left: 10.0, right: 10, top: 1, bottom: 1),
          //   child: InkWell(
          //     onTap: () {},
          //     child: Row(
          //       children: [
          //         SvgPicture.asset(
          //           Assets.svgOfferPercentage,
          //         ),
          //         const SizedBox(
          //           width: 6,
          //         ),
          //         Text(
          //           'Offers',
          //           style: caption.copyWith(color: Colors.grey),
          //         )
          //       ],
          //     ),
          //   ),
          // )
        ],
      ),
      body: SafeArea(
        child: Obx(() {
          return controller.isLoading.value
              ? Container()
              : Stack(
                  children: [
                    CustomScrollView(
                      controller: controller.controller,
                      scrollDirection: Axis.vertical,
                      physics: const BouncingScrollPhysics(),
                      slivers: [
                        SliverToBoxAdapter(
                          child: MarketHeader(marketData: controller.marketData),
                        ),
                        const SliverToBoxAdapter(
                          child: SizedBox(
                            height: paddingMedium,
                          ),
                        ),
                        const SliverToBoxAdapter(
                          child: Padding(
                            padding: EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                            child: SuperSavingDealCard(),
                          ),
                        ),
                        const SliverToBoxAdapter(
                          child: SizedBox(
                            height: paddingLarge,
                          ),
                        ),
                        SliverToBoxAdapter(
                            child: Padding(
                          padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${controller.marketData?.shoplist?.length} SHOPS ARE IN SM STREET',
                                style: subtitle2,
                              ),
                              Image.asset(
                                Assets.imagesBookmarkOutlineSmall,
                                width: 14,
                                color: AppColors.primary_color.withOpacity(.7),
                              ),
                            ],
                          ),
                        )),
                        const SliverToBoxAdapter(
                          child: SizedBox(
                            height: paddingMedium,
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: SingleChildScrollView(
                            physics: const BouncingScrollPhysics(),
                            padding: const EdgeInsets.only(left: paddingLarge),
                            scrollDirection: Axis.horizontal,
                            child: Obx(() {
                              return Row(
                                children: List.generate(controller.categories.length, (index) {
                                  log("$index index");
                                  var cat = controller.categories[index];
                                  log("${cat?.name}");
                                  return Padding(
                                    padding: const EdgeInsets.only(right: paddingSmall),
                                    child: ChoiceChip(
                                      labelPadding: const EdgeInsets.all(2.0),
                                      label: Text(
                                        cat?.name == 'All' ? '${cat?.name!}' : '${cat?.name!}(${cat?.count!})',
                                        style: captionLite.copyWith(
                                            color: controller.defaultMarketTypeIndex.value == index
                                                ? AppColors.white
                                                : AppColors.black1),
                                      ),
                                      selected: controller.defaultMarketTypeIndex.value == index,
                                      selectedColor: Colors.black,
                                      pressElevation: 0,
                                      backgroundColor: AppColors.white,
                                      onSelected: (value) {
                                        controller.defaultMarketTypeIndex.value =
                                            value ? index : controller.defaultMarketTypeIndex.value;
                                      },
                                      // backgroundColor: color,
                                      elevation: 1,
                                      padding: const EdgeInsets.symmetric(horizontal: paddingLarge),
                                    ),
                                  );
                                }),
                              );
                            }),
                          ),
                        ),
                        const SliverToBoxAdapter(
                          child: SizedBox(
                            height: paddingMedium,
                          ),
                        ),
                        SliverList(
                          delegate: SliverChildBuilderDelegate((context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(left: paddingLarge, right: paddingLarge, bottom: 8.0),
                              child: SizedBox(
                                height: 150,
                                child: ShopItemCard(
                                  isFullWidth: true,
                                  shop: controller.shops[index],
                                ),
                              ),
                            );
                          }, childCount: controller.shops.length),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Obx(() {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AnimatedContainer(
                            width: Get.width * .65,
                            duration: const Duration(milliseconds: 200),
                            height: controller.isVisible.value ? kToolbarHeight : 0,
                            decoration: BoxDecoration(
                              color: AppColors.primary_color,
                              borderRadius: BorderRadius.circular(28),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(Assets.svgFilterIcon),
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        'Filter',
                                        style: body1.copyWith(color: AppColors.white),
                                      ),
                                    ],
                                  ),
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(top: 12.0, bottom: 12),
                                  child: VerticalDivider(
                                    color: Colors.white,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: paddingExtraLarge, right: paddingExtraLarge),
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(Assets.svgSortIcon),
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        'Sort',
                                        style: body1.copyWith(color: AppColors.white),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                    ),
                    Positioned(
                      bottom: 80,
                      right: paddingLarge,
                      child: Obx(() {
                        return Visibility(
                          visible: controller.rightBottomBoxIsVisible.value,
                          child: Container(
                            height: 200,
                            width: 200,
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              gradient: LinearGradient(
                                  colors: [
                                    AppColors.primary_color,
                                    AppColors.primary_color_lite,
                                  ],
                                  begin: FractionalOffset(0.0, 0.0),
                                  end: FractionalOffset(1.0, 0.8),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp),
                            ),
                            child: Stack(
                              children: [
                                Positioned(
                                  right: paddingSmall,
                                  top: paddingSmall,
                                  child: InkWell(
                                    onTap: () {
                                      controller.rightBottomBoxIsVisible.value = false;
                                    },
                                    child: const Icon(
                                      Icons.close,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                    )
                  ],
                );
        }),
      ),
    );
  }
}
