import 'package:get/get.dart';

import '../controllers/market_details_controller.dart';

class MarketDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MarketDetailsController>(
      () => MarketDetailsController(),
    );
  }
}
