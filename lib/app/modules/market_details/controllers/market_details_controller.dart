import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/MarketShopListResponse.dart';
import 'package:markkito_customer/app/data/models/ShopListResponse.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/storage.dart';

class MarketDetailsController extends GetxController {
  var defaultMarketTypeIndex = 0.obs;

  var isVisible = false.obs;
  var rightBottomBoxIsVisible = false.obs;

  ScrollController? controller;

  var pageNo = 0;

  MarketData? marketData;
  var isLoading = true.obs;

  var categories = <Catogeries?>[].obs;
  var shops = <Shop?>[].obs;

  var marketName = ''.obs;

  @override
  void onInit() {
    super.onInit();
    controller = ScrollController();
    controller?.addListener(() {
      if (controller?.position.userScrollDirection == ScrollDirection.forward) {
        if (!isVisible.value) {
          isVisible.value = true;
        }
      } else {
        if (isVisible.value) {
          isVisible.value = false;
        }
      }
    });

    getMarketDetails();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    controller?.dispose();
  }

  getMarketDetails() async {
    var data = {
      'pageNo': pageNo,
      'latitude': Storage.instance.getValue(Constants.latitude),
      'longitude': Storage.instance.getValue(Constants.longitude),
      'marketId': Get.arguments['marketId']
    };
    var result = await XHttp.request(Endpoints.r_market_shop_list, method: XHttp.POST, data: data);
    var marketShopData = MarketShopListResponse.fromJson(jsonDecode(result.data));
    if (marketShopData.statusCode == 200) {
      if (marketShopData.marketShopData != null) {
        marketData = marketShopData.marketShopData;
        categories.addAll(marketShopData.marketShopData!.catogeries!);

        categories.insert(0, Catogeries(name: 'All', count: 0, id: 999));

        shops.addAll(marketShopData.marketShopData!.shoplist!);

        marketName.value = marketData!.marketName!;
        isLoading.value = false;
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  void updateShopByCat(Catogeries cat) {
    shops.clear();

    if (cat.name == "All") {
      shops.addAll(marketData!.shoplist!);
    } else {
      shops.value =
          marketData!.shoplist!.where((element) => element.type?.toLowerCase() == cat.name?.toLowerCase()).toList();
    }
  }
}
