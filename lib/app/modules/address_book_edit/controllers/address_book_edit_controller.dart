import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:logger/logger.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/AddressListResponse.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:markkito_customer/network/endpoints.dart';

class AddressBookEditController extends GetxController {
  Rx<Address?> currentAddress = Rx(null);
  var isLoading = true.obs;

  late GoogleMapController mapController;
  late TextEditingController mobileNumberController;

  String? completePhoneNumber;

  String? phoneNumber;

  void onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  var markers = <MarkerId, Marker>{}.obs;

  @override
  void onInit() {
    super.onInit();
    mobileNumberController = TextEditingController();
    log('${Get.arguments['position']}');
    log('${Get.arguments['data']}');
    var data = jsonEncode(Get.arguments['data']);
    log('${data}');
    currentAddress.value = Address.fromJson(jsonDecode(data));
    log('${currentAddress.value?.addressId}');
    isLoading.value = false;
    mobileNumberController.text = '${currentAddress.value?.contact1}';

    addOrUpdateMarker();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void addOrUpdateMarker() {
    final marker = Marker(
      markerId: MarkerId('${currentAddress.value?.addressId}'),
      position: LatLng(currentAddress.value?.latitude, currentAddress.value?.longitude),
      // icon: BitmapDescriptor.,
      draggable: true,
      onDragEnd: (position) {
        updateNewMarkerPosition(position);
      },
      infoWindow: InfoWindow(
        title: '${currentAddress.value?.address1}',
        snippet: '${currentAddress.value?.address1}\n${currentAddress.value?.contact1}',
      ),
    );
    markers[MarkerId('${currentAddress.value?.addressId}')] = marker;
  }

  updateNewMarkerPosition(LatLng position) async {
    // final marker = markers.values.toList().firstWhere((item) => item.markerId == currentAddress.value?.addressId);

    String? placeText;
    List<Placemark> placemarks1 = await placemarkFromCoordinates(position.latitude, position.longitude);
    if (placemarks1.isNotEmpty) {
      Logger().e(placemarks1[0]);
      placeText =
          "${placemarks1[0].street} ${placemarks1[0].locality},${placemarks1[0].name},${placemarks1[0].subAdministrativeArea}";
      // placemarks.value = placeText;
      // locality.value = "${placemarks1[0].locality}";
      currentAddress.value?.address1 = placeText;
      currentAddress.value?.latitude = position.latitude;
      currentAddress.value?.longitude = position.longitude;
      addOrUpdateMarker(); //update
    }
  }

  // show input autocomplete with selected mode
  // then get the Prediction selected
  Future<void> showOverlayPlaces() async {
    Prediction? p = await PlacesAutocomplete.show(
      context: Get.context!,
      apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
      onError: (e) {},
      mode: Mode.overlay,
      strictbounds: false,
      types: [],
      language: "en",
      decoration: InputDecoration(
        hintText: 'Search',
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Colors.white,
          ),
        ),
      ),
      components: [Component(Component.country, "in"), Component(Component.country, "ae")],
    );

    displayPrediction(p, Get.context!);
  }

  Future<void> displayPrediction(Prediction? p, BuildContext context) async {
    if (p != null) {
      // get detail (lat/lng)
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: 'AIzaSyAODoObqlpreCc_Rx86b4FFJXG702X5TDI',
        apiHeaders: await const GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId!);
      final lat = detail.result.geometry!.location.lat;
      final lng = detail.result.geometry!.location.lng;

      mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, lng), zoom: 13),
        ),
      );
      // isLoading.value = true;
      await updateNewMarkerPosition(LatLng(lat, lng));
      // isLoading.value = false;
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text("${p.description} - $lat/$lng")),
      // );
    }
  }

  void updateAddress() async {
    /* var data = {
      'vendorUserId': shopId,
    };
    var result = await XHttp.request(Endpoints.c_shop_favourite, method: XHttp.POST, data: data);
    var favoriteResponse = FavoriteResponse.fromJson(jsonDecode(result.data));
    if (favoriteResponse.statusCode == 200) {
      if (favoriteResponse.data?.status == 'success') {
        SnackBarSuccess(titleText: "Success", messageText: "Shop added to favorite").show();
      } else {
        SnackBarFailure(titleText: "Failed", messageText: "Unable to favorite").show();
      }
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
    }*/
  }
}
