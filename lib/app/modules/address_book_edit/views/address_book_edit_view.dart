import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/app/routes/app_pages.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/generated/assets.dart';
import 'package:markkito_customer/themes/custom_theme.dart';

import '../controllers/address_book_edit_controller.dart';

class AddressBookEditView extends GetView<AddressBookEditController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
        titleText: 'Edit Address',
      ),
      body: Obx(() {
        return controller.isLoading.value
            ? Container()
            : Column(
                children: [
                  Container(
                    height: Get.height * .5,
                    color: Colors.red,
                    child: GoogleMap(
                      markers: controller.markers.values.toSet(),
                      myLocationEnabled: true,
                      onMapCreated: controller.onMapCreated,
                      onLongPress: (position) {
                        controller.updateNewMarkerPosition(position);
                      },
                      initialCameraPosition: CameraPosition(
                        target: LatLng(
                            controller.currentAddress.value?.latitude, controller.currentAddress.value?.longitude),
                        zoom: 13.0,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(paddingLarge),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      Assets.svgLocation,
                                      width: 16,
                                    ),
                                    const SizedBox(
                                      width: 8,
                                    ),
                                    Obx(() {
                                      return Flexible(
                                        child: Text(
                                          '${controller.currentAddress.value?.address1}'.split(',')[0],
                                          style: subtitleLite.copyWith(fontWeight: FontWeight.bold),
                                          maxLines: 1,
                                          textAlign: TextAlign.start,
                                          overflow: TextOverflow.fade,
                                          softWrap: false,
                                        ),
                                      );
                                    }),
                                  ],
                                ),
                              ),
                              MaterialButton(
                                color: Colors.grey.shade200,
                                textColor: AppColors.primary_color,
                                height: 32,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(4),
                                    ),
                                    side: BorderSide(color: Colors.grey.shade300)),
                                onPressed: () async {
                                  controller.showOverlayPlaces();
                                },
                                child: Text(
                                  'CHANGE',
                                  style: caption,
                                ),
                              ),
                            ],
                          ),
                          Obx(() {
                            return Text(
                              controller.currentAddress.value?.address1,
                              style: captionLite,
                              maxLines: 2,
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.fade,
                              softWrap: true,
                            );
                          }),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          IntlPhoneField(
                            controller: controller.mobileNumberController,
                            obscureText: false,
                            disableLengthCheck: false,
                            dropdownIconPosition: IconPosition.trailing,
                            showDropdownIcon: true,
                            flagsButtonPadding: EdgeInsets.all(10),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8),
                              enabledBorder: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                // width: 0.0 produces a thin "hairline" border
                                borderSide: BorderSide(color: Colors.grey, width: 0.0),
                              ),
                              focusedBorder: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                borderSide: BorderSide(color: AppColors.primary_color, width: 1),
                              ),
                              hintText: '9496xxxxxx',
                              hintStyle: subtitle1,
                            ),
                            initialCountryCode: 'IN',
                            onChanged: (phone) {
                              print(phone.number);
                              controller.phoneNumber = phone.number;
                              controller.completePhoneNumber = phone.completeNumber;
                            },
                          ),
                          const SizedBox(
                            height: paddingExtraLarge,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: PrimaryButton(
                                  text: 'Update',
                                  onTap: () {
                                    controller.updateAddress();
                                    // Get.parameters['fromHome'];
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
      }),
    );
  }
}
