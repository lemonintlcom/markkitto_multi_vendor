import 'package:get/get.dart';

import '../controllers/address_book_edit_controller.dart';

class AddressBookEditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddressBookEditController>(
      () => AddressBookEditController(),
    );
  }
}
