import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:get/get.dart';
import 'package:markkito_customer/app/modules/custom_widgets/bottom_sheet_modal.dart';
import 'package:markkito_customer/app/modules/custom_widgets/custom_app_bar.dart';
import 'package:markkito_customer/app/modules/home/controllers/home_controller.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

import '../controllers/review_controller.dart';
import 'package:flutter_rating_stars/flutter_rating_stars.dart';

class ReviewView extends GetView<ReviewController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
        titleText: 'Reviews',
      ),
      floatingActionButton: Obx(() {
        return AnimatedSlide(
          duration: controller.duration,
          offset: controller.showFab.value ? Offset.zero : const Offset(0, 2),
          child: FloatingActionButton.extended(
            label: Text('Add Review'),
            icon: const Icon(Icons.reviews),
            backgroundColor: Storage.instance.getValue(Constants.colorCode) == null
                ? AppColors.primary_color
                : hexToColor(Storage.instance.getValue(Constants.colorCode)),
            onPressed: () {
              Get.find<HomeController>().isLoggedIn
                  ? controller.showAddReviewBottomSheet(context)
                  : BottomSheetModal().showLoginRequired(context);
            },
          ),
        );
      }),
      body: NotificationListener<UserScrollNotification>(
        onNotification: (notification) {
          final ScrollDirection direction = notification.direction;
          if (direction == ScrollDirection.reverse) {
            controller.showFab.value = false;
          } else if (direction == ScrollDirection.forward) {
            controller.showFab.value = true;
          }
          return true;
        },
        child: Obx(() {
          return controller.isLoading.value
              ? Container()
              : controller.reviewList.isEmpty
                  ? Center(
                      child: Text(
                        'No reviews found',
                        style: headline5.copyWith(color: Colors.black),
                      ),
                    )
                  : ListView.separated(
                      controller: controller.controller,
                      physics: const BouncingScrollPhysics(),
                      itemCount: controller.reviewList.length,
                      separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
                      itemBuilder: (BuildContext context, int index) {
                        var item = controller.reviewList[index];
                        return Padding(
                          padding: const EdgeInsets.only(top: 6, bottom: 6),
                          child: ListTile(
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                RatingStars(
                                  value: double.parse('${item.score}'),
                                  onValueChanged: (v) {},
                                  starBuilder: (index, color) => Icon(
                                    Icons.star,
                                    color: color,
                                  ),
                                  starCount: 5,
                                  starSize: 20,
                                  valueLabelColor: const Color(0xff9b9b9b),
                                  valueLabelTextStyle: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                      fontSize: 12.0),
                                  valueLabelRadius: 15,
                                  maxValue: 5,
                                  starSpacing: 2,
                                  maxValueVisibility: false,
                                  valueLabelVisibility: true,
                                  animationDuration: const Duration(milliseconds: 1000),
                                  valueLabelPadding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                                  valueLabelMargin: const EdgeInsets.only(right: 8),
                                  starOffColor: const Color(0xffe7e8ea),
                                  starColor: Colors.yellow.shade700,
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                item.customerName == null
                                    ? Container()
                                    : Text(
                                        '${item.customerName}',
                                        style: captionLite.copyWith(fontSize: 16),
                                      ),
                                const SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  '${item.review}',
                                  style: caption.copyWith(color: Colors.black, fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
        }),
      ),
    );
  }
}
