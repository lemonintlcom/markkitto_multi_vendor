import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:markkito_customer/app/data/api_service/XHttp.dart';
import 'package:markkito_customer/app/data/models/AddReviewResponse.dart';
import 'package:markkito_customer/app/data/models/ReviewListResponse.dart';
import 'package:markkito_customer/app/modules/custom_widgets/primary_button.dart';
import 'package:markkito_customer/constants/colors.dart';
import 'package:markkito_customer/constants/constants.dart';
import 'package:markkito_customer/constants/dimens.dart';
import 'package:markkito_customer/network/endpoints.dart';
import 'package:markkito_customer/themes/custom_theme.dart';
import 'package:markkito_customer/utils/color_utils.dart';
import 'package:markkito_customer/utils/storage.dart';

class ReviewController extends GetxController {
  var isLoading = true.obs;
  var reviewList = <Review>[];
  var textController = TextEditingController();

  Rx<double> ratingValue = Rx(3.5);
  String type = 'vendor';
  int typeId = Storage.instance.getValue(Constants.storeId);

  var showFab = true.obs;
  final duration = const Duration(milliseconds: 300);
  ScrollController? controller;

  @override
  void onInit() {
    super.onInit();
    getReviews();
    controller = ScrollController();
    controller?.addListener(() {
      if (controller?.position.userScrollDirection == ScrollDirection.forward) {
        if (!showFab.value) {
          showFab.value = true;
        }
      } else {
        if (showFab.value) {
          showFab.value = false;
        }
      }
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  getReviews() async {
    var data = {
      'type': type,
      'typeId': typeId,
    };
    var result = await XHttp.request(Endpoints.r_reviews_and_ratings, method: XHttp.POST, data: data);
    var reviewsResponse = ReviewListResponse.fromJson(jsonDecode(result.data));
    if (reviewsResponse.statusCode == 200) {
      if (reviewsResponse.review!.isNotEmpty) {
        reviewList.clear();
        reviewList.addAll(reviewsResponse.review!);
      }
      isLoading.value = false;
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }

  showAddReviewBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.grey.withOpacity(.4),
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(paddingExtraLarge),
            child: Container(
              height: Get.height * .5,
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: AppColors.white,
                  width: 0.8,
                ),
              ),
              child: Column(
                children: [
                  Center(
                    child: FractionallySizedBox(
                      widthFactor: 0.25,
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        height: 4,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(2),
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Write your review',
                      style: subtitle1,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(
                        height: paddingExtraLarge,
                      ),
                      Obx(() {
                        return RatingBar.builder(
                          initialRating: ratingValue.value,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                          itemBuilder: (context, _) => const Icon(
                            Icons.star,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        );
                      }),
                      const SizedBox(
                        height: paddingExtraLarge,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(paddingExtraLarge),
                        child: SizedBox(
                          height: 120,
                          child: TextField(
                            controller: textController,
                            minLines: 1,
                            maxLines: 5,
                            keyboardType: TextInputType.multiline,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(8),
                              enabledBorder: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                // width: 0.0 produces a thin "hairline" border
                                borderSide: BorderSide(color: Colors.grey, width: 0.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(Radius.circular(8)),
                                borderSide: BorderSide(
                                    color: Storage.instance.getValue(Constants.colorCode) == null
                                        ? AppColors.primary_color
                                        : hexToColor(Storage.instance.getValue(Constants.colorCode)),
                                    width: 1),
                              ),
                              hintText: 'Write your review here',
                              hintStyle: subtitle1.copyWith(color: Colors.grey),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: paddingExtraLarge,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(paddingExtraLarge),
                        child: Row(
                          children: [
                            Expanded(
                              child: PrimaryButton(
                                text: 'Submit',
                                onTap: () {
                                  doSubmitReviewApi();
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  void doSubmitReviewApi() async {
    var data = {
      'score': ratingValue.value,
      'type': 'vendor',
      'comment': textController.text,
      'vendorUserTypeId': Storage.instance.getValue(Constants.storeId),
    };
    var result = await XHttp.request(Endpoints.c_add_rating, method: XHttp.POST, data: data);
    var addReviewResponse = AddReviewResponse.fromJson(jsonDecode(result.data));
    if (addReviewResponse.statusCode == 200) {
      isLoading.value = false;
      getReviews();
      Get.back();
      SnackBarSuccess(titleText: "Success", messageText: "${addReviewResponse.data?.status}").show();
    } else {
      SnackBarFailure(titleText: "Oops..", messageText: "Something went wrong").show();
      isLoading.value = false;
    }
  }
}
