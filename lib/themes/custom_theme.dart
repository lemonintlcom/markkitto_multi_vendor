import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pinput/pinput.dart';

import '../constants/colors.dart';

class CustomTextStyle {
  static TextStyle title(BuildContext context) {
    return Theme.of(context).textTheme.headline5!.copyWith(fontSize: 20, color: Colors.grey[700]);
  }
}

class SnackBarSuccess {
  final String titleText;
  final String messageText;
  final IconData icon;
  final Color backgroundColor;
  final Color colorText;

  SnackBarSuccess({
    required this.titleText,
    required this.messageText,
    this.icon = Icons.check_circle,
    this.backgroundColor = AppColors.primary_color,
    this.colorText = AppColors.white,
  });

  show() {
    Get.snackbar(
      "",
      "",
      titleText: Text(
        titleText,
        style: body1.copyWith(fontWeight: FontWeight.bold, color: Colors.white),
      ),
      messageText: Text(
        messageText,
        style: captionLite.copyWith(fontWeight: FontWeight.normal, color: Colors.white),
      ),
      icon: Icon(
        icon,
        color: Colors.white,
      ),
      backgroundColor: backgroundColor.withOpacity(.6),
      colorText: colorText,
    );
  }
}

class SnackBarFailure {
  final String titleText;
  final String messageText;
  final IconData icon;
  final Color backgroundColor;
  final Color colorText;

  SnackBarFailure({
    required this.titleText,
    required this.messageText,
    this.icon = Icons.error_outline,
    this.backgroundColor = Colors.redAccent,
    this.colorText = AppColors.white,
  });

  show() {
    Get.snackbar(
      "",
      "",
      titleText: Text(
        titleText,
        style: body1.copyWith(fontWeight: FontWeight.bold, color: Colors.white),
      ),
      messageText: Text(
        messageText,
        style: captionLite.copyWith(fontWeight: FontWeight.normal, color: Colors.white),
      ),
      icon: Icon(
        icon,
        color: Colors.white,
      ),
      backgroundColor: backgroundColor.withOpacity(.6),
      colorText: colorText,
    );
  }
}

final defaultPinTheme = PinTheme(
  width: 56,
  height: 56,
  textStyle: GoogleFonts.nunito(
    fontSize: 22,
    color: const Color.fromRGBO(30, 60, 87, 1),
  ),
  decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(19),
    border: Border.all(color: AppColors.primary_color_lite),
  ),
);

TextStyle headline1 = GoogleFonts.nunito(
  fontWeight: FontWeight.w300,
  fontSize: 96,
  letterSpacing: -1.5,
);
TextStyle headline2 = GoogleFonts.nunito(
  fontWeight: FontWeight.w300,
  fontSize: 60,
  letterSpacing: -0.5,
);
TextStyle headline3 = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 48,
  letterSpacing: 0,
);
TextStyle headline4 = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 34,
  letterSpacing: 0.25,
);
TextStyle headline5 = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 24,
  letterSpacing: 0,
);
TextStyle title = GoogleFonts.nunito(
  fontWeight: FontWeight.w600,
  fontSize: 20,
  letterSpacing: 0.15,
);

TextStyle headline6 = GoogleFonts.nunito(
  fontWeight: FontWeight.w500,
  fontSize: 20,
  letterSpacing: 0.15,
);
TextStyle subtitle1 = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 16,
  letterSpacing: 0.15,
);
TextStyle subtitleLite = GoogleFonts.nunito(
  fontWeight: FontWeight.w300,
  fontSize: 16,
  letterSpacing: 0.45,
);
TextStyle subtitle2 = GoogleFonts.nunito(
  fontWeight: FontWeight.w500,
  fontSize: 14,
  letterSpacing: 0.1,
);
TextStyle body1 = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 16,
  letterSpacing: 0.5,
);
TextStyle body2 = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 14,
  letterSpacing: 0.25,
);
TextStyle body3 = GoogleFonts.nunito(
  fontWeight: FontWeight.w600,
  fontSize: 16,
  letterSpacing: 0.25,
);
TextStyle caption = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 12,
  letterSpacing: 0.4,
);
TextStyle captionLite = GoogleFonts.nunito(
  fontWeight: FontWeight.w300,
  fontSize: 13,
  letterSpacing: 0.4,
);
TextStyle captionSmall = GoogleFonts.nunito(
  fontWeight: FontWeight.w400,
  fontSize: 10,
  letterSpacing: 0.4,
);
